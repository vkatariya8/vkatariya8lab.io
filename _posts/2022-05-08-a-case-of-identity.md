---
layout: post
title: "A Case of Identity"
description: "Where I finally claim parts of a new identity?"
date: "2022-05-08"
tags: [blog]
categories: [blog]
comments: false
share: true
---

I had a breakthrough of sorts this week. Just now, I was in Uttarakhand visiting a friend's farm/homestead, and what was particularly nice was that I was able to teach them a few things about fermentation. The particular breakthrough I just had was that that's how they probably see me now: as someone who is good with food and ferments. Wow! I've been trying for years and years to break free from the identity of solely being smart. And I've arrived, I guess.

Looking back, it seems like most of the validation I received as a kid was for being smart and clever and good at exams and such. I think it was so pervasive that it became my identity, and then I started to resent it as I entered my higher and graduate studies. A large part of resenting it was an effort to move _away_ from it, so that I could spend some time figuring out who _I_ was, rather than fit the mould of the expectations that were placed on me. There's so much I can write about this, but I'll be brief for now.

It also appears, retrospectively, that a lot of the hobbies I picked up were for precisely this reason. In particular, my quizzing habit/practice is an interesting example. I love(d) quizzing, and was decent at it, but eventually realized it was leaving me too close to the smart/clever/studious identity that I was trying hard to let go of. And so, for this reason and for a few others too, I decided to actively avoid competitive quizzing. It feels very natural to do so. My other hobbies, at least the ones that have stuck for a few years (cooking, baking, fermenting, gardening, running), have a kind of unifying theme to them. They are all stuff to physically do! In a particular place. Not at a laptop or on a piece of paper.

For a few years, I think I was really struggling with a sort of reconciliation. That is, how do I internalize that I am smart and good at some things without letting it become my identity, and how exactly do I include the other esoteric-ish things I do in a way that doesn't make me feel like I have two split personalities? I think it is safe to say that this was an existential crisis, albeit not a life-threatening one.

And now! I met a friend who primarily knows me for my cooking and fermenting and baking! And that's been reinforced because of my time with them! And probably if they need to introduce me to someone, that's how they'll do it: as a cool person who ferments stuff and knows a lot about food. I can't fully comprehend how free this makes me feel right now.

I think what's really nice is the way I've been patient with myself? I've let this process take its due course. At the same time, parts of it have been active in a way that I didn't recognize at the time, but I didn't stop myself at some points in time when I could have. I suppose that reaching out for more handy than nerdy hobbies seemed intuitive and something I needed, and I let myself reach out and start to claim some of that territory without second-guessing myself? And it feels like I'm decently integrated now? Like, how do I say it? I feel like I have different facets but don't feel compelled to gravitate to any of the edges. And none of them are pulling me too strongly.
