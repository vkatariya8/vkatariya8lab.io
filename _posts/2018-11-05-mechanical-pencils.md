---
layout: post
title:  "My Mechanical Pencils"
tags: [stationery, writing, blog]
categories: blog
date: 2018-11-05
---

A few weeks ago, I was up early to finish some homework but just didn't feel like doing it. Idling and drawing random symbols in my notebook, I picked up a new acquisition, the Pentel Graph Gear 1000 0.9mm mechanical pencil. With its thick lead and tip, it was unlike what I usually write with, and suddenly I was happily tapping at my notebook, writing things down, and admiring the finish that the pencil gave. That pencil was single-handedly responsible for me completing my homework that day.

Ever since I was a tiny kid, I've enjoyed using different writing instruments. A medium-weight fine-nib fountain pen will always be my favorite in terms of joy of writing, but over the last year, I've discovered a few new mechanical pencils which make the act of taking notes a joy. 

For years, I used mechanical pencils without giving them much thought. I used cheap light pencils which were merely things that could hold a piece of graphite lead and offer some traction for grip. Partly inspired by an overall decision to generate less waste and use higher-quality products for a longer time, I looked online for some new mechanical pencils.

I found three pencils that I truly love now. They are all different, and I won't shy away from saying that they have individual personalities. But the fact that I love the most is the sheer amount of design effort that's gone into these. And right now, I'm surprised that I didn't notice this earlier; I've always looked admiringly at well-designed fountain pens, so why should pencils be different?

The Uni Kuru Toga Roulette 0.5mm is my daily workhorse pencil. The Kuru Toga line of pencils from Uni is unique because of the automatic mechanism that rotates the lead each time you press the pencil on the paper. As you write, the pencil ensures that the sharpest point of the lead stays in contact with the paper. You don't have to twirl the pencil around in your hand to ensure you get an even line. This pencil does it for you. It is magic. The pencil is made almost entirely of metal, feels hefty but not heavy, and is easily the best pencil I have ever written with.

I also have a Zebra Del Guard 0.7mm as well, and this pencil has another unique technology. The "Del Guard", as it is called, consists of a few springs close to the tip that absorb and redistribute the forces on the lead, making it very difficult to snap the lead in two when writing or drawing. And finally, the Pentel Graph Gear I alluded to in the beginning of this post. That one is not my type, but I enjoy using it occasionally where I almost poke into my notebook vertically and write boldface print characters for fun.

It's funny to think that on some days, I write just because I want to feel the friction and movement that my mechanical pencils offer. My Kuru Toga lets me draw the most beautiful Greek symbols for my physics research, and you know, whatever works, works, right?
