---
layout: post
title: "Traitorous fridge"
description: "The many ways my refrigerator betrayed my trust"
date: "2023-07-03"
tags: [blog]
categories: [blog]
comments: false
share: true
---

A few months ago, my fridge stopped fridge-ing properly. I didn't realize it at first, because I usually use up my veggies pretty quickly and a lot of my fridge is ferments, so they don't spoil in the fridge but they do 
