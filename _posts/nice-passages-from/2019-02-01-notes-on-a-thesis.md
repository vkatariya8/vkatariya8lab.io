---
layout: post
title: "Notes on a Thesis, by Tiphaine Rivière"
description: "Select pages from a hilarious graphic novel"
date: "2019-02-01"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---

This was a hilarious book to read. As a PhD student myself, I could empathize with a significant fraction of what Jeane, in the book, has to go through as she tries to write a thesis. Here are a few funny pages.

<br>

![]({{"/assets/notes-on-a-thesis/76.png" | absolute_url}}){:class="img-responsive" .center-image}

<br>

* * *

<br>

![]({{"/assets/notes-on-a-thesis/115.png" | absolute_url}}){:class="img-responsive" .center-image}

![]({{"/assets/notes-on-a-thesis/119.png" | absolute_url}}){:class="img-responsive" .center-image}

![]({{"/assets/notes-on-a-thesis/121.png" | absolute_url}}){:class="img-responsive" .center-image}

<br>

* * *
