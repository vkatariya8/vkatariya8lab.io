---
layout: post
title: "Let Me Not Be Mad, by A.K. Benjamin"
description: "Excerpts from Let Me Not Be Mad, by A.K. Benjamin"
date: "2019-11-11"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---

Excerpts of choice from _Let Me Not Be Mad_, by A.K. Benjamin.

<br>

> Often the research points to only the coarsest shallows of what we do; our bodies and minds endlessly encoding one another in the most delicate, ineffable detail. Sometimes clinical need makes us stray into poorly lit places, like measuring "intuition"--an "old whore making yet another comeback," according to one recent paper--but however we define it, intuition is no more than a password for the small fraction of data we can't quite keep up with (beyond the even smaller fraction that we can), as the sum of one another, of you and I, works through us with limitless nuance.

<br>

---

<br>

> It's not too cynical to suggest that we too might run the risk of getting lost constructing fantastically elaborate and expensive simulacra of our own ideas.

> Especially when the ideas are as slippery as "you". Am I talking to a *charming* homunculus sitting somewhere behind an inordinately complex control panel, a pair of blue-gray eyes behind the painting's eyes? And would that "you" have a smaller, more defenseless, even more charming you residing within, like a Russian doll? Or are "you" fundamentally not there, there as only effect, an attractive epiphenomenon of the authorless integration of different faculties? An accident, in other words.

<br>

---

<br>

> When a clinician turns his hand to writing case studies, he might as well be writing fiction for all the relevance it bears to what actually happens in the room. If, like Philip Roth says, you're wrong about someone before you meet them, wrong again when you meet them, wrong once more when you think about the meeting afterward, then when it comes to writing them up you might as well be working from scratch. Names and details must be changed, but we shouldn't stop there. Some fictionalizing is inevitable; necessary and contaminating in equal measure: Imagination might just help the next patient.

<br>

---

<br>

> He was asking me simply, like a father might, but there was also something rhetorical in it, as though it wasn't mine to answer, or that it contained an embedded contradiction--like a Zen koan--meaning I couldn't receive it simply. He leaned in a little, flinty eyes fixing me, hinting at cool and warmth at the same time. The room felt like he'd been sitting in it for fifty years, a hermit's cave, every corner under his control. I thought about the featureless, dissociated feel of my clinic rooms, as much an extension of myself as a new, poorly fitted artificial limb. There was a real sense of force in just being there, a place to be myself while he gently x-rayed me; in fact, it was disconcertingly intense: I wish I'd brought a case to hide behind, but I was the case.

<br>

---
