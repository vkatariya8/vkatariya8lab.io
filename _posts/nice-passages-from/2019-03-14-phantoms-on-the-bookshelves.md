---
layout: post
title: "Phantoms on the Bookshelves, by Jacques Bonnet"
description: "Memoir-style book on reading and collecting books"
date: "2019-03-14"
tags: [books, nice-passages-from]
categories: [nice-passages-from]
comments: false
share: true
---

<br>

> Manguel is at one on this with Pliny the Elder: "It is very rarely that a bad book does not contain some merit for the cultivated man." In these two reasons not to part with a book -- the recall of a long-lost sentiment, or some possible interest in the future -- there surfaces a strange anxiety. The book is the precious material expression of a past emotion, or the chance of having one in years to come, and to get rid of it would bring the risk of a serious sense of loss. [...] An inextinguishable curiosity drove me to find out what lay behind the words and phrases, and the unknown reality on to which I had stumbled ("I should like you to be amazed not only by what you are reading, but by the miracle that it should be readable at all" - Vladimir Nabokov, _Pale Fire_). The fanatical reader is not only anxious, he or she is curious. And surely human curiosity -- condemned as it was by certain Fathers of the Church as being of no purpose since the coming of Christ, and even prohibited, since we now have the Gospels -- is one of the determining factors of all our actions? A capital element in the search for knowledge, in scientific discoveries or technological progress, the essential force behind human endeavour. And curiosity has no end: it is without limits. It feeds on itself, is never satisfied with what it finds, but must always press on, exhausting itself only with our dying breath.

<br>

---

<br>

> After that, a strange relationship becomes established between the bibliomaniac and his (or her) thousands of books. The same relation as between a gardener and an invasive climbing plant: the plant grows all by itself, in a manner invisible to the naked eye, but at a rate of progress that is measurable after a few weeks. The gardener, unless he is willing ot chop it down, can only indicate the direction he wants it to take. In just the same way, prolific libraries take on an independent existence, and become living things. ("To build up a library is to create a life. It is never merely a random collection of books" - _The Paper House_) We may have chosen its themes, and the general pathways along which it will develop, but we can only stand and watch as it invades all the walls of the room, climbs to the ceiling, annexes the other rooms one by one, expelling anything that gets in the way. It eliminates pictures hanging on the walls, or ornaments that obstruct its advance; it moves on with its necessary but cumbersome acolytes -- stools and ladders -- and forces its owner into constant reorganization, since its progress is not linear and calls for ever new kinds of division.

<br>

---

<br>

> It is hardly surprising that reading should be experienced as a unique activity, and in my own case, there is always the euphoria of being able to put a reality behind the name of an author or the title of a book. ("I read without selecting, just to get in touch" - Walter Benjamin.) Until it has been read, a book is, at worst, a jumble of signs on the page, at best a vague, perhaps false image, arising from what one has heard about it. To pick up a book in your hands, and discover what it really contains is like conferring flesh and blood, in other words a density and thickness, that it will never lose again, to what was previously just a word.
> 
> [...]
> 
> Every time you open a book for the first time, there is something akin to safe-breaking about it. Yes, that's exactly it: the frantic reader is like a burglar who has spent hours and hours digging a tunnel to enter the strongroom of a bank. He emerges face to face with hundreds of strongboxes, all identical, and opens them one by one. And each time the box is opened, it loses its anonymity and becomes unique: one is filled with paintings, another with bundles of banknotes, a third with jewels or letters tied in ribbon, engravings, objects of no value at all, silverware, photos, gold sovereigns, dried flowers, files of paper, crystal glasses, or children's toys -- and so on. There is something intoxicating about opening a new one, finding its contents and feeling overjoyed that in a trice one is no longer in front of a set of boxes, but in the presence of the riches and the wretched banalities that make up human existence.

<br>

---

<br>

> Oddly enough, the infinite source of information which the internet provides does not have for me the same magical status as my library. Here I am in front of my computer, I can look up anything I want, jumping even further in time and space than through my books, but there's something missing: that touch of the divine. Perhaps it's something physical: I'm only using my dingertips: the whole process is outside me, going through a screen and a machine. Nothing like these walls lined with books which I know -- almost -- by heart. On one hand, I feel as if I have a fabulous artificial arm, able to move about in that interstellar space outside, while on the other, I am inside a womb whose walls are my book-lined shelves -- the archetype in literature would be the inside of the _Nautilus_, 20,000 leagues under the sea. As you see, it is not always a rational matter.

<br>

---

<br>

> The books in my library are like old houses, breathing the presence of the men and women who have lived there in the past, with their sufferings, their loves, their hates, their surprises and disappointments, their hopes and their resignation. On reflection, I have always lived in old buildings myself...

<br>

---
