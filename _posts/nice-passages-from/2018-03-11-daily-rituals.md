---
layout: post
title: "Daily Rituals: How Artists Work, by Mason Currey"
description: "Excerpts from Daily Rituals, a book by Mason Currey"
date: "2018-03-11"
tags: [nice-passages-from, books]
categories: nice-passages-from
comments: false
share: true
---

This was a nice book I read. It's by Mason Currey and it's a survey of the daily rituals and routines of 150+ artists. Very insightful and also humanizing. Of course, the very successful are working consistently for long periods of time, and it's inspiring and also daunting at the same time. However, everyone is distracted - by social obligations, compulsive letter-writing, something or the other.

After reading this book, I felt inspired and bolstered. The major part of doing is just showing up every day. Here are some passages.

<br>

---

<br>

> "There were no parties, no receptions, no bourgeois values. We completely avoided all that. There was the presence only of essentials. It was an uncluttered kind of life, a simplicity deliberately constructed so that she could do her work." - about Simone de Beauvoir.

<br>

---

<br>

> “Do you know what moviemaking is?” [Ingmar] Bergman asked in a 1964 interview. “Eight hours of hard work each day to get three minutes of film. And during those eight hours there are maybe only ten or twelve minutes, if you’re lucky, of real creation. And maybe they don’t come. Then you have to gear yourself for another eight hours and pray you’re going to get your good ten minutes this time.”

<br>

---

<br>

> Sometimes I don’t understand why my arms don’t drop from my body with fatigue, why my brain doesn’t melt away. I am leading an austere life, stripped of all external pleasure, and am sustained only by a kind of permanent frenzy, which sometimes makes me weep tears of impotence but never abates. I love my work with a love that is frantic and perverted, as an ascetic loves the hair shirt that scratches his belly. Sometimes, when I am empty, when words don’t come, when I find I haven’t written a single sentence after scribbling whole pages, I collapse on my couch and lie there dazed, bogged down in a swamp of despair, hating myself and blaming myself for this demented pride that makes me pant after a chimera. A quarter of an hour later, everything has changed; my heart is pounding with joy. - Gustave Flaubert

<br>

---

<br>

> “I keep to this routine every day without variation,” he told The Paris Review in 2004. “The repetition itself becomes the important thing; it’s a form of mesmerism. I mesmerize myself to reach a deeper state of mind.” - Haruki Murakami

<br>

---

<br>

> For the morning writing, her ritual is to rise around 5:00, make coffee, and “watch the light come.” This last part is crucial. “Writers all devise ways to approach that place where they expect to make the contact, where they become the conduit, or where they engage in this mysterious process,” [Toni] Morrison said. “For me, light is the signal in the transaction. It’s not being in the light, it’s being there before it arrives. It enables me, in some sense.”

<br>

---

<br>

> When the writing wasn’t going well, he would often knock off the fiction and answer letters, which gave him a welcome break from “the awful responsibility of writing”—or, as he sometimes called it, “the responsibility of awful writing.” - Ernest Hemingway.

<br>

---

<br>

> (And although he had many patients who relied on him, [Carl] Jung was not shy about taking time off; “I’ve realized that somebody who’s tired and needs a rest, and goes on working all the same is a fool,” he said.)

<br>

---

<br>

> All those I think who have lived as literary men,—working daily as literary labourers,—will agree with me that three hours a day will produce as much as a man ought to write. But then, he should so have trained himself that he shall be able to work continuously during those three hours,—so have tutored his mind that it shall not be necessary for him to sit nibbling his pen, and gazing at the wall before him, till he shall have found the words with which he wants to express his ideas.  - Anthony Trollope.

<br>

---