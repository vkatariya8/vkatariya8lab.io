---
layout: post
title: "Erosion, by Terry Tempest Williams"
description: "Excerpts from Erosion, by Terry Tempest Williams"
date: "2020-05-24"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---

<br>

Imagine a ruin so strange it must never have happened.

First, picture the forest. I want you to be its conscience, the eyes in the trees. The trees are columns of slick, brindled bark like muscular animals overgrown beyond all reason. Every space is filled with life: delicate, poisonous frogs war-painted like skeletons, clutched in copulation, secreting their precious eggs onto dripping leaves. Vines strangling their own kin in the everlasting wrestle for sunlight. The breathing of monkeys. A glide of snake belly on branch. A single-file army of ants biting a mammoth tree into uniform grains and hauling it down to the dark for their ravenous queen. And, in reply, a choir of seedlings arching their necks out of rotted tree stumps, sucking life out of death. This forest eats itself and lives forever.

<br>

---

<br>

And you'll say I did. You'll say I walked across Africa with my wrists unshackled, and now I am one more soul walking free in a white skin, wearing some thread of the stolen goods: cotton or diamond, freedom at the very least, prosperity. Some of us know how we came by our fortune, and some of us don't, but we wear it all the same. There's only one question worth asking now: How do we aim to live with it?

I know how people are, with their habits of mind. Most will sail through from cradle to grave with a conscience as clean as snow. It's easy to point at other men, conveniently dead, starting with the ones who first scooped up mud from riverbanks to catch the scent of a source. Why, Dr. Livingstone, I presume, wasn't he the rascal! He and all the profiteers who've since walked out on Africa as a husband quits a wife, leaving her with her naked body curled around the emptied-out mine of her womb. I know people. Most have no earthly notion of the price of a snow-white conscience.

<br>

---

<br>

Wilderness is a place where we experience the quiet and sometimes violent unfolding of nature, where the natural processes of life are sustained and supported. It is where we feel the rightness of relationships, where we sense our true place, a part of, not apart from, the forces of life. Wilderness is harmony revisited with adaptations unexpected and surprising like the first shining organisms that shimmered toward life, emerging from that steamy primordial swamp. And just like every other creature, we are selected for and selected against. Wilderness returns us to this one simple fact: we are animals. Unlike the world of humans, who trade on greed, scarcity and selfishness, nature functions on frugality, abundance and altruism. Nothing wasted. Yes, there is a brutality to the wild, but there is also resiliency in the renewal of each day.

<br>

---

<br>

Little has changed. Underwater, the seals glide, spin and turn abruptly--some just missing a brush with our bodies. I want to touch them, but resist. Can intimacy exist between two species? Or only longing?

A baby fur seal erupts in front of my mask and blows bubbles in my face. I surface--no fur seal in sight. I submerge once again, swim ahead to find him. He nips my arm from behind. I turn--nothing. Suddenly the baby is back, suspended in the water, floating upside down. He is staring at me. I stare back. Curiosity is the great teacher. His large opaque eyes are a wilderness.

<br>

---

<br>

The Divine Feminism is not a mystical presence outside us, but an embodied presence within us made of flesh and bones, xylem and phloem, fur and scales, inhabiting every inch of this planet we call home. Humility is the way. Evolution is the path. Revolution is coming as our wellspring of desires finally meets Earth on its geologic terms. We are eroding. We are evolving. This is my mantra. The time has come to stop seeing ourselves as saviors and instead see ourselves as human beings on a burning globe capable of acknowledging the harm we have caused. Do we dare to hold our severed hearts in our hands as both an offering and a sacrifice in the name of all that is now required?

<br>

---


