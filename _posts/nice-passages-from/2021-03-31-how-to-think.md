---
layout: post
title: "How to Think, by Alan Jacobs"
description: "Excerpts from How to Think, by Alan Jacobs"
date: "2021-03-31"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---

<br>

Robinson further comments that this kind of usage "demonstrates how effectively such consensus can close off subject from inquiry," which may be the most important point of all. The more useful a term is for marking my inclusion in a group, the less interested I will be in testing the validity of my use of that term against-well, against any kind of standard. People who like accusing others of Puritanism have a fairly serious investment, then, in knowing as little as possible about actual Puritans. They are invested, for the moment anyway, in not thinking.

Robinson's analysis is acute, and all the more so given that it was written before the Internet became a culturewide phenomenon. Why would people ever think, when thinking deprives them of "the pleasure of sharing an attitude one knows is socially approved"-especially in an online environment where the social approval of one's attitudes is so much easier to acquire, in the currency of likes, faves, followers, and friends? And to acquire instantaneously?

p. 21

<br>

---

<br>

So, again, no: academic life doesn't do much to help one think, at least not in the sense in which I am commending thinking. It helps one to amass a body of knowledge and to learn and deploy certain approved rhetorical strategies, which requires a good memory, intellectual agility, and the like. But little about the academic life demands that you question your impulsive reactions-and that's true, as Daniel Kahneman suggests, even when what you do with your academic life is study impulsive reactions.
Being a teacher, though: that's a different thing. I have been teaching undergraduates for more than thirty years now, and generally speaking undergraduate education is a wonderful laboratory for thinking. Most of my students know what they believe, and want to argue for it, but they also realize that they still have a lot to learn. (The widespread belief that college students are unteachably arrogant know-it-alls does not match my experience. I know the type, but it's not a my common one, and it's not any more common now than it was when I started this game.) It's very rewarding to show them not necessarily that their beliefs are wrong, but that they haven't defended them very well, haven't understood their underlying logic, haven't grasped the best ways to commend their views to skeptical Others.

p. 24

<br>

---

<br>

[note: RCO = repugnant cultural order]

As I hinted earlier, if fundamentalist or evangelical Christians tend to be the RCO for secular academics, the reverse is true as well-and that mutual suspicion is something I've been trying to navigate my whole adult life. And now I live in a political order that, taken as a whole, has assumed the lamentable traits-willful incomprehension, toxic suspicion-that I'm used to seeing in those smaller mutually antagonistic communities. Everyone today seems to have an RCO, and everyone's RCO is on social media somewhere. We may be able to avoid listening to our RCO, but we can't avoid the realization that he or she is there, shouting from two rooms away.


This is a profoundly unhealthy situation. It's unhealthy because it prevents us from recognizing others as our neighbors-even when they are quite literally our neighbors. If I'm consumed by this belief that that person over there is both Other and Repugnant, I may never discover that my favorite television program is also his favorite television program; that we like some of the same books, though not for precisely the same reasons; that we both know what it's like to nurse a loved one through a long illness. All of which is to say that H may all too easily forget that political and social and religious differences are not the whole of human experience. The cold divisive logic of the RCO impoverishes us, all of us, and brings us closer to that primitive state that the political philosopher Thomas Hobbes called "the war of every man against every man."

p. 27

<br>

---

<br>

The second component is this: when your feelings are properly cultivated, when that part of your life is strong and healthy, then your responses to the world will be adequate to what the world is really like. To have your feelings moved by the beauty of a landscape is to respond to that landscape in the way that it deserves; to have your feelings moved in a very different direction by the sight of people living in abject poverty is to respond to that situation in the way that it deserves. The latter example is especially relevant to someone like Mill who wishes to be a social reformer: if your analysis leads you to the conclusion that is it unjust that people suffer in poverty in a wealthy country, but your feelings do not match your analysis, then something has gone awry with you. And it may very well happen that if the proper feelings are not present and imaginatively active, then you will not even bother to do the analysis that would reveal unmistakable injustice. If the feelings are not cultivated the analytical faculties might not function at all. (This is a point to which we will return.)

p. 44

<br>

---

<br>

They were moved, I think, by a heartfelt commitment to solidarity, and again, at times solidarity should trump what I have called "critical reflection." When your friend has just fallen and broken her arm, it is time to comfort her and get her care, not to offer a lecture on the dangers of skateboarding. That should come later, and perhaps shouldn't come from you at all (depending on what your relationship is). But when Coates makes a  "case for reparations,' that's a matter of national public policy, which means that, though solidarity with the victims of injustice is an indispensable driver of meaningful political action, solidarity is not enough: it must be supplemented by a colder-eyed look at what particular strategies and tactics are most likely to realize the desired end.

But it's hard to see things that way when you are something of an optimist, in Scruton's sense: a questioning of your preferred means can look like indifference toward your most treasured ends. We all fall into this trap from time to time. But the distinction between the two is absolutely vital, and must always be kept in the forefront of our minds in any public debate. If we are willing to grant, at the outset, that the people we're debating agree about ends-that they want a healthy and prosperous society in which all people can flourishthen we can converse with them, we can see ourselves as genuine members of a community.

And even if at the end of the day we have to conclude that we all do not want the same goods (which can, alas, happen), it is better that we learn it at the end of the day than decide it before sunrise. Along that path we can learn from one another in a great many ways and we have a chance of discovering unexpected opportunities for membership: for there can be more genuine fellowship among those who share the same disposition than among those who share the same beliefs, especially if that disposition is toward kindness and generosity.

p. 68

<br>

---

<br>

What System 1 does for us is to provide us with a repertoire of biases, biases that reduce the decision-making load on our conscious brains. These biases aren't infallible, but they provide what Kahneman calls useful "heuristics': they're right often enough that it makes sense to follow them and not to try to override them without some good reason (say, if you're someone whose calling in life is to help homeless people). We simply would not be able to navigate through life without these biases, these prejudices the cognitive demands of having to assess every single situation would be so great as to paralyze us. [...]


So we need the biases, the emotional predispositions, to relieve that cognitive load. We just want them to be the right ones. As a wise man once said, one of the key tasks of critical reflection is to distinguish the true prejudices by which we understand from the false ones by which we misunderstand.* System 1 works on its own, without conscious direction, but it can be changed, trained; it can develop new habits. This is what Mill meant when he spoke of the power of rightly ordered affections to shape the character. Learning to feel as we should is enormously helpful for learning to think as we should.

p. 86

<br>

---

<br>

The identification of argument with war is so complete that if you try to suggest some alternative way of thinking about what argument is-It's an attempt to achieve mutual understanding; It's a means of clarifying our views you're almost certainly going to be denounced as a wishy-washy, namby-pamby sissy-britches.

We fixate so immovably on this notion of argument as war in part because human beings, generally speaking, are insanely competitive about everything; but also because in many arguments there truly is something to be lost, and most often what's under threat is social affiliation. Losing an argument can be a personal embarrassment, but it can also be an indication that you've sided with the wrong people, which means that you need to find a new ingroup or else learn to live with what the Marxists call "false consciousness." 

p. 97

<br>

---

<br>

The problem, of course, and sadly, is that we all have some convictions that are unsettled when they ought to be settled, and others that are settled when they ought to be unsettled. To understand this problem and begin addressing it, we need to think in terms of the old Aristotelian language of virtue and vice, in which a virtue lies midway between two opposing vices. We don't want to be, and we don't want others to be, intractably stubborn; but we don't want them to be pusillanimous and vacillating either. Tommy Lasorda, the onetime Los Angeles Dodgers manager, used to say that managing players was like holding a bird in your hands: grip it too firmly and you crush it, too loosely and it escapes and flies away. In the life of thought, holding a position is like that: there's a proper firmness of belief that lies between the extremes of rigidity and flaccidity. We don't want to be paralyzed by indecision or indifference, but like the apocryphal Keynes, we want to have the mental flexibility and honesty to adjust our views accordingly when the facts change.*

All that is difficult enough to manage, but there are further complications: we need to be able to make reliable assessments about the state of our knowledge, in such a way that when necessary we can hold back from taking any position until we learn more; and we need to accept that while knowledge may be analog, decision making is often digital, that is, binary.

p. 126

<br>

---