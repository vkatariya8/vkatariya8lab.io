---
layout: post
title: "The Scar: A Personal History of Depression and Recovery, by Mary Cregan"
description: "Excerpts from The Scar: A Personal History of Depression and Recovery, by Mary Cregan"
date: "2020-05-19"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---

Some of the best writing on depression that I have encountered. 

<br>

> The deaths of Wallace and Enke bear witness to the worst that depressive illness can do. Despite the scale of their accomplishments, they remained vulnerable to the slow disintegration of mind and self, to despair and self-loathing, and to the compulsion to die. Having gone through that uniquely horrible experience and having worried for a long time that I wouldn't live to grow old, I'm troubled when I read about high-profile suicides like these. It's as though I've lost a comrade in a shared struggle. But most deaths from depression are the ones we never hear about: those of people who went undiagnosed, or who didn't have insurance, or succumbed to drug addiction or alcoholism, or whose families didn't notice what was happening, or who got away despite the best efforts of their doctors and loved ones to help them. The profoundly depressed can't see that her thinking is distorted and, after a certain point in the downward spiral, can't hear or comprehend the people who argue that she has a life ahead if she can just hold on.

<br>

---

<br>

> Gradually, I had stopped feeling so much--so much grief, so much anger, so much shame--and had begun to feel nothing at all. I was convinced that my body was just a remnant, and an entirely pointless one since my mind and soul, whatever essences define a human being, were dead. My consciousness was in permanent exile from life as it proceeded around me. The negativity I felt about myself didn't extend to other people. Other people were good; they were just fine. They knew how to live. I was failing to grasp this most basic human instinct. It was baffling and also damning, that I should be lacking something so universal and necessary. My intense conviction that this body needed to die was the result of my utter hopelessness and inability to imagine a future, and of a belief that my own life was broken, irreparable, and over.

<br>

---

<br>

> Most people with mood disorders--and people with other kinds of psychiatric illness too, I imagine--find it hard to put their subjective experience into words. Colors, metaphors and visual images come to mind more readily than a precise verbal description. I've come to visualize depression as a series of gray tones deepening from pure white to pure black. For me, this image conveys the way depression moves subtly between darker and lighter as I feel worse and better and worse again over the passage of time. Put another way: if the healthy mind is a glass of clear water, depression is what happens if you add successive drops of ink and stir. Each addition darkens the consciousness within the mind, and the vision looking out from it.

<br>

---

<br>

> It took a long time to work all of this out, because it's very hard to see yourself clearly when depressed. The problem is that you think with your mind, but your mind is ill and untrustworthy. Your mind is your enemy. Once you've lived through several terrible episodes, you begin to get a feeling for how the thing will go. (In a story he wrote while still in college, David Foster Wallace called it "The Bad Thing", which nicely captures both the abstraction and the horror.) Eventually you realize that you can and do end up splitting yourself: a part of you can remain detached, watching the thing that is squeezing out any light and vitality within you. If a little space remains for the observing, rational consciousness, you can hold on. If not, you can get swept away in the delusion that you can never get better. Trying to kill yourself, for instance, is getting swept away. 

<br>

---

<br>

> Writing this book has made me recognize how thoroughly depression has shadowed my life. Over the years, in periods of great difficulty, feeling the paralysis of ambition and the absence of vitality, I've called myself a failure--a boring, vacant, uncreative person--instead of recognizing that depression had taken over once again. I've had too little compassion for myself, and my self-condemnation has only made things worse. Serious depression causes useless, prolonged suffering, and--given how common it is--is a wasting of human potential on an enormous scale.

<br>

---