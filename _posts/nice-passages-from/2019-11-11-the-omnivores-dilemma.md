---
layout: post
title: "The Omnivore's Dilemma, by Michael Pollan"
description: "Excerpts from The Omnivore's Dilemma, by Michael Pollan"
date: "2019-11-11"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---

Excerpts of choice from _The Omnivore's Dilemma_, by Michael Pollan.

<br>

> Another theme, or premise really, is that the way we eat represents our most profound engagement with the natural world. Daily, our eating turns nature into culture, transforming the body of the world into our bodies and minds. Agriculture has done more to reshape the natural world than anything else we humans do, both its landscapes and the composition of its flora and fauna. Our eating also constitutes a relationship with dozens of other species--plants, animals, and fungi--with which we have coevolved to the point where our fates are deeply entwined. Many of these species have evolved expressly to gratify our desires, in the intricate dance of domestication that has allowed us and them to prosper together as we could never have prospered apart. But our relationships with the wild species we eat--from the mushrooms we pick in the forest to the yeasts that leaven our bread--are no less compelling, and far more mysterious. Eating puts us in touch with all that we share with the other animals, and all that sets up apart. It defines us.

<br>

---

<br>

> [Fritz] Haber's story embodies the paradoxes of science: the double edge to our manipulations of nature, the good and evil that can flow not only from the same man but the same knowledge. Haber brought a vital new source of fertility and an awful new weapon into the world; as his biographer wrote, "[I]t's the same science and the same man doing both." Yet this dualism dividing the benefactor of agriculture from the chemical weapons maker is far too pat, for even Haber's benefaction has proven decidedly to be a mixed blessing.

<br>

---

<br>

> Liberated from the old biological constraints, the farm could now be managed on industrial principles, as a factory transforming inputs of raw material--chemical fertilizer--into outputs of corn. Since the farm no longer needs to generate and conserve its own fertility by maintaining a diversity of species, synthetic fertilizer opens the way to monoculture, allowing the farmer to bring the factory's economies of scale and mechanical efficiency to nature. If, as has sometimes been said, the discovery of agriculture represented the first fall of man from the state of nature, then the discovery of synthetic fertility is surely a second precipitous fall. Fixing nitrogen allowed the food chain to turn from the logic of biology and embrace the logic of industry. Instead of eating exclusively from the sun, humanity now began to sip petroleum.

<br>

---

<br>

> "It's all connected. This farm is more like an organism than a machine, and like any organism has its proper scale. A mouse is the size of a mouse for a good reason, and a mouse that was the size of an elephant wouldn't do very well." - Joel Salatin

<br>

---

<br>

> While we were cleaning up, scrubbing the blood off the tables and hosing down the floor, customers began arriving to pick up their chickens. This was when I began to appreciate what a morally powerful idea an open-air abattoir is. Polyface's customers know to come after noon on a chicken day, but there's nothing to prevent them from showing up earlier and watching their dinner being killed--indeed, customers are welcome to watch, and occasionally one does. More than any USDA rule or regulation, this transparency is their best assurance that the meat they're buying has been humanely and cleanly processed.

> "You can't regulate integrity," Joel is fond of saying; the only genuine accountability comes from a producer's relationship with his or her customers, and their freedom "to come out to the farm, poke around, sniff around. It after seeing how we do things they want to buy food from us, that should be none of the government's business."

<br>

---

<br>

> When you think about it, it *is* odd that something as important to our health and general well-being as food is so often sold strictly on the basis of price. The value of relationship marketing is that it allows many kinds of information besides price to travel up and down the food chain: stories as well as numbers, qualities as well as quantities, values rather than "value". And as soon as that happens people begin to make different kinds of buying decisions, motivated by criteria other than price. But instead of stories about how it was produced accompanying our food, we get bar codes--as inscrutable as the industrial food chain itself, and a fair symbol of its almost total opacity.

<br>

---

<br>

> But before the morning was out I'd begun to find a few chaterelles on my own. I began to understand what it meant to have my eyes on, and the chanterelles started to pop out of the landscape, one and then another, almost as though they were beckoning to me. So had I stumbled upon a particularly good spot or had I learned at last how to see them? Nature or nurture? There was no way of telling, though I did have the eerie experience of resurveying the very same patch of ground and finding a Siamese pair of chanterelles, bright as double egg yolks, in a spot where a moment before I could swear there had been nothing but the tan carpet of leaves. Either they had just popped up or visual perception is a lot more variable, and psychological, than we think. It is certainly ruled by expectation, because whenever I was convinced I was in a good spot the mushrooms were more likely to appear. "Seeing is believing" has it backward when it comes to hunting mushrooms; in this case, believing is seeing. My ability to see mushrooms seemed to function less like a window than a tool, a constructed and wielded thing.

<br>

---

<br>

> This forest proposes a completely different way of being in nature. The morels would just as soon I pass them by, and it will be a long time before the first berries return to this blasted landscape and declare their bright presence. It's a little like being in a foreign country: *No one knows me here!* In the forest you're encumbered by none of the agriculturist's obligations of citizenship; you feel some of the traveler's exquisite lightness of being in a place oblivious to his presence, as well as his hyperreal sense of first sight, first smell, first taste. That sense, too, of *something for nothing*, for all this is coming to you simply by dint of walking around and deploying your senses. Of course the rush of newness is usually shadowed by worry: *Am I getting lost? Should I pick that mushroom, too?*

<br>

---


