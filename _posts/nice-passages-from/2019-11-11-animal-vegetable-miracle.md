---
layout: post
title: "Animal, Vegetable, Miracle by Barbara Kingsolver"
description: "Excerpts from Animal, Vegetable, Miracle"
date: "2019-11-11"
tags: [books, nice-passages-from]
categories: [nice-passages-from]
comments: false
share: true
---

Excerpts of choice from _Animal, Vegetable, Miracle_ by Barbara Kingsolver.

> But "locally grown" is a denomination whose meaning is incorruptible. Sparing the transportation fuel, packaging, and unhealthy additives is a compelling part of the story, but the plot goes well beyond that. Local food is a handshake deal in a community gathering place. It involves farmers with first names, who show up week after week. It means an open-door policy on the field, where neighborhood buyers are welcome to come have a look, and pick their food from the vine. _Local_ is farmers growing trust.

<br>

---

<br>

> Now that I'm decades older and much less clever than I was in college, I'm getting better at facing life's routines the way my friend faces his cornfield. I haven't mastered the serene mindset on all household chores (What do you do for fun around here? *I scrub pots and pans, okay??*), but I might be getting there with cooking. Eternal is the right frame of mind for making food for a family: cooking down the tomatoes into a red-gold oregano-scented sauce for pasta. Before that, harvesting sun-ripened fruits, pinching oregano leaves from their stems, growing these things from seed--*yes*. A lifetime is what I'm after. Cooking is definitely one of the things we do for fun around here. When I'm in a blue mood I head for the kitchen. I turn the pages of my favorite cookbooks, summoning the prospective joyful noise of a shared meal. I stand over a bubbling soup, close my eyes, and inhale. From the ground up, everything about nourishment steadies my soul.

<br>

---

> Households that have lost the soul of cooking from their routines may not know what they're missing: the song of a stir-fry sizzle, the small talk of clinking measuring spoons, the yeasty scent of rising dough, the painting of flavors onto a pizza before it slides into the oven. The choreography of many people working in one kitchen is, by itself, a certain definition of family, after people have made their separate ways home to be together. The nurturing arts are more than just icing on the cake, insofar as they influence survival. We have dealt to today's kids the statistical hand of a shorter life expectancy than their parents, which would be *us*, the ones taking care of them. Our thrown-away food culture is the sole reason. By taking the faster drive, what did we save?

<br>

---
