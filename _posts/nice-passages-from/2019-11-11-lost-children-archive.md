---
layout: post
title: "Lost Children Archive, by Valeria Luiselli"
description: "Excerpts from Lost Children Archive, by Valeria Luiselli"
date: "2019-11-11"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---

Excerpts of choice from _Lost Children Archive_, by Valeria Luiselli.

<br>

> I suppose an archive gives you a kind of valley in which your thoughts can bounce back to you, transformed. You whisper intuitions and thoughts into the emptiness, hoping to hear something back. And sometimes, just sometimes, an echo does indeed return, a real reverberation of something, bouncing back with clarity when you've finally hit the right pitch and found the right surface.

<br>

---

<br>

> This last line is underlined in pencil, then circled in black ink, and also flagged in the margin with an exclamation mark. Was it me or him who underlined it? I don't remember. I do remember, though, that when I read Sontag for the first time, just like the first time I read Hannah Arendt, Emily Dickinson, and Pascal, I kept having those sudden, subtle, and possibly microchemical raptures--little lights flickering deep inside the brain tissue--that some people experience when they finally find words for a very simple and yet till then utterly unspeakable feeling. When someone else's words enter your consciousness like that, they become small conceptual light-marks. They're not necessarily illuminating. A match struck alight in a dark hallway, the lit tip of a cigarette smoked in bed at midnight, embers in a dying chimney; none of these things has enough light of its own to reveal anything. Neither do anyone's words. But sometimes a little light can make you aware of the dark, unknown space that surrounds it, of the enormous ignorance that envelops everything we think we know. And that recognition and coming to terms with darkness is more valuable than all the factual knowledge we may ever accumulate.

<br>

---

<br>

> The only thing that parents can really give their children are little knowledges: this is how you cut your own nails, this is the temperature of a real hug, this is how you untangle knots in your hair, this is how I love you. And what children give their parents, in return, is something less tangible but at the same time larger and more lasting, something like a drive to embrace life fully and understand it, on their behalf, so they can try to explain it to them, pass it down to them "with acceptance and without rancor", as James Baldwin once wrote, but also with a certain rage and fierceness. Children force parents to go out looking for a specific pulse, a gaze, a rhythm, the right way of telling the story, knowing that stories don't fix anything or save anyone but maybe make the world both more complex and more tolerable. And sometimes, just sometimes, more beautiful. Stories are a way of subtracting the future from the past, the only way of finding clarity in hindsight.

<br>

---


