---
layout: post
title: "Federer and Me, by William Skidelsky"
description: "Obsessed about Federer, Skidelsky explores his fandom in a delightful way"
date: "2019-03-14"
tags: [books, nice-passages-from]
categories: [nice-passages-from]
comments: false
share: true
---

<br>

This was a wonderful book, which no doubt arose because of the fact that I am an ardent supporter of Federer. Skidelsky mixes aspects of his Federer fandom, experiences with depression, expertise of tennis history and more with gay abandon. It is wonderful because it shows you how involved Federer is in his ultimate happiness in life. I feel the same and so I won't keep you bored with my rambling when you can read some of his writing in these choice excerpts.

---

<br>

> Although it's more romantic to depict myself as having no real say in the matter, there was a time, early on, when I think things could have gone either way. In this regard, becoming a fan is like falling in love: before the headlong plunge, there's often a period of indeterminancy, when the outlines of the thing you're feeling--this new fascination--remain unclear. At this stage, you're still a free agent: you can draw back or push on, cling to your independence or submit. Opt for the latter, and you'll soon relinquish all decision-making capability, but until that happens, a degree of agency is involved.

<br>

---

<br>

> The match, I now realize, was another milestone in my fanship: it was the first time I remember seriously caring about the outcome. Previously, when I'd watched Federer, the experience had never felt particularly fraught. I had simply relished the spectacle of his superiority. But this time it was different. Federer appeared vulnerable, genuinely threatened. Halway through the match I realized how very sad I would be if he lost. This feeling--which I suppose was a sign that I was moving into a new, more invested phase of my fanship--caught me unawares. When he hit that famous inside-in forehand to break Nadal in the final set, I remember rising to my feet and punching the air, and being surprised to find myself acting in such a manner.

<br>

---

<br>

> [...] that the one place a tennis player's weight shouldn't go when striking a ball is backwards. Yet this impression of falling away is a kind of illusion, a product of the capacity Federer's body seems to have of moving in two different directions at once. On the one hand, his torso rotates round to the left, opening right up in the process (as is standard); on the other hand, his (normally straight) arm, wrist and head all stay behind, and somehow move out towards the ball. With Federer, the forehand isn't so much a rotation, or an uncoiling, as an opening out, and this is why the movement appears to be flowing rather than muscular. Energy is dissipated rather than forced in one direction; the shot has a molten, liquid quality.

<br>

---

<br>

*(about Nadal)*

> Yet the thing about him is that he never does collapse. He goes away, recuperates, and then--incredibly--comes back stronger. With Nadal, it's always the same; that odd combination of fallibility and fortitude; an endlessly repeated cycle. Down in a rally, he comes up with an impossible pass. Down in a match, he raises his game to new heights. His powers of rebound are freakish, unprecedented; the man is unbreakable, immovable, unputdownable. You almost feel that, if his body weren't as creaky as it is, he'd start inflicting injuries upon himself, merely to reproduce the conditions of adversity in which he flourishes. Perhaps this is ultimately the point of his rituals: they're not so much to silence the voices in his head as to place yet another obstacle in his path. There has never been a champion more *burdened* than Nadal, one who makes winning appear less straightforward.

<br>

---

<br>

> Pundits talk--incessantly, infuriatingly--about the "old Federer", and what they surely mean by this is the Federer who played with the freedom of knowing that, if he played his best, no one could touch him. That Federer--for all that he has reemerged periodically in recent times--can never be fully recaptured. He is a memory, a chimeral presence. Around 2008 and 2009, the infinite became limited; there was a cruel reduction in scale. [...]
> 
> Again, though, in a larger sense, all this could be seen as necessary. The aura of perfection, of godliness, that clung to Federer during his best years needed to be dispelled. It wasn't good for the sport, maybe not good even for Federer himself. Something had to be done, and the gods of tennis duly obliged. Having (presumably) scratched their heads for some time, they sent down the one thing capable of stopping Federer--a bunglesome messenger from a future-gone-wrong, an embodiment of every crudifying technological development of the previous four decades, a player who, with one 4,000-rpm smote of his racket, could smash all Federer's artistry, his subtlety, to pieces. Nadal, one could say, was the price tennis had to pay for Federer' genius.

<br>

---

<br>

> But the similarities run deeper. Tennis, like writing, is technically complex: all those strokes to master, all those rules to obey (don't take the racket back beyond your shoulder when volleying; assume the trophy pose just before striking the serve). It's a game of endless small adjustments, a sport for tinkerers, perfectionists. At the same time, tennis is an unusually--perhaps uniquely--psychological sport. It's not just that players have to be good readers of character, able to figure out what an opponent's weaknesses are and how to exploit them, it's also that the sport itself exerts such acute mental pressures. [...] No sport is more inward-looking than tennis, more conducive to emotional turmoil, downward-spiraling patterns of thought. And in this, too, of course, it has something in common with writing.

<br>

---

<br>

> Great athletes often interact with their environments in distinctive ways. This is true of both Nadal and Federer. Nadal's special ability is one of self-transformation; when the ball isn't there, he speeds up to meet it. Federer's is the opposite: he makes the ball--or, one could say, the universe--slow down, bends it to his own stately pace. Perhaps this is finally what really divides them: one transforms himself to fit his environment; the other makes his environment fit him.

<br>

---

<br>

> Djokovic is a human spider: insubstantial of torso, all the force, the emphasis, of his body resides in those sprawling, tentacular limbs.

<br>

---

<br>

> After all, his tennis is often described as perfect (or, as his more ardent fans like to put it, "peRFect"). When people say this, they don't simply mean that his game is flawless. The sentiment is more active; they are imputing a moral quality to his tennis, suggesting that he somehow plays the game as it *should* be played. What is perfect, after all, must also be right. This helps make sense of the overblown, frankly disproportionate pain committed fans such as myself so often experience when Federer loses. Our trauma is not mainly, or only, caused by the fact that we feel sorry for him; it is more todo with our sense that some fundamental wrong, some injustice, has been perpetrated. The *New Yorker* write Nick Paumgarten accurately captured this feeling in a piece he wrote following Federer's semifinal defeat by Djokovic at the 2011 US Open. "The point is that to root for Federer is to root for a Platonic ideal", Paumgarten wrote. "It is like rooting for the truth."

<br>

---

<br>

> Success, in sport as in life, is always relative. To gauge what it means, you have to understand what it's being measured against. When the defeats come, the preceding triumphs evaporate: it's as if they'd never existed, had only ever been dreams. For the fan, a balanced reckoning of positive and negative is never achievable. When failure and disappointment come--as they inevitably will--they always hurt just the same.

<br>

---
