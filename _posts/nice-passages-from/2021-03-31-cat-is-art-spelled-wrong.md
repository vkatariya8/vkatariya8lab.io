---
layout: post
title: "Cat Is Art Spelled Wrong (a compilation of essays)"
description: "Excerpts from Cat Is Art Spelled Wrong, eds. Caroline Casey, Chris Fischbach, Sarah Schultz"
date: "2021-03-31"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---

<br>

Such pleasures recall the words of Evelyn Waugh regarding P. G. Wodehouse, whose works similarly provide us with a balm against the sadness and grief of the world. Wodehouse, Waugh observed, had made for us an "idyllic world [that] can never stale." His characters "exist in a world of pristine paradisal innocence. For Mr. Wodehouse there has been no Fall of Man... His characters have never tasted the forbidden fruit. They are still in Eden."
So it is with the feline demigods who kindly soothe our daily hurt by falling off a wall, sleeping with a pit bull, or purring in a box on the internet. Cat videos too "will continue to release future generations from a captivity that may be more irksome than our own." They too are "a world for us to live in and delight in."They are the internet's crowning achievement, a realm of universal mirth and innocent fun.

essayist: Maria Bustillos

<br>

---

<br>

Like a crowd, the internet preserves and amplifies a buzz: We watch because others do, because we saw it somewhere or were told, because it was forwarded or linked to us, because we are susceptible to listicles. But leaving our houses and assembling by the thousands to attend the Internet Cat Video Festival shows another kind of faith. In what, I wonder: Pointlessness? Loneliness? Irony? Nostalgia? Are we opting out of our desires to biggie-size our glittering consumer-grade culture and fit it in our mouths by watching videos of cats instead? Are we engaging our shared mania for mania? Do we just join a loop? Or is it where the meta and the aww can meet and coexist, where we might meet someone to coexist with?

essayist: Ander Monson

<br>

---

<br>

The mass of cat videos on YouTube has that aggregate effect, but in a way, so can any given YouTube cat video that is not too gimmicky or overcalculated. Human perception seems programmed to test our theories of mind on everything we see, to determine whether we attribute deliberation and consciousness to it or chalk it up to blind nonagency. A child skips a stone across the water, but a hill does not roll a rock down in its slope. A dog seems to act with dogged intention even when its aims are idiotic, yet it is maddeningly difficult to demarcate when a cat is doing some
thing consciously or instinctively accidentally. The gaze rebounds, in each second flipping the answer back and forth, as if we were staring at one of those diagrams that in one glance seems to be a rabbit and in the next a
duck, alternating rapidly and deteriorating as the signal continues mind, no mind, mind, no mind, duck, rabbit, duck, rabbit, duckrabbit, ducrabbit, durabbit, drabbit, drrbit, drrbbt, drrbbt, drrbbt. It's a tickle in the brain that breaks down into nonsense, and so we laugh, and our theories of mind buckle for a moment. This is the Zen of the cat video, and indeed the Zen of cats. As Lewis-Kraus's Wired article reports, studies have shown that we're not imagining it cats do indeed pay more attention to people the less we want them to, and vice versa, as if they were not so much independent as somehow bent on correcting an error in our ways.

essayist: Carl Wilson

<br>

---

<br>

How, then, did cats become the cute icon extraordinaire? Unlike Hello Kitty or Beanie Babies or Pillow Pets, not only are cats alive—already an impediment to cuteness—but they scratch and bite, use urine the 1 as a form of protest, and, perhaps worst of all, kill other cute things. They command attention and accommodation, shamelessly exerting control over an entire household. Cats are notoriously conceited, supremely confident of their dominance. When a cat deigns to offer comfort and companionship, the relationship remains of the cat's design, contingent on its capricious inclinations. As most cat lovers acknowledge, the cat's imperviousness is key to its charm. Knowing the object of our attentions will never be subjugated makes its pursuit enjoyable: In chasing the cat's exquisite not-need, we allow ourselves to need. The tension between human agendas and kitty self-determination lies at the very heart of feline devotion.

In a cat-and-mouse-game of our own invention, we attempt subjugation, and the cat resists, over and over and over. With the online cat video, how ever, our attempts at mastery have acquired their most powerful arsenal to date. Camera in hand, the cat is forced to yield its dignity, abandon its claims to privacy, and finally acquire a reputation wholly counter to its ancestral roots. The ancient cats of myth, literature, art, and even early twentieth-century popular culture bear little semblance to today's big eyed kittens. YouTube speaks a tale of catness thoroughly at odds with
feline history.

essayist: Sasha Archibald

<br>

---
