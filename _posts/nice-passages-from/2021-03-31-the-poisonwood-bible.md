---
layout: post
title: "The Poisonwood Bible, by Barbara Kingsolver"
description: "Excerpts from The Poisonwood Bible, by Barbara Kingsolver"
date: "2021-03-31"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---

<br>

I could never work out whether we were to view religion as a life-insurance policy or a life sentence. I can understand a wrathful God who'd just as soon dangle us all from a hook. And I can understand a tender, unprejudiced Jesus. But I could never quite feature the two of them living in the same house. You wind up walking on eggshells, never knowing which Tata Nzolo is home at the moment. Under that uncertain roof, where was the place for my girls? No wonder they hardly seemed to love me half the time I couldn't step in front of my husband to shelter them from his scorching light. They were expected to look straight at him and go blind.

<br>

---

<br>

It struck me what a wide world of difference there was between our sort of games "Mother May I?," "Hide and Seek" and his: "Find Food," "Recognize Poisonwood," "Build a House." And here he was a boy no older than eight or nine. He had a younger sister who carried the family's baby everywhere she went and hacked weeds with her mother in the manioc field. I could see that the whole idea and business of Childhood was nothing guaranteed. It seemed to me, in fact, like something more or less invented by white people and stuck onto the front end of grown-up life like a frill on a dress. For the first time ever I felt a stirring of anger against my father for making me a white preacher's child from Georgia. This wasn't my fault. bit my lip and labored on my own small house under the guava tree, but beside the perfect talents of Pascal, my own hands lumbered like pale flippers on a walrus out of its element. My embarrassment ran scarlet and deep, hidden under my clothes.

<br>

---

<br>

As long as I kept moving, my grief streamed out behind me like a swimmer's long hair in water. I knew the weight was there but it didn't touch me. Only when I stopped did the slick, dark stuff of it come floating around my face, catching my arms and throat till I began to drown. So I just didn't stop. The substance of grief is not imaginary. It's as real as rope or the absence of air, and like both those things it can kill. My body understood there was no safe place for me to be.

<br>

---

<br>

But his kind will always lose in the end. I know this, and now I know why. Whether it's wife or nation they occupy, their mistake is the same: they stand still, and their stake moves underneath them. The Pharaoh died, says Exodus, and the children of Israel sighed by reason of their bondage. Chains rattle, rivers roll, animals startle and bolt, forests inspire and expand, babies stretch open-mouthed from the womb, new seedlings arch their necks and creep forward into the light. Even a language won't stand still. A territory is only possessed for a moment in time. They stake everything on that moment, posing for photographs while planting the flag, casting themselves in bronze. Washington crossing the Delaware. The capture of Okinawa. They're desperate to hang on.

But they can't. Even before the flagpole begins to peel and splinter, the ground underneath arches and slides forward into its own new destiny. It may bear the marks of boots on its back, but those marks become the possessions of the land. What does Okinawa remember of its fall? Forbidden to make engines of war, Japan made automobiles instead, and won the world. It all moves on. The great Delaware rolls on, while Mr. Washington himself is no longer even what you'd call good compost. The Congo River, being of a different temperament, drowned most of its conquerors outright. In Congo a slashed jungle quickly becomes a field of flowers, and scars become the ornaments of particular face. Call it oppression, complicity, stupefaction, call it what you like, it doesn't matter. Africa swallowed the conqueror's music and sang a new song of her own.

<br>

---
