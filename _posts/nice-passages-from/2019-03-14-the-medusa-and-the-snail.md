---
layout: post
title: "The Medusa and the Snail, by Lewis Thomas"
description: "A series of lovely vignettes and essays by biologist Lewis Thomas"
date: "2019-03-14"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---


> But I came away from the zoo with something, a piece of news about myself: I am coded, somehow, for otters and beavers. I exhibit instinctive behavior in their presence, when they are displayed close at hand behind glass, simultaneously below water and at the surface. I have receptors for this display. Beavers and otters possess a "releaser" for me, in the terminology of ethology, and the releasing was my experience. What was released? Behavior. What behavior? Standing, swiveling flabbergasted, feeling exultation and a rush of friendship. I could not, as the result of the transaction, tell you anything more about beavers and otters than you already know. I learned nothing new about them. Only about me, and I suspect also about you, maybe about human beings at large: we are endowed with genes which code out our reaction to beavers and otters, maybe our reaction to each other as well.


---


> The earth holds together, its tissues cohere, and it has the look of a structure that really would make comprehensible sense if only we knew enough about it. From a little way off, photographed from the moon, it seems to be a kind of organism. Looked at over its whole time, it is plainly in the process of developing, like an enormous embryo. It is, for all its stupendous size and the numberless units and infinite variety of its life forms, coherent. Every tissue is linked for its viability to every other tissue; it gets along by symbiosis, and the invention of new modes of symbiotic coupling is a fundamental process in its embryogenesis. We have no rules for the evolution of this kind of life. We have learned a lot, and in some biomathematical detail, about the laws governing the evolution of individual species on the earth, but no Darwin has yet emerged to take account of the orderly, coordinated growth and differentiation of the whole astonishing system, much less its seemingly permanent survival. It makes an interesting problem: how do mechanisms that seem to be governed entirely by chance and randomness bring into existence new species which fit so neatly and precisely, and usefully, as though they were the cells of an organism? This is a wonderful puzzle.


---


> But security is the last thing we feel entitled to feel. We are, perhaps uniquely among the earth's creatures, the worrying animal. We worry away our lives, fearing the future, discontent with the present, unable to take in the idea of dying, unable to sit still. We deserve a better pres, in my view. We have always had a strong hunch about our origin, which does us credit; from the oldest language we know, the Indo-European tongue, we took the word for earth--*Dhghem*--and turned it into "humus" and "human"; "humble" too, which does us more credit. We are by all odds the most persistently and obsessively social of all species, more dependent on each other than the famous social insects, and really, when you look at us, infinitely more imaginative and deft and social living. [...]
> 
> There is nothing at all absurd about the human condition. We matter. It seems to me a good guess, hazarded by a good many people who have thought about it, that we may be engaged in the formation of something like a mind for the life of this planet. If this is so, we are still at the most primitive stage, still fumbling with language and thinking, but infinitely capacitated for the future. Looked at this way, it is remarkable that we've come as far as we have in so short a period, really no time at all as geologists measure time. We are the newest, the youngest, and the brightest thing around.


---


> And maybe, given the fundamental instability of the molecule, it had to turn out this way. After all, if you have a mechanism designed to keep changing the ways of living, and if all the new forms have to fit together as they plainly do, and if every improvised new gene representing an embellishment in an individual is likely to be selected for the species, and if you have enough time, maybe the system is simply bound to develop brains sooner or later, and awareness.
> 
> Biology needs a better word than "error" for the driving force in evolution. Or maybe "error" will do after all, when you remember that it came from an old root meaning to wander about, looking for something.


---
