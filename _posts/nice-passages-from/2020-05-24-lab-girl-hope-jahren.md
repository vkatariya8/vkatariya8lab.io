---
layout: post
title: "Lab Girl, by Hope Jahren"
description: "Excerpts from Lab Girl, by Hope Jahren"
date: "2020-05-24"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---

<br>

Now _you_ ask a question about _your_ leaf.
    
Guess what? You are now a scientist. People will tell you that you have to know math to be a scientist, or physics or chemistry. They're wrong. That's like saying you have to know how to knit to be a housewife, or that you have to know Latin to study the Bible. Sure, it helps, but there will be time for that. What comes first is a question, and you're already there. It's not nearly as involved as people make it out to be.

So let me tell you some stories, one scientist to another.

<br>

---

<br>

People are like plants: they grow toward the light. I chose science because science gave me what I needed--a home as defined in the most literal sense: a safe place to be.

<br>

---

<br>

Time has also changed me, my perception of my tree, and my perception of my tree's perception of itself. Science has taught me that everything is more complicated than we first assume, and being able to derive happiness from discovery is a recipe for a beautiful life. It has also convinced me that carefully writing everything down is the only real defense we have against forgetting something important that once was and is no more, including the spruce tree that should have outlived me but did not.

<br>

---

<br>

I was the only person in an infinite exploding universe who knew that this powder was made of opal. In a wide, wide world, full of unimaginable numbers of people, I was--in addition to being small and insignificant--special. I was not only a quirkly bundle of genes, but I was also unique existentially, because of the tiny detail that I knew about Creation, because of what I had seen and then understood. Until I phoned someone, the concrete knowledge that opal was the mineral that fortified each seed on each hackberry tree was mine alone. Whether or not this was something worth knowing seemed another problem for another day. I stood and absorbed this revelation as my life turned a page, and my first scientific discovery shone, as even the cheapest plastic toy does when it is new.

<br>

---

<br>

The creative process born from these necessities gives rise to delightfully quirky creations, unique as their creators. Like all art, they are a product of their period and attempt to address the issues of their age. Also like art, they appear outmoded and antiquated when viewed from the future that they helped create. Yet, there is a singular fascination to be indulged when we stop and stare at the piecework of previous scientists' hands, amazed over the care taken with the peripheral elements, just as we are dazzled by the hundred tiny brushstrokes that magically agglomerate into one small boat on the horizon within a pointillistic painting.

<br>

---

<br>

Ed's suggestion that the Earth's ocean chemistry could be reset completely was a dangerous idea when he was young, and he had stayed up nights to study while the people he knew were watching Joe DiMaggio and arguing about the McCarthy trials. Forty years later his idea was one that I could take for granted as I dared my way into my own ambiguous future. It was kind of tragic, I reflected, that we all spent our lives working but never really got good at our work, or even finished it. The purpose instead was for me to stand on the rock that he had thrown into the rushing river, bend and claw another rock from the bottom, and then cast it down a bit further and hope it would be a useful next step for some person with whom Providence might allow me to cross paths.

<br>

---

<br>

This funk had dragged on, however, and I couldn't help but imagine what his week of mourning in California must have been like. How the coming of dusk dissolves the laces of the splint that holds you together during the day, and the desperate sadness that follows can be anesthesized only with sleep. The heaviness of opening your eyes the next morning when you realize you've begun another day of grief, so pervasive that it removes even the taste from the food that you eat. I knew that when someone you love has died, you feel that you have also. And I knew that there was nothing that I, or anybody else, could do to fix it.

<br>

---
