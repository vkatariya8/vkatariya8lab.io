---
layout: post
title: "Braiding Sweetgrass, by Robin Wall Kimmerer"
description: "Excerpts from Braiding Sweetgrass, by Robin Wall Kimerrer"
date: "2019-11-11"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---

Excerpts of choice from _Braiding Sweetgrass_, by Robin Wall Kimmerer.

<br>

> Even now, after more than fifty Strawberry Moons, finding a patch of wild strawberries still touches me with a sensation of surprise, a feeling of unworthiness and gratitude for the generosity and kindness that comes with an unexpected gift all wrapped in red and green. "Really? For me? Oh, you shouldn't have." After fifty years they still raise the question of how to respond to their generosity. Sometimes it feels like a silly question with a very simple answer: eat them.

<br>

---

<br>

> Strawberries first shaped my view of a world full of gifts simply scattered at your feet. A gift comes to you through no action of your own, free, having moved toward you without your beckoning. It is not a reward; you cannot earn it, or call it to you, or even deserve it. And yet it appears. Your only role is to be open-eyed and present. Gifts exist in a realm of humility and mystery--as with random acts of kindness, we do not know their source.

<br>

---

<br>

> When botanists go walking the forests and fields looking for plants, we say we are going on a foray. When writers do the same, we should call it a *metaphoray*, and the land is rich in both.

<br>

---
<br>

<center><i>The Three Sisters</i></center>

> It should be them who tell this story. Corn leaves rustle with a signature sound, a papery conversation with each other and the breeze. On a hot day in July--when the corn can grow six inches in a single day--there is a squeak of internodes expanding, stretching the stem toward the light. Leaves escape their sheaths with a drawn-out creak and sometimes, when all is still, you can hear the sudden pop of ruptured pith when water-filled cells become too large and turgid for the confines of the stem. There are the sounds of being, but they are not the voice.

> The beans must make a caressing sound, a tiny hiss as a soft-haired leader twines around the scabrous stem of corn. Surfaces vibrate delicately against each other, tendrils pulse as they cinch around a stem, something only a nearby flea beetle could hear. But this is not the song of beans.

> I've lain among ripening pumpkins and heard creaking as the parasol leaves rock back and forth, tethered by their tendrils, wind lifting their edges and easing them down again. A microphone in the hollow of a swelling pumpkin would reveal the pop of seeds expanding and the rush of water filling succulent orange flesh. These are sounds, but not the story. Plants tell their stories not by what they say, but by what they do.

<br>

---

<br>

> Against the black humus, colors stand out like neon lights on a dark wet street. Juicy school bus orange, goldthread roots crisscross the ground. A web of creamy roots, each as thick as a pencil, connects all the sarsapillas. Chris says right away, "It looks like a map." With roads of different colors and sizes, it really does. There are interstates of heavy red roots whose origins I do not know. We tug on one and few feet away a blueberry bush jounces in reply. White tubers of Canada mayflower are connected by translucent threads like county roads between villages. A mycelial fan of pale yellow spreads out from a clump of dark organic matter, like the small dead-end streets of a cul-de-sac. A great dense metropolis of fibrous brown roots emanates from a young hemlock. They all have their hands in it now, tracing the lines, trying to match the root colors to the aboveground plants, reading the map of the world.

<br>

---

<br>

> Outside the door of his cabin, the circle of young cedars look like women in green shawls, beaded with raindrops catching the light, graceful dancers in feathery fringe that sways with their steps. They spread their branches wide, opening the circle, inviting us to be part of the dance of regeneration. Clumsy at first, from generations of sitting on the sidelines, we stumble until we find the rhythm. We know these steps from deep memory, handed down from Skywoman, reclaiming out responsibility as cocreators. Here in a homemade forest, poets, writers, scientists, foresters, shovels, seeds, elk, and alder join in the circle with Mother Cedar, dancing the old-growth children into being. We're all invited. Pick up a shovel and join the dance.

<br>

---

<br>

> The reflecting surface of the pool is textured with their signatures, each one different in pace and resonance. Every drip it seems is changed by its relationship with life, whether it encounters moss or maple or fir bark or my hair. And we think of it as simply rain, as if it were one thing, as if we understood it. I think that moss knows rain better than we do, and so do maples. Maybe there is no such thing as rain; there are only raindrops, each with its own story.

<br>

---
