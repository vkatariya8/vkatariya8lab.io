---
layout: post
title: "The Earthsea Cycle, by Ursula K. Le Guin"
description: "Excerpts from Ursula K. Le Guin's Earthsea Cycle"
date: "2018-04-12"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---

<center><i>from The Farthest Shore (book 3)</i></center>

<br>

> “But the dragons,” said Arren. “Do they not do great evil? Are they innocent?” “The dragons! The dragons are avaricious, insatiable, treacherous; without pity, without remorse. But are they evil? Who am I, to judge the acts of dragons? . . . They are wiser than men are. It is with them as with dreams, Arren. We men dream dreams, we work magic, we do good, we do evil. The dragons do not dream. They are dreams. They do not work magic: it is their substance, their being. They do not do; they are.”

<br>

---

<br>

<center><i>from Tales from Earthsea (book 5)</i></center>

<br>

> The danger in trying to do good is that the mind comes to confuse the intent of goodness with the act of doing things well.

<br>

---

<br>

<center><i>from The Other Wind (book 6)</i></center>

<br>

> Alder hesitated. “I’ve thought it might be so. But it seems . . . shameless to think it. We loved each other, more than I can say we loved each other, but was our love greater than any other before us? Was it greater than Morred’s and Elfarran’s?” “Maybe not less.” “How can that be?” Sparrowhawk looked at him as if saluting something, and answered him with a care that made Alder feel honored. “Well,” he said slowly, “sometimes there’s a passion that comes in its springtime to ill fate or death. And because it ends in its beauty, it’s what the harpers sing of and the poets make stories of: the love that escapes the years. That was the love of the Young King and Elfarran. That was your love, Hara. It wasn’t greater than Morred’s, but was his greater than yours?” Alder said nothing, pondering. “There’s no less or greater in an absolute thing,” Sparrowhawk said. “All or nothing at all, the true lover says, and that’s the truth of it. My love will never die, he says. He claims eternity. And rightly. How can it die when it’s life itself? What do we know of eternity but the glimpse we get of it when we enter in that bond?”

<br>

> “I think,” Tehanu said in her soft, strange voice, “that when I die, I can breathe back the breath that made me live. I can give back to the world all that I didn’t do. All that I might have been and couldn’t be. All the choices I didn’t make. All the things I lost and spent and wasted. I can give them back to the world. To the lives that haven’t been lived yet. That will be my gift back to the world that gave me the life I did live, the love I loved, the breath I breathed.”

<br>

> He had said something like that to Sparrowhawk. “It’s all beyond me,” he had said. The old man looked at him a while and then, calling him by his true name, said, “The world’s vast and strange, Hara, but no vaster and no stranger than our minds are. Think of that sometimes.”


<br>

---