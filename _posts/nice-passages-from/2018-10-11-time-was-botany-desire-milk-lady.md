---
layout: post
title: "Time Was, The Botany of Desire, and The Milk-Lady of Bangalore"
description: "Excerpts from three books"
date: "2018-10-11"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---

<br>

<center><h3><b><i>Time Was</i>, by Ian McDonald</b></h3></center>

<br>

This is a book about two (male) lovers whose lives are sundered apart by a quantum physics experiment gone wrong. It was tiny (~150 pages) and quick, but also poignant and with a nonlinear time structuring, just what I wanted. I read it quickly.

<br>

> I have never been able to resist the word “but”. It’s the ragged edge of the photograph, the texture of a provenance disturbing the flat perfection of a book.

<br>

---

<br>

> If you want to write, you must write experience. What is to be that thing. There is everything in a moment.    
> My experience? Love, as suddenly it leaves me gasping for breath, so sharp it is a spear run through my belly.

<br>

---

<br>

> Now I understand. This is what poetry is for. This is why it exists. No gods, no muses, no inspiration, only the need to find words, syntax, structure and meter for feelings that do not go into words.  
> Emotions have no definition other than themselves. They are irreducible, the atoms of sensation. All written art is an attempt to communicate what it is to feel, to ask the terrifying question: Is what I experience in my head the same as what you experience? Terrifying because we can never know for certain. We hope; we risk.

<br>

---

<br>

> I was vertiginous with wonder. That is the only word in our language, but it cannot convey the emotion of those tectonic truths closing with a sound like trumpets, the physical sense of being at the same time huge and small, a feeling like acceleration of the universe receding and at the same time racing in towards me.

<br>

---

<br>

<center><h3><b><i>The Botany of Desire</i>, Michael Pollan</b></h3></center>

<br>

The book's subtitle is, "A plant's eye view of the world". I loved it so much. It describes the journey of humanity through history from the point of view of our partnership with/domestication of four plants: apples, tulips, marijuana, and potatoes. Extremely well researched, eloquently yet straightforwardly written; this is what a nonfiction book should be. I learnt a lot too, about the tulipomania in the Netherlands, about the cannabinoid network of chemicals in our brain, about the realities, both positive and negatives, about monoculture crops and genetic modification.

<br>

> This stands for that: flowers by their very nature traffic in a kind of metaphor, so that even a meadow of wildflowers brims with meanings not of our making. Move into the garden, however, and the meanings only multiply as the flowers take aim not only at the bee’s or the bat’s or the butterfly’s obscure notions of the good or the beautiful, but at ours as well. Sometime long ago the flower’s gift for metaphor crossed with our own, and the offspring of that match, that miraculous symbiosis of desire, are the flowers of the garden.

<br>

---

<br>

> So the flowers begot us, their greatest admirers. In time human desire entered into the natural history of the flower, and the flower did what it has always done: made itself still more beautiful in the eyes of this animal, folding into its very being even the most improbable of our notions and tropes. Now came roses that resembled aroused nymphs, tulip petals I noted shape of daggers, peonies bering the scent of women. We in turn did our part, multiplying the flowers beyond reason, moving their seeds around the planet, writing books to spread their fame and ensure their happiness. For the flower it was the same old story, another grand co-evolutionary bargain with a willing, slightly credulous animal—a good deal on the whole, though not nearly as good as the earlier bargain with the bees.
>
> And what about us? How did we make out? We did very well by the flower. There were, of course, the pleasures to the senses, the sustenance of their fruit and seeds, and the vast store of new metaphor. But we gazed even farther into the blossom of a flower and found something more: the crucible of beauty, if not art, and maybe even a glimpse into the meaning of life. For look into a flower, and what do you see? Into the very heart of nature’s double nature—that is, the contending energies of creation and dissolution, the spiring toward complex form and the tidal pull away from it. Apollo and Dionysus were names the Greeks gave to these two faces of nature, and nowhere in nature is their contest as plain or as poignant as it is in the beauty of a flower and its rapid passing. There, the achievement of order against all odds and its blithe abandonment. There, the perfection of art and the blind flux of nature. There, somehow, both transcendence and necessity. Could that be it—right there, in a flower—the meaning of life?

<br>

---

<br>

> To my eye, there are few sights in nature quite as stirring as fresh rows of vegetable seedlings rising like a green city on the spring ground. […] The sublimities of wilderness have their place, okay, and their legions of American poets, God knows, but I want to speak a word here for the satisfactions of the ordered earth. I’d call it the Agricultural Sublime if that didn’t sound too much like an oxymoron. 
>
> Which it probably is. The experience of the sublime is all about nature having her way with us, about the sensation of awe before her power, about feeling small. What I’m talking about is the opposite, and admittedly more dubious, satisfaction of having our way with nature: …
>
> [...]
>
> These days the sublime is mostly a kind of vacation, in both a literal and a moral sense. After all, who has a bad word to say about wilderness anymore? By comparison, this other impulse, the desire to exert our control over nature’s wildness, bristles with ambiguity. We’re unsure about our power in nature, its legitimacy, and its reality, and rightly so. Perhaps more than most, the farmer or the gardener understands that his control is always something of a fiction, depending as it does on luck and weather and much else that is beyond his control. It is only the suspension of disbelief that allows him to plant again every spring, to wade out in the season’s uncertainties.

<br>

---

<br>

> The Irish also found that they could grow these potatoes with a bare minimum of labor or tools, in something called a “lazy bed”. […] No plowed earth, no rows, and certainly no Agricultural Sublime—a damnable defect in English eyes. Potato growing looked nothing like agriculture, provided none of the Apollonian satisfactions of an orderly field of grain, no marital ranks of golden wheat ripening in the sun. Wheat pointed up, to the sun and civilization; the potato pointed down. Potatoes were chthonic, forming their undifferentiated brown tubers unseen beneath the ground, throwing a slovenly flop of vines above.

<br>

---

<br>

<center><h3><b><i>The Milk-Lady of Bangalore</i>, Shoba Narayan</b></h3></center>

<br>

This was a delight. Narayan studied and lived in the US before returning to Bangalore, in India. She starts off by first trying out fresh milk from cows kept near her home. It escalates quickly: soon she finds herself _buying_ a cow, and more.

<br>

> As always, Sarala has an answer for everything, “Cows are like Buddha, Madam,” she says. “They can meditate for hours. Just sitting in one spot. Absorbing the sun’s rays.”
>
> I thought only native cows absorbed the sun’s rays.
>
> “All cows absorb the sun’s rays,” says Sarala. “Only native cows know what to do with them. Look at these HF cows. They are happily relaxing under the sun, as if this were a beach.”

<br>

---

<br>

> “So what do you do” Try out the cow to see if it brings you luck?” I ask jokingly.
>
> To my surprise, Johnson nods. “That’s how it used to be done in the old days,” he says. “Anytime you bought a valuable object, be it a diamond or a cow, you’d keep it at home for a few days to see what effect it had on your family. If it brought bad luck, you could return the cow, no questions asked. Because the same cow would bring good luck to a another family.”
>
> "What is called ‘right fit’ in the corporate world,” I murmur.
>
> Sarala tells me about whorls on the underbelly of the cow. Johnson’s expertise has made her insecure. She feels that she to prove herself.
>
> “Lots of people use whorls as a reason to discard cows. I am not one of those,” she says, frowning disapprovingly at Johnson as if he were one of those offenders. “Whorls are like a dish antenna, or a blueprint. They tell you a lot about the nervous system and energy flow in the animal.”

<br>

---

<br>

> But there is more, Johnson tells us. The real reason for his stubbornness is that he doesn’t want to sell his cow to us city folks who live far away.
>
> Sarala understands. “Madam, you are a mother,” she says. “If you had a choice between marrying off your daughter to someone from the next town and someone from North India, wouldn’t you choose the man close by? So that you can at least go and visit your girl once a month. The same with this cow. The seller may not have the money to maintain her but he wants to see her, make sure she is okay. If he sells to us and we take her to Bangalore, he knows that he will never see his cow again. Why would he do that?”

<br>

---
