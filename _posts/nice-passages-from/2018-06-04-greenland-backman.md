---
layout: post
title: "Greenland and Frederick Backman"
description: "Choice excerpts from two books"
date: "2018-06-04"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---

<center><h3><b>A Wilder Time, by William Glassley</b></h3></center>

<br>

This is a book about adventures into Greenland. I read only the first ten or fifteen pages and for some reason didn't finish the book. On reviewing these excerpts, I should check it out from the library again next time I go.

<br>

> Destinations, whether new or old, are expectations shrouded in imaginary landscapes. We set off with ideas for adventures that we hope might materialize and we imagine pathways to things we fear but secretly wish to confront. We think of our destination as an end point to a journey, but its reality is seldom that. Destinations may also evolve into portals that devour expectations, immersing us in the inconceivable. So it is for me when I travel into the Greenland wilderness.

<br>

---

<br>

> I now understand that wilderness is as much story as it is place. Untouched lands provide inspiration and nourish our imagination with mysteries and connections impossible to conceive anywhere else. The depth of their richness, the complexity of their structure are beyond common experience. Wilderness is the primordial heart of what we conceive as soul, and as a consequence, it must be accepted as a version of home. For me, Greenland has been the landscape that embodies that lesson. Ironically, perhaps, it was the pursuit of quantitative, objective observations that exposed the emotional truths contained in wild places.

<br>

---

<br>

> All that we see is surface. What we perceive as experience derives from light reflected, a product of events that have flowed to the present and become, in a moment, a shape seen. Life teaches us to extract texture and form, weight and warmth from that impression.

<br>

---

<br>

<center><h3><b>The Deal of a Lifetime, Fredrik Backman</b></h3></center>

<br>

A small novella that I breezed through. Wouldn't really recommend, although this following bit stuck with me (couple with [this](http://theoatmeal.com/comics/unhappy)). More background would spoil things for you if you're going to read the book.

<br>

> When the doctor gave me the diagnosis, I didn’t have an awakening, I just did my accounts. Everything I’d built, the footprints I’ve left behind. Weak people always look at people like me and say, “He’s rich, but is he happy?” As though that was a relevant measure of anything. Happiness is for children and animals, it doesn’t have any biological function. Happy people don’t create anything, their world is one without art and music and skyscrapers, without discoveries and innovations. All leaders, all of your heroes, they’ve been obsessed. Happy people don’t get obsessed, they don’t devote their lives to curing illnesses or making planes take off. The happy leave nothing behind. They live for the sake of living, they’re only on earth as consumers. Not me.

<br>

---

<br>

> I didn’t answer for quite some time. I thought about whether you would have done it, given your life for someone else. You probably would. Because you’re your mother’s son, and she’s already given a life. The one she could have lived if she hadn’t lived for you and for me. 
>
> I turned to the woman. “I’ve sat here watching him every evening since I got sick.”
>
> She nodded. “I know.”
>
> I knew that she knew. I’d understood that much by now. “Every night, I wondered whether it was possible to change a person.”
>
> “What did you conclude?”
>
> “That we are who we are."

<br>

---

