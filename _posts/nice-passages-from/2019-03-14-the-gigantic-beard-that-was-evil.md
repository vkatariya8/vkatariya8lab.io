---
layout: post
title: "The Gigantic Beard that Became Evil, by Stephen Collins"
description: "Surreal and subversive graphic novel"
date: "2019-03-14"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---

What a great graphic novel. I love this format.

* * *
<br>

![]({{"/assets/the-gigantic-beard-that-was-evil/1.png" | absolute_url}}){:class="img-responsive" .center-image}

<br>

* * *

<br>

![]({{"/assets/the-gigantic-beard-that-was-evil/2.png" | absolute_url}}){:class="img-responsive" .center-image}

<br>

* * *

<br>

![]({{"/assets/the-gigantic-beard-that-was-evil/3.png" | absolute_url}}){:class="img-responsive" .center-image}

<br>

* * *

<br>

![]({{"/assets/the-gigantic-beard-that-was-evil/4.png" | absolute_url}}){:class="img-responsive" .center-image}

![]({{"/assets/the-gigantic-beard-that-was-evil/5.png" | absolute_url}}){:class="img-responsive" .center-image}

<br>

* * *

<br>

![]({{"/assets/the-gigantic-beard-that-was-evil/6.png" | absolute_url}}){:class="img-responsive" .center-image}

<br>

* * *
