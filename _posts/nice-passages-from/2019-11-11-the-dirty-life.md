---
layout: post
title: "The Dirty Life, by Kristin Kimball"
description: "Excerpts from The Dirty Life, by Kristin Kimball"
date: "2019-11-11"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---

Excerpts of choice from _The Dirty Life_ by Kristin Kimball.

<br>

> He had recently turned against the word *should*, and doing so had made him a happier person. He found the market economy and its anonymous exchange boring. He'd like to imagine a farm where no money traded hands, only goodwill and favors. He had a theory that you had to start out by giving stuff away--preferably big stuff, worth, he figured, about a thousand dollars. At first, he said, people are discomfited by such a big gift. They try to make it up to you, by giving you something big in return. And then you give them something else, and they give you something else, and pretty soon nobody is keeping score. There is simply a flow of things from the place of excess to the place of need. It's personal, it's satisfying, and everyone feels good about it. This guy is completely nuts, I thought. *But what if he's right?*

<br>

---

<br>

> He became obsessed with rolling fresh sushi, and when he had a crush on a girl in middle school he cooked her a seven-course dinner. Eventually he ditched cookbooks and went freelance, adhering to a few simple principles: keep your knives sharp, taste everything, and don't be stingy with the salt. His love of food was part of what eventually led him to farming. The only way he'd be able to afford the quality of food he craved, he said, was to become a banker or to grow it himself, and he couldn't sit still long enough to be a banker.

<br>

---

<br>

> The shovel hadn't encountered any rocks, and the soil was the color of coffee, and its texture wasn't anything like the clay we'd uncovered earlier. He squeezed it in his hand, stroked his thumb over it, smelled it, and finally reached out his tongue and tasted it. It was a silty loam, a soil so rich and good it makes a farmer weep, and it ran for a quarter mile along the southern edge of the farm before turning once again to clay.

> I think Mark fell in love with the land at that moment, with the same certainty and speed he had fallen in love with me. From then on, there was no question in his mind that this would be our home.

<br>

---

<br>

> When we would talk about our future in private, I would ask Mark if he really thought we had a chance. Of course we had a chance, he'd say, and anyway, it didn't matter if this venture failed. In his view, we were already a success, because we were doing something hard and it was something that mattered to us. You don't measure things like that with words like _success_ or _failure_, he said. Satisfaction comes from trying hard things and then going on to the next hard thing, regardless of the outcome. What mattered was whether or not you were moving in a direction you thought was right. This sounded extremely fishy to me.

<br>

---

<br>

> The auctioneer began his pitch for the chairs, describing them as lovingly as if they had come directly from under his own mother's kitchen table. Shopping is a simple transaction--do I want this thing at this given price?--but an auction is relative: Do I want this thing more than the man standing next to me wants it? How much more? It's a party, a casino, a circus, or a concert, and the auctioneer is its host, its ringmaster, its conductor. The bidding began, the numbers rolling off his tongue, elided and almost incomprehensible, sandwiched between meaningless syllables and corny one-liners. 

<br>

---

<br>

> Of all the confounding things I encountered that first year, the heat of decomposition--its intensity and duration--was the most surprising, the one that made me want to slap my knee and say, Who knew? That heat comes from the action of hordes or organisms, some so tiny billions can live in a tablespoon of soil. They are in there, eating and multiplying and dying, feeding on and releasing the energy that the larger organisms--the plants and the animals--stored up in _their_ time, energy that came, originally, from the sun. I think it's worth it, for wonder's sake, to stick your hand in a compost pile in winter and be burned by a series of suns that last set the summer before.

<br>

---

<br>

> When I think of it now, I can see that our wedding day was exactly like our marriage, and like our farm, both exquisite and untidy, sublime and untamed. What I knew even then, though, in the middle of the chaos, was that the love at its center was not just the small human love between Mark and me. It was an expression of a larger loving-kindness, and, when I remember it, I have the feeling of being held in the hands of our friends, family, community, and whatever mysterious force made the fields yield abundant food. It is the feeling of falling, and of being gently caught.

<br>

---


