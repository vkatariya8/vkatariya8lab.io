---
layout: post
title: "How to Catch a Mole, by Marc Hamer"
description: "Excerpts from How to Catch a Mole, by Marc Hamer"
date: "2020-05-24"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---

<br>

Scything too creates a pleasing sound, a long *swishh* with each stroke. It has a good rhythm: swinging from the waist, cutting from the right to the left with relaxed arms outstretched, and striding slowly forward step by step, cutting a swathe up to eight feet wide, and leaving a neat windrow on my left as the stalks fall off the three-foot-long blade. *Swishh*, step, *swishh*, step, *swishh*. Without my even trying, it all ends up co-ordinated with my breathing. *In* as I swing back and step, *out* as I swing for the cut. Long and slow. [...]

It is a human process, and the tools are simple and brown and honest. I have grown old with these tools: they are handmade of wood, steel and stone, and they have grown old with me and have moulded to my hand. I have a relationship with tools like this: I feel that all the things in the world that I touch are touching me back.

<br>

---

<br>

I catch moles for money, and it keeps me busy when the gardens are resting. But of course there are personal reasons that make somebody attracted to this kind of work. When I tell people at parties how I earn my money they laugh. Not that I go to many parties. To people of the towns, understandably, molecatching is some kind of music-hall joke, something from the colorful rustic past, like being a chimney sweep or a comedic mechanical from *A Midsummer Night's Dream*.

When they stop laughing they become curious and ask lots of questions, mostly about killing things. When I tell them I have been a vegetarian for fifty years they show me their confused face. Things don't seem to add up. Life is rarely as neat and tidy as we would like. I prefer it that way. Reason is just one of the many important ways of experiencing the world.

<br>

---

<br>

When I am out in the countryside, walking or hunting, I become solitary and leave my man-nature behind. I become a different kind of creature: something more fluid, free, adaptable and instinctive. This is something that developed in me when I was young and living in the wild. Living moment to moment with no thought or feeling, no ideas or obvious mental process going on, just instinct, an awareness of the field, but not a separate awareness of myself being in the field. We seem to become the same thing. Me, the field, the weather, the scents flowing in and out. Tracking an animal requires this level of awareness, and losing myself like this is an important part of my existence. Not knowing, not thinking, is for me a most desirable state of awareness. Any thoughts I might have seem to be just a reflection upon that awareness, a step away from direct experience that insulates me from the electricity of the moment.

<br>

---

<br>

By the gate there are bare willow trees with a small flock of tiny long-tailed birds flicking through the dangling yellow catkins - black and beige, perhaps, a flash of pink or green? They are too fast, and in the dawn under the trees it is too dark to see them properly, but I think I know them. My memory for the names of things is not what it was: it doesn't seem important for me to try and remember. The answer will come of its own accord, or it won't. Words have a different existence to the things they name: they live in different places, have different lives.

<br>

---

<br>

They are seeking evidence of previous civilization without disturbing the site: they call it 'moleology'. Sometimes in molehills I find bits of nylon clothing and baler twine, or the aluminum tops of rusted drinks cans; it depresses me that these un-natural, man-made things refuse to decay and join with the earth. The only permanent things about man are his waste. Natural things decay. There is a bittersweet state of existence that all natural things go through, a stage when they stop being what they were and start being something else. I think I am at that point.

<br>

---

<br>

At some point on a long walk you stop being who you thought you were, but you don't question it because the questions stop too. I became for a while just steps and breathing. Walking and resting. Everything fell away: all the small nonsenses of life that had seemed so large. My identity destroyed. My individuality killed as I became one with everything. Walking for a long time dissolved all of my negative and positive feelings about myself and about others. I became completely empty: there were no anchors, nothing at all to hang on to. What was left was just acceptance and love of what is. Anything else just seemed ridiculous. The 'self' that I had learned through childhood to present to the world was lost, and I have left it behind now for long enough that it has become impossible to construct any kind of solid immutable 'self', because I don't know how to do it. I am always aware from moment to moment that behind the mask there is nothing, and that very silence is the most wonderful, the most perfect, thing there is.

<br>

---

<br>

Having worked all my life, created a family, discovered a home, I feel as secure as a working-class man ever feels, and I feel a sense of equality again with the crow and the toad and the hawthorn, with the rain and wind. I am them and they are me. I lost my self-importance early on and do not want to differentiate myself from the world around me. I am just another animal, another tree, another wild flower in the meadow among billions of others, each unique in their own way, each just like the others in other ways, each one just another expression of nature trying to survive. There is something deeply magnificent in being just ordinary.

<br>

---

<br>

I have no theories about why these good and bad feelings happened. I have become a pragmatist about such things, and no longer try to understand them. Life is so full of mystery, answers are so few, I do not trust them. I prefer unanswered questions. At the end of the answers there is usually a person who enjoys the power of appearing to know. I have come to like things that are left unfinished. It's the question that shines the light, that seeks. The answer's often a dim reflection of the vastness of the question. There are no answers that satisfy. 

This is a small life, and everything comes to nothing in the end. I like that. I like the idea of smallness, and the wonder of basic human things.

<br>

---

<br>

There is much birdsong in the trees and hedgerows; I can hear it this afternoon. A *peep peep peep peep* and a *chitterwijee, chitterwijee* and many others. I can hear the calls of four, maybe five different birds looking for mates or defending their territories. I don't know their names or which song belongs to which bird, apart from those brave enough to sit and sing by me, the robin and the blackbirds, and of course I know the crows, magpies, gulls and pigeons. But the smaller flocking birds are just flitting, singing clouds. Once I was a curiosity or threat to them. Now I am so familiar I have become invisible. I am nobody, and so I have reached the pinnacle of my existence.

<br>

---

<br>

There is always sadness. I once heard a friend, depressed, under the influence, with a broken relationship, say, "The glass is broken, it can't be repaired." But she was wrong. Things cannot be made as they were, but they can become something else. They can be re-made. All things are impermanent, and everything wears down to dust. Everything has its end and each thing carries the beginning of the next thing. Healing is not about re-making things as they once were, healing is about acceptance and forgiveness and love and growth and beginning again. Scar tissue is an inevitable part of life.

The closer things are to becoming nothing, the more tender they become, and the more tender are the feelings they bring out: a newborn child, a hatchling, a dying old man. A dried seed head surrounded by others; a skeletal leaf floating on a pond; a piece of broken pottery in a pile of soil; half an eggshell lying on the grass; a small bone from a rabbit's leg lying in the sand dunes. Small things that are near their end.

A whole story explodes out of these things. The preciousness in the drying seed head is tied up in the sadness of its turning to dust, and there's joy in the seeds of its offspring showing up in the spring. Beauty is a balance between sadness and joy, and is created in the moment, in the relationship between the viewer and the thing viewed. My life is full of this. Such feelings are never in the past or the future, they are only ever here in the interaction between you and this moment.

<br>

---
