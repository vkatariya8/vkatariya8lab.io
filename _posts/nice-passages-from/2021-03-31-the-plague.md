---
layout: post
title: "The Plague, by Albert Camus"
description: "Excerpts from The Plague, by Albert Camus"
date: "2021-03-31"
tags: [nice-passages-from, books]
categories: [nice-passages-from]
comments: false
share: true
---

<br>

In this respect our townsfolk were like everybody else, wrapped up in themselves; in other words they were humanists; they disbelieved in pestilences. A pestilence isn't a thing made to man's measure; therefore we tell ourselves that pestilence is a mere bogy of the mind, a bad dream that will pass away. But it doesn't always pass away and, from one bad dream to another, it is men who pass away, and the humanists first of all, because they haven't taken their precautions. Our townsfolk were not more to blame than others, they forgot to be modest that was all and thought that everything still was possible for them; which presupposed that pestilences were impossible. They went on doing business, arranged for journeys, and formed views. How should they have given a thought to anything like plague, which rules out any future, cancels journeys, silences the exchange of views. They fancied themselves free, and no one will ever be free so long as there are pestilences.

<br>

---

<br>

Thus the first thing that plague brought to our town was exile. And the narrator is convinced that he can set down here, as holding good for all, the feeling he personally had and to which many of his friends confessed. It was undoubtedly the feeling of exile that sensation of a void within which never left us, that irrational longing to hark back to the past or else to speed up the march of time, and those keen shafts of memory that stung like fire. Sometimes we toyed with our imagination, composing ourselves to wait for a ring at the bell announcing somebody's return, or for the sound of a familiar footstep on the stairs; but, though we might deliberately stay at home at the hour when a traveller coming by the evening train would normally have arrived, and though we might contrive to forget for the moment that no trains were running, that game of make-believe, for obvious reasons, could not last. Always a moment came when we had to face the fact that no trains were coming in. And then we realized that the separation was destined to continue, we had no choice but to come to terms with the days ahead. In short, we returned to our prisonhouse, we had nothing left us but the past, and even if some were tempted to live in the future, they had speedily to abandon the idea anyhow, as soon as could be once they felt the wounds that the imagination inflicts on those who yield themselves to it.

<br>

---

<br>

At such moments the collapse of their courage, will-power and endurance was so abrupt that they felt they could never drag themselves out of the pit of despond into which they had fallen. Therefore they forced themselves never to think about the problematic day of escape, to cease looking to the future, and always to keep, so to speak, their eyes fixed on the ground at their feet. But, naturally enough, this prudence, this habit of feinting with their predicament and refusing to put up a fight were ill rewarded. For, while averting that revulsion which they found sO unbearable, they also deprived themselves of those redeeming moments, frequent enough when all is told, when by conjuring up pictures of a reunion to be, they could forget about the plague. Thus, in a middle course between these heights and depths, they drifted through life rather than lived, the prey of aimless days and sterile memories, like wandering shadows that could have acquired substance only by consenting to root themselves in the solid earth of their distress.

<br>

---

<br>

Those who had jobs went about them at the exact tempo of the plague, with dreary perseverance. Everyone was modest. For the first time exiles from those they loved had no reluctance to talking freely about them, using the same words as everybody else, and regarding their deprivation from the same angle as that from which they viewed the latest statistics of the epidemic. This change was striking since, until now, they had jealously withheld their personal grief from the common stock of suffering; now they accepted its inclusion. Without memories, without hope, they lived for the moment only. Indeed the Here and Now had come to mean everything to them. For there is no denying that the plague had gradually killed off in all of us the faculty not of love only but even of friendship. Naturally enough, since love asks something of the future, and nothing was left us but a series of present moments.

<br>

---
