---
layout: post
title: "The Overstory, by Richard Powers"
description: ""
date: "2018-08-08"
tags: [nice-passages-from, book]
categories: [nice-passages-from]
comments: false
share: true
---

I read about _The Overstory_ a while ago, and had to wait for ages to actually check it out from my local library. But it was worth the wait! Boy, it was a crazy story. It starts off with a few short stories of people whose lives are somehow decided by/connected to trees. That's the part of the book that's called "Roots". The major part of the story happens in the "Trunk" part, where these people's lives become intertwined. The book is about trees, and how we're cutting down forests at a rapid pace and not replacing them with close to enough diversity. The last part of the book is a little melodramatic, but in some sense fitting. There's a lot happening. The book won the USA National Book Award. 

Here's an excerpt from it [from a science magazine, _Nautilus_](http://nautil.us/issue/59/connections/the-woman-redeemed-by-trees).

And a few excerpts I chose. Not too many, but the prose in the book is still very good. Short and simple sentences, vivid descriptions nonetheless, and people whose characters are deeply fleshed out. The cover is beautiful too.

<br>

---

<br>

> Her trees are far more social than ever Patricia suspected. There are no individuals. There aren’t even separate species. Everything in the forest is the forest. Competition is not separable from endless flavors of cooperation. Trees fight no more than do the leaves on a single tree. It seems most of nature isn’t red in tooth and claw, after all. For one, those species at the base of the living pyramid have neither teeth nor talons. But if trees share their storehouses, then every drop of red must float on a sea of green.

<br>

---

<br>

> Something marvelous is happening underground, something we’re just learning how to see. Mats of mycorrhizal cabling link trees into gigantic, smart communities spread across hundreds of acres. Together, they form vast trading networks of goods, services and information…
>
> There are no individuals in a forest, no separable events. The bird and the branch it sits on are a joint thing. A third of more of the food a big tree makes may go to feed other organisms. Even different kinds of trees form partnerships. Cut down a birch, and a nearly Douglas-fir may suffer…
>
> In the great forests of the East, oaks and hickories synchronize their nut production to baffle the animals that feed on them. Word goes out, and the trees of a given species — whether they stand in sun or shade, wet or dry — bear heavily or not at all, together, as a community…
>
> Forests mend and shape themselves through subterranean synapses. And in shaping themselves, they shape, too, the tens of thousands of other, linked creatures that form it from within. Maybe it’s useful to think of forests as enormous spreading, branching, underground super-trees.

<br>

---

<br>

> The single best things you can do for the world. It occurs to her: The problem begins with that word world. It means two such opposite things. The real one we cannot see. The invented one we can’t escape. She lifts the gland and hears her father read out loud: Let me sing to you now, about how people turn into other things.

<br>
---
