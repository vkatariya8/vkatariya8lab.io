---
layout: post
title: "The Best We Could Do, Thi Bui"
description: "Tragic but Beautiful Graphic Novel Memoir"
date: 2018-05-02
tags: [books, nice-passages-from]
categories: nice-passages-from
comments: false
share: true

---

Just sharing some shots from the book. It's about escaping from Vietnam and being an immigrant and being a child and being a parent and more. It somehow talks about being a mother from the perspective of a mother and from the perspective of a daughter who's newly a mother. I recommend it highly. The blurb on the cover says "A book to break your heart and heal it." and that is correct.

![]({{"/assets/the-best-we-could-do/1.png" | absolute_url}}){:class="img-responsive" .center-image}
* * *

* * *

![]({{"/assets/the-best-we-could-do/2.png" | absolute_url}}){:class="img-responsive" .center-image}

![]({{"/assets/the-best-we-could-do/2-2.png" | absolute_url}}){:class="img-responsive" .center-image}

* * *

* * *

![]({{"/assets/the-best-we-could-do/3.png" | absolute_url}}){:class="img-responsive" .center-image}

![]({{"/assets/the-best-we-could-do/3-2.png" | absolute_url}}){:class="img-responsive" .center-image}

* * *

* * *

![]({{"/assets/the-best-we-could-do/4.png" | absolute_url}}){:class="img-responsive" .center-image}

![]({{"/assets/the-best-we-could-do/4-2.png" | absolute_url}}){:class="img-responsive" .center-image}

* * *

* * *

![]({{"/assets/the-best-we-could-do/5.png" | absolute_url}}){:class="img-responsive" .center-image}

* * *

* * *

![]({{"/assets/the-best-we-could-do/6.png" | absolute_url}}){:class="img-responsive" .center-image}

* * *

* * *

![]({{"/assets/the-best-we-could-do/7.png" | absolute_url}}){:class="img-responsive" .center-image}

![]({{"/assets/the-best-we-could-do/7-2.png" | absolute_url}}){:class="img-responsive" .center-image}


* * *

* * *

![]({{"/assets/the-best-we-could-do/8.png" | absolute_url}}){:class="img-responsive" .center-image}

* * *

* * *

![]({{"/assets/the-best-we-could-do/10.png" | absolute_url}}){:class="img-responsive" .center-image}

* * *

* * *

![]({{"/assets/the-best-we-could-do/11.png" | absolute_url}}){:class="img-responsive" .center-image}

* * *
