---
layout: post
title: "Passing for Human, by Liana Finck"
description: "Surreal, Dark and Poignant Graphic Novel Memoir"
date: 2018-11-11
tags: [books, nice-passages-from]
categories: [nice-passages-from]
comments: false
share: true

---

Liana Finck is an artist I admire a lot, primarily for her Instagram. She wrote a memoir and it was everything and more of what I expected. Enjoy.

* * * 

![]({{"/assets/passing-for-human/80.png" | absolute_url}}){:class="img-responsive" .center-image}

* * *

![]({{"/assets/passing-for-human/92.png" | absolute_url}}){:class="img-responsive" .center-image}

![]({{"/assets/passing-for-human/93.png" | absolute_url}}){:class="img-responsive" .center-image}

* * *

![]({{"/assets/passing-for-human/136.png" | absolute_url}}){:class="img-responsive" .center-image}

* * *

![]({{"/assets/passing-for-human/163.png" | absolute_url}}){:class="img-responsive" .center-image}

![]({{"/assets/passing-for-human/164.png" | absolute_url}}){:class="img-responsive" .center-image}

* * *
