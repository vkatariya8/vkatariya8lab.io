---
layout: post
title: "The Shadow of the Wind, The Circuit, Wonder Boys, and The Lathe of Heaven"
description: "Excerpts from a few books."
date: "2019-01-28"
tags: [books, nice-passages-from]
categories: [nice-passages-from]
comments: false
share: true
---

<center><h3><b><i>The Shadow of the Wind</i>, by Carlos Ruiz Zafón</b></h3></center>

<br>

> “This is a place of mystery, Daniel, a sanctuary. Every book, every volume you see here, has a soul. The soul of the person who wrote it and of those who read it and lived and dreamed with it. Every time a book changes hands, every time someone runs his eyes down its pages, its spirit grows and strengthens.

<br>

---

<br>

> this place, books no longer remembered by anyone, books that are lost in time, live forever, waiting for the day when they will reach a new reader’s hands. In the shop we buy and sell them, but in truth books have no owner. Every book you see here has been somebody’s best friend. Now they have only us, Daniel. Do you think you’ll be able to keep such a secret?”

<br>

---

<br>

> Tell me, what sort of women do you like, Daniel?” “I don’t know much about them, honestly.” “Nobody knows much about women, not even Freud, not even women themselves. But it’s like electricity: you don’t have to know how it works to get a shock on the fingers.

<br>

---

<br>

> She laughed nervously. “I don’t know what came over me. Don’t be offended, but sometimes one feels freer speaking to a stranger than to people one knows. Why is that?” I shrugged. “Probably because a stranger sees us the way we are, not as he wishes to think we are.”

<br>

---

<center><h3><b><i>The Circuit</i>, by Rowan Ricardo Phillips</b></h3></center>

<br>

The BEST tennis book I have read. All my favorite excerpts [here](/nice-passages-from/the-circuit). Here are two of my faves.

<br>

> You and Nick. He’s bored, you’re pissed; he’s pissed, you’re bored. The rather important difference is that Nick is a kid invested in the vicissitudes of his life, the existential plight of being Nick Kyrgios. But he’s a kid in his early twenties, meaning he thinks he’s an adult. He thinks he’s lived, so he knows things; but also that he needs to live so that he knows things. You are an actual adult, meaning you’re old in some manner so that when you say you’re old it’s met with silence. And you want your relationship with Nick to be like it is with most of the players on the circuit–it’s a marriage: you love it, or you fake it, or you get the f\*ck out.  
> Nick neither loves it nor fakes it nor gets the f\*ck out.  
> He shows up again and again with the same bullshit, which is meaningful to him because it’s the part of his life that he can’t get back, that feels like it will exist apart from all of the other parts, which is something you sense when you’re young but because you’re young you tend to act on that sense stupidly. This is the beauty and utter horror of being young.

<br>

---

<br>

> Despite his tall frame, to which over the years he’s added much muscle, Murray isn’t a particularly powerful player, nor is he one imbued with much in the way of grace. […] Indeed, there’s little quiet in the way he moves. When he stretches to chase down a difficult shot sent his way from the other side of the net, an under-duress yell often precedes his legs firing into action like the loud engine of a muscle car sputtering before it revs up, rears back, and jumps into its speed. He’s lightning quick. Not just of body but also of mind. England has embraced him as their noble (and now knighted) champion, but his game reflects the streetwise Scot in him. He lives off of wry chicanery hidden in his consistency; his shot pattern screws with his opponents’ rhythm, often lulling them into a false sense of expectation. Imagine being given a Russian nesting doll and opening it, working your way through one carbon copy of the same doll after another, until you come across one with an egg yolk stowed inside it that spills onto your lap. This is what it’s like to play him.

<br>

---

<br>

<center><h3><b><i>Wonder Boys</i>, by Michael Chabon</b></h3></center>

<br>

> This is in my opinion why writers—like insomniacs—are so accident-prone, so obsessed with the calculus of bad luck and missed opportunities, so liable to rumination and a concomitant inability to let go of a subject, even when urged repeatedly to do so.

<br>

---

<br>

A sober man at a party is lonely as a journalist, implacable as a coroner, bitter as an angel looking down from heaven. There’s something purely foolish about attending any large gathering of men and women without benefit of some kind of philter or magic dust to blind you and weaken your critical faculties. I don’t mean to make a big deal out of sobriety, by the way. Of all the modes of human consciousness available to the modern consumer I consider it to be the most overrated.

<br>

---

<br>

<center><h3><b><i>The Lathe of Heaven</i>, by Ursula K. Le Guin</b></h3></center>

<br>

To cross a river, ford it, wade it, swim it, use boat, ferry, bridge, airplane, to go upriver, to go downriver in the ceaseless renewal and beginning of current: all that makes sense. But in going under a river, something is involved which is, in the central meaning of the word, perverse. There are roads in the mind and outside it the mere elaborateness of which shows plainly that, to have got into this, a wrong turning must have been taken way back.

<br>

---

<br>

"I can answer your questions, and I do. . . . But anyway: look. You can't go on changing things, trying to run things." "You speak as if that were some kind of general moral imperative." He looked at Orr with his genial, reflective smile, stroking his beard. "But in fact, isn't that man's very purpose on earth—to do things, change things, run things, make a better world?" "No!" "What is his purpose, then?" "I don't know. Things don't have purposes, as if the universe were a machine, where every part has a useful function. What's the function of a galaxy? I don't know if our life has a purpose and I don't see that it matters. What does matter is that we're a part. Like a thread in a cloth or a grass-blade in a field. It is and we are. What we do is like wind blowing on the grass."

<br>

---

<br>

He nodded. "Would you like some coffee?" he asked. It was more than dignity. Integrity? Wholeness? Like a block of wood not carved. The infinite possibility, the unlimited and unqualified wholeness of being of the uncommitted, the nonacting, the uncarved: the being who, being nothing but himself, is everything. Briefly she saw him thus, and what struck her most, of that insight, was his strength. He was the strongest person she had ever known, because he could not be moved away from the center. And that was why she liked him. She was drawn to strength, came to it as a moth to light. She had had a good deal of love as a kid but no strength around her, nobody to lean on ever: people had leaned on her. Thirty years she had longed to meet somebody who didn't lean on her, who wouldn't ever, who couldn't....

<br>

---
