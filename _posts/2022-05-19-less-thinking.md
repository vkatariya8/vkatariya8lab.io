---
layout: post
title: "Less is more when it comes to thinking"
description: "Right now, I'm thinking less, and life seems easier"
date: "2022-06-17"
tags: [blog]
categories: [blog]
comments: false
share: true
---

I'm currently in a pretty good and stable mental place. Something that comes with that is a very mellow internal dialogue with myself. It's very low-stakes, and I'm not analyzing things much, or even making an effort at "processing" things that happen. The processing seems to be happening automagically. I like this level of energy in my head--not too little to keep me depressed, but also not so much that I spend too much energy in circular or unproductive thought patterns.

What's the most interesting thing for me here is that this mental state seems to be characterized by an apparent lack of effort. I will say upfront here that I know there is a correlation/causation ambiguity going on here. I mean this in the sense of am I feeling good because of lack of effort, or is it the feeling good that makes me put less effort in? Either way, it gives me a sense of what my ideal way of being is like.

An example of healthy or "easy processing" in the recent past is that I had a severely upsetting and triggering episode last week. It lasted for about half an hour, then I visited a friend, told them, got a hug, and it was gone. This is quite amazing for me, as usually I have considered that I'm slow at processing things emotionally and thus have resigned myself to a protracted and difficult path out of something difficult. Also, this particular episode didn't seem like a shortcut, it still feels like I did justice to the thing I felt. 

I've also not been journaling for a few weeks. This is also going to fall under the correlation/causation thing. I know that generally when things are going well, I don't feel the need to journal. Right now, though, it feels like it's different from just that. I feel like I'm more willing to bounce off things quickly. I think there is a self-indulgence associated with journaling about things bothering me, and I just don't have that self-indulgence anymore. There can be multiple reasons for this. One is that I've lost patience. The other is that my long break from employment have slowly let me become "me". And so on.

I think this is a big step in my large, multi-year project (which I began in 2020) of wanting to feel through things rather than think through things. Historically, I have processed things by thinking through them, and I was always rewarded for thinking in general. It seemed "right", "deserved", and "effortful". I think that last adjective is the most relevant here. I've always thought of moving through life as a labour, and when I shifted course to put my feelings and instincts first, I pleasantly realized that not all inner mind things are strenuous acts. I think that in slowly becoming truer to myself and my feelings, things come easier to me, and it is straightforward to act in ways that align with who I am. So of course it's easy to process things. 