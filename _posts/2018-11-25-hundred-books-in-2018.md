---
layout: post
title: "A hundred books in 2018"
description: "A long-standing goal, conquered"
date: "2018-11-25"
tags: [books,reading,blog,reflection]
categories: [books, blog]
comments: false
share: true
---

For the past three years, I've made it a goal of sorts to read a hundred books in a calendar year. Through my undergraduate studies, I didn't find the time to do so, and I was also reading fantasy series' that consisted of a number of large, long books. In 2017, I moved across countries and also got "stuck" for the third major time in my life when it came to reading. This year has been more kind: I discovered the wonders of the public library, was far more stable in my domestic living arrangements, and had ample reason to seek escape in the world of books.

I've crossed a hundred books and it hasn't even been December yet. Given the rate at which I'm currently reading, I'm most likely going to end up at 110-115 books this year. But it doesn't feel like I've _accomplished_ something. Yes, I am proud of the fact that I read a hundred. But it has not been a struggle. I have thoroughly enjoyed the process; if I didn't like something, I put it away and read something else. I made the process smooth. And that's what it's supposed to be. When I read for personal pleasure in my leisure time, I'm looking for exactly that: pleasure and an escape.

However, the fact that I had this goal was something concrete to aim towards; when I felt aimless and did not want to engage in a particular task, I could at least read a book and move towards the hundred-book goal. I'm the kind of person who needs some sort of statistic or documentation to prove that I actually did something at all. I refrained from posting updates to the social network for books, Goodreads, and didn't parade my weekly reading progress to a lot of people. I shared excerpts and recommendations when I thought they were in order. The reason I'm saying this is because when I think about it in retrospect, this was both a challenging and pleasant goal to assign myself. It was always something that I could reach given some diligence from myself, and it was also something that I would enjoy _doing_. I'm firmly of the mind that the best things you do are the ones that you enjoy _doing_, rather than the ones that you enjoy _done_. 

I think it will be personally rewarding and worthwhile to write another post soon about the books that have been most influential, useful, interesting and beautiful this year, so I'll do that before the year is done. 

This brings me to my last thought: what's my reading-related goal for the next year? I think my focus will be more on deep reading and understanding, rather than exposing myself to different styles and genres as I did this year. I want to read books that I _know_ I will re-read, or re-read the books that I've set aside for re-reading. And I also want to read the classics that I've always shied away from for years now. It's exciting. It's a different goal, and challenging in a different way. Of course, it's not set in stone, and if I don't see myself liking something for an extended period of time, I'll change things around. 
