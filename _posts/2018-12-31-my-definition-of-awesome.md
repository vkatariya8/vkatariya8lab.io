---
layout: post
title: "My Definition of Awesome"
description: "Lord Brocktree the badger lord, an unlikely anchoring for an adjective"
date: "2018-12-31"
tags: [blog, personal]
categories: blog
comments: false
share: true
---

I've written earlier about the value of choosing the correct adjectives ([link](../lazy-adjectives/)) in one's writing or talking. Apart from that, I also think that some adjectives are tied to some things. This is true for me personally. For me, the word laconic ties in deeply with Sherlock Holmes. When I think of the word and whether I want to use it or not, I think of the similarity of whatever I'm thinking about with Holmes. Similarly, I want to share my anchor for the word "awesome". For many years, I'd been using the word awesome rather indiscriminately. Something went well? "Awesome." Someone calls and says they'll be arriving in ten minutes? "Awesome." But are those things awesome at all? Now, I've retired the use of it for situations that aren't in spirit to this passage in _Lord Brocktree_, one of my favorite books of all time:

> They thundered along the tunnel and hit the vermin like a tidal wave. The awesome Brocktree went straight through the Hordebeasts, his sword sycthing a harvest of death.

Brocktree is a Badger lord, the mightiest and strongest creature alive in this book. The sword he wields can be used only by him, and with two hands. When I imagine him running in war through a tunnel with that massive sword hefted aloft, I think _awesome_. And so when I want to use this adjective, I want it to capture the spirit of Lord Brocktree. After all, the dictionary definition of awesome is this:

> awesome: extremely impressive or daunting; inspiring great admiration, apprehension, or fear

A picture of Lord Brocktree for reference:

![picture of Lord Brocktree](https://www.websterbookstore.com/assets/images/product/2203338.jpg)
