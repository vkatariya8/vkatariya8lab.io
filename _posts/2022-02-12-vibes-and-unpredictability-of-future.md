---
layout: post
title: "I can't predict who I'm going to be"
description: ""
date: "2022-02-12"
tags: [blog]
categories: [blog]
comments: false
share: true
---

Hmm. Something that keeps coming up in conversations with other people is how surprised I am at who I am or who I've become. To be more specific, in 2017 when I graduated from undergrad, I was not super interested in food or gardening or fermentation. Now I am, and I've shed a _lot_ of aspects of my erstwhile personality while picking up things I couldn't predict at the time. Of course, that was a more formative time than my upcoming five years, but it's still nice to know that there are aspects of me that I'm going to discover in time, and which I can't predict now.

The thing I'm thinking about all the time these days is what my future is going to look like, and what _I'm_ going to become in a few years. Given that five years ago, I couldn't have predicted who I am today, it's safe to say that I don't have much of a clue. It's scary, of course, but it's also quite exciting. Who knows where life is gonna go!?

There always seems to be a vague set of vibes that I want more of. That changes much slower than any specific desires of mine. Right now, I think the vibe I want is kindness, doing work that is somewhat aligned with me and what I want to see in the world, and deeper and better relationships. I think this is quite vague because there's no specific path that these target vibes describe or restrict me to.

I think this is a nice way of looking at things? It's good, sometimes, to have specific goals and notions. However, I feel like they provide something _too_ concrete to compare with. On the other hand, with something vague, there's always an additional opportunity to be kind to myself when judging where I've gotten or how far away I've drifted from what I wanted. 

The last thing I want to say is that this kinda connects to the relative lack of ambition I have these days. When in school and undergrad, I felt like I was on a certain well-described and well-trodden path, and so it made sense to work hard and strive to be at the top of my class, or to be successful, etc. Right now, I don't really have a sense of what or where I'm going to be, and that segues nicely with how I don't feel much ambition too.

