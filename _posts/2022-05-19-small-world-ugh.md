---
layout: post
title: "[incomplete] Small world (ugh)"
description: "Why a small world is not a good thing"
date: "2022-05-19"
tags: [blog]
categories: [blog]
comments: false
share: true
---

Very much a series of rough thoughts, very opinionated.

- When we find surprising connections in new things we do, or in new people we meet, we often say that it's a small world.
- It is indeed a small world.
- But I don't think that's a good thing. I think that my reaction to each such realization is the exact opposite of delight.
- I've come to realize that each time we realize it's a small world, it is a reminder of the way our spaces are shaped. Many fields and projects and avenues are gatekept to the extent that only a certain pattern of individuals can break in. And this is a sort of nepotism (not the best word).
- The other reason, relatedly, that this is upsetting is because these "small world" connections go back to remembrances of childhood. While it's cool in a way.. it also means that you're well set on your path even then sometimes.
- So on a personal level, each "small world" realization makes the world seem narrower and more stifling.
- However, I think it more serious to think of it at a larger scale. I think many, if not all endeavours, will be made better with a plurality of people taking part in them. Thus, I think it's good for us to _actively_ reject this small world phenomenon. And perhaps try to build systems where they do not occur.