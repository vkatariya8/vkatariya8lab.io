---
layout: post
title: "Thoughts on reading a recent Federer biography"
description: "Learning about relaxing from Federer's approach to life and tennis"
date: "2021-12-10"
tags: [blog]
categories: [blog]
comments: false
share: true
---

I recently read a [biography of Roger Federer](https://www.goodreads.com/book/show/56222198-the-master) by Christopher Clarey. I've followed Federer's career for a while now, and so I wasn't sure what I was going to get out of the biography.

The most interesting thing, for me, was something that Federer says to Clarey at one point about relaxing, and longevity. Federer has played on the professional tennis circuit for decades, which is rare in tennis circles. I think everyone likes to ask him what his "secret" is. Of course, the secret is a combination of multiple things--good training, good diet, being in touch with his body, being preternaturally talented, having the perfect physique for the sport, etc.

However, the one thing that I didn't know before reading this book is this. Clarey is meeting Federer somewhere in Switzerland for lunch after Federer has had a tennis practice session. Federer says how he his superpower is that he can completely relax during that lunch and even treat it as a time to recover and rejuvenate. This is despite a grueling and intense physical practice session that just preceded it.

I think this is something I would like to carry forward in my life. I find it difficult to switch in and out of being engaged fully. I don't really have control over it, which I would like to improve. For example, I had my PhD defense ten days ago. I _knew_ that I was fully prepared for it. I had my talk ready, I knew what I was going to talk about, etc. But I was still very nervous about it all. I could alleviate that anxiety temporarily by actually working more on my slides. However, I couldn't let the nervousness slide when I needed it to. It would have been very useful and more efficient for me to engage the high-octane part of my brain when needed, and then let it rest.

Instead, I seem to operate at an almost-constant tempo in life, with regards to my brain that is. I can bring the same mental energy when talking to a friend or watching a YouTube video as I bring to Serious Research Work. This is not ideal, plus a lot of this brain energy is inefficient (irrational worrying, circular thought processes going nowhere, etc.). I think what Federer said can help me with it.

It kinda sounds counterintuitive, but I feel like to begin a habit of it, I need to make a conscious decision to relax. I think some level of self-talk might be useful until I'm able to easily flit between engaged and relaxed. 

Overall, I'm pretty pleased that I got this much out of a biography about Federer. I was honestly just hoping for some pleasant memories and reminiscences. But this particular insight seems really valuable to me in life, and especially now as I feel very high-strung and on edge. 
