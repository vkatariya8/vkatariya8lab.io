---
layout: post
title: "Saying goodbye to a tea"
description: "How finishing a tea is like losing a friend"
date: "2019-01-08"
tags: [blog, tea, whimsical]
categories: blog
comments: false
share: true
---

This is a tiny and almost rambling post--I'm attempting to be more regular with my writing here, thank you for bearing with me.

TWo days ago I brewed my last leaves of a tea I got from a trip a few months ago. As I used up the last of the leaves and placed them in my French Press, I felt a twinge of guilt and sadness--when will I drink this tea again? Will I at all? I drink tea almost all the time, and it provides a background and context to my reading, or working. For different times and different moods, I call upon different teas. For when I'm angsty and fidgety, I add some dried lavender or chamomile to my green tea, or drink one of my luxuriant tea-bag tisanes that contain no caffeine. For when I need to perk up and possibly wake up, I drink black tea, and I drink it rather black. My daily companion is a good, solid green tea. As these thoughts ran through my head, I was reminded of something I thought a few years ago--isn't each of these teas like a friend? Don't I call upon different people when I want to talk about different things? Does that mean... that each time I finish a tea, it's like saying goodbye to a friend? If I finish a tea that I know I'll order again soon, it's like saying goodbye to a friend temporarily. The parallels can be drawn further, but I don't see the point to it.

What is nice, though, is the fact that I seem to _bond_ with each of the teas in my time with them. I learn their uses, and know when to "use" (drink) them. Of course, that's not how I think about friendships and relationships, but from a selfish advice-gathering perspective, I suppose everyone does think about the value each friend brings to them. For the record, I don't think that's a bad thing. Anyway, I am sleepy now, and don't want any more jolts, so it's time for being alone, and without tea. Goodnight. 

All this philosophical rambling ends with something a friend said to me regarding this matter...

> begs the question, can you buy more friends?


