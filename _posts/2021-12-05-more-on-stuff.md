---
layout: post
title: "More on stuff"
description: "More thoughts on all the stuff I have and some morbid-ish reflections"
date: "2021-12-05"
tags: [blog]
categories: [blog]
comments: false
share: true
---

A few months ago (in the last blog post in fact), I wrote about some thoughts regarding... stuff. I'm much closer to leaving Baton Rouge now, and the prospect of dealing with all my stuff has moved from the vaguely theoretical to the strictly practical. Today I gave some books to a friend, asked another friend if they wanted my fermentation-grade bottles, and arranged to sell my grain mill. I've been doing a version of this every weekend now: sorting through stuff, realizing that there's very little that I can take back home, and trying to give away specific things to people who're going to use them.

I feel like Bilbo in the beginning of _The Lord of the Rings_, except my departure is going to be planned. Speaking of departures.. I'm reminded of why I wanted to write this blog post in the first place. I was talking to a friend (the one I gave some books to) about leaving, mostly because they're off to visit India tomorrow. We were talking about a specific melancholy that both of us feel before embarking on a trip. Then this friend asked me how I felt about leaving Baton Rouge permanently.

This reminded me of a pattern of thought that's recurred often in the past few months for me. I've been thinking about how my leaving Baton Rouge is a lot like leaving life itself, as in, dying. A lot of the things I'm doing now are things that have direct analogues to what a person would do if they were told they only had a few more months, or years, to live. 

For example, my landlady and I often talk about my garden, and about its future state. Knowing that I'm going to be leaving soon, I planted some perennial plants so that they could be there without much supervision. This sounds a lot to me like planting a tree to outlast you, or doing something to (a) minimize the burden on people after you've left, and (b) leaving a sort of legacy.

The morbid analogies and thoughts definitely show up more when I think about my stuff. I'm trying to be mindful and careful about who gets what, and I'm trying to not let anything go to waste if I know someone who'll be able to use it. It sounds quite similar to drafting up a will.

Rather than being alarming, I think this line of thought is quite grounding. I don't live a "low-impact" life by any means, but I would like to. And even if I can't or don't do so, I think, at the very least, that I should do this; i.e., not throw away stuff indiscriminately.

Not exactly sure where this blog post is going, so I think I'll stop. But I wanted to get these words down and confirm to myself that this is fine. Even though there is some sadness in leaving a place where I've set down considerable roots (indirectly true in a literal sense if you think about the plants I've raised), looking at the things I own and use prompts reflection. 