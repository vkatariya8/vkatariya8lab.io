---
layout: post
title: "Mumbai observations"
description: "running compendium of observations about living in Mumbai"
date: "2023-01-25"
tags: [blog]
categories: [blog]
comments: false
share: true
---

I moved to Mumbai five months ago now. It's been a pretty interesting and mostly enjoyable experience. I wanna keep a running list of oddities and quirks I've experienced in this city, which by far has amused and entertained me more than any other place I've lived ini.

1. **Walking etiquette**. One of the basic things I knew about Mumbai was that everyone is on the move, hustling to make ends meet, doing gigs upon gigs upon side gigs. However, I was not prepared for how _badly_ everyone walks on the sidewalks. Of course, we don't have enough sidewalk for the number of people, but at the same time, people take up _so much space_. They also stop suddenly, don't obey "lane" rules, and so on. After observing this for a few months, I have a few theories. The simplest, people are just bad walkers. Another theory is that personal space is at a premium, so people want to take up as much space as they can in public. Or.. people are so stressed with all their hustling that they don't have the headspace for the spatial awareness and peripheral vision good sidewalking requires. I'm inclined to go with the first theory though.
2. **Elbows**. A continuation of the first, to some extent. The second-most dangerous object when walking on the streets is people's elbows! People love to shove them out, or just put them in places where they're going to jostle others. Obviously, this is mostly a guy thing. I find it really annoying. Whenever I feel like it, I go butt into people's elbows hoping that they'll get the message to stop elbow-spreading.
3. **Conversations**. I've had random, enlightening and fun conversations with cab drivers really often. More than other cities I've been in, I think the cab drivers here enjoy the transience of our short relationship and are happy to have a conversation with lots of style and not too much substance (kinda). My taxi to work only takes five minutes, but I've played agony aunt, gotten life advice ("just keep your heart open"), and been told how the city has changed in the last twenty years. The highlight, though, was a cab ride from the airport when the driver recited his favourite Kabir ke dohe to me.
4. **Pickles**. People trust each other here, to a larger extent than elsewhere. But every time I walk home from work, I see two pickle shops which have chained the larger pickle jars to each other, presumably to prevent people from stealing them! It's quite funny honestly.
5. 
