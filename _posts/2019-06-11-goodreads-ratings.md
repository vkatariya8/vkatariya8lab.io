---
layout: post
title: "5-star ratings"
description: "A rant against Goodreads' rating system"
date: "2019-06-11"
tags: [blog]
categories: [blog]
comments: false
share: true
---

This year, I've been updating my reading progress meticulously on Goodreads. It's nice to do so, and every time I finish a book, I get a slight tinge of satisfaction from going online and telling everyone I finished it. It also lets me nicely categorize books according to shelf (which I could easily also do myself).

However, there's something about Goodreads that I don't like, and that's the fact that every book has to be given a 5-star rating. I'm not even railing against the fact that the book has to be distilled to a number; that's fine. However, giving me only five options is too restrictive. What does 5 stars mean? What is the difference between 3 and 4 stars? I think it depends on how you go about the task of rating.

I have some keystone books that, in my head, define my benchmarks for "great", "good", "average", "mediocre" and so on. The benchmarks don't end there, though. For example, there's a keystone book for "funny but also excessively crude, read parts of it when wanting to feel like being slapped in the face" (_A Confederacy of Dunces_). So when Goodreads tells me to assign some rating to a book, I immediately compare it to my "standard" 3-, 4- and 5-star books, pick which it's closest to, and boom.

This is overtly simplistic. Where is the 3.5 stars? The 3.75? They need to exist! {%marginnote 'insti-story' 'Side anecdote: when in my undergrad, as we were finishing our time there, I would go up to people I knew and ask them to rate their college life on a scale of 3.7, or 4.9, or some other random number. The idea was to dislodge the default 3/5, 4/5 or so rating that they might have thought of already, and to make them think about a more precise answer.' %} Sometimes I add books to a shelf that's for books I _need_ to read again -- surely those books deserve 5 stars according to Goodreads? No, not really.

This brings me to something else that I think about a lot: should we really measure these things? Of course, many of the things that we experience in our lives are totally intangible, and can't be reduced to mere numbers. When I track my life excessively through spreadsheets, sometimes I feel like I'm living _for_ the numbers, rather than let the numbers be a byproduct of my actions. Would life be easier (marginally) if I didn't have to think about what rating to provide to each book? I think the answer to this is no. {%marginnote 'answer' 'When you consume a lot of anything -- food, books, music, etc., you are bound to start a sort of dictionary and encyclopedia for them in your head. Whether or not you assign them numbers, ratings and rankings, you still try to think of them as a composite whole and holistically combine them into how they have shaped you. This may result in categorizing, identifying masterpieces, and so on.' %} I _want_ to know how these have affected me. I want to keep a list of books that can be my friends, which I can call upon when I need to. I also want teachers, jokers, stern mentors, and so on. Unless I start thinking of books relative to all the other books I've read, I can't create these lists in my head. 

I want to write some posts in the future mulling about my spreadsheet habit. Keeping track of everything is rewarding, although it sometimes becomes dreary and a liability. 
