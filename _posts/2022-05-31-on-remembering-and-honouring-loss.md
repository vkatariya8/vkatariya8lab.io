---
layout: post
title: "On remembering and honouring memory of loss"
description: "I talk about how I remember people I've lost and how it's changed over the years"
date: "2022-05-31"
tags: [blog,personal,reflection]
categories: [blog]
comments: false
share: true
---

_I exclusively talk about death and loss in this post. You may want to skip it if you don't have the emotional bandwidth now._  

&nbsp;
<br/>

---

&nbsp;
&nbsp;
&nbsp;

> "Courage, Merry, courage for our friends." -- Eowyn

Over the past few years, I've lost some friends and loved ones. It's been hard, obviously, to deal with and move on from the voids that those losses have left in my life. In this essay, I'm going to talk about it at some length. It's the first time I've talked about these things in a public setting, so please be kind when you read.

Lately, I've made a conscious effort to feel more of my feelings. This has been a project that's been going on for a couple of years, and came from a desire to be more human and less depressed. Because of this, these losses have welled up to a significant degree. It appeared as though they were waiting to be acknowledged and processed in a certain way. I'm not sure how exactly to say this, but I think there were two things involved. I wanted to move on, but at the same time I also wanted to stay true to the memory of these people who were important to me. For a long time, this involved a certain degree of guilt and self-flagellation. I thought that the way to remember someone was to keep reminding yourself of the specific things that you missed them for.

I felt like the best way to honour someone's memory was by omission, by forgoing something in my life. Someone I know of and follow on social media lost someone who was close to them few years ago. They made a nice post about their friend, and then declared that as tribute, they would retire a certain adjective they used for their friend from their vocabulary. It was like a sports team retiring the jersey number of one of their most important members. At the time, I thought that this was a really cool thing to do. So I decided to use it too since it was a form of reminding myself of the void that they left behind.

Since then, many things have changed. I have changed. My general outlook towards life has changed (see [Participating in the world](https://vishalkatariya.com/blog/participating-in-the-world/)) from one borne of cynicism to one borne more of.. possibilities and taking up space. I now think that it's important to base my beliefs and thinking in what I want and what I can imagine, rather than what I don't like. Relatedly, I realized is that remembering people by giving up something was a way to center some form of guilt. I don't think guilt is the right word here, but it'll do for now.

For instance, my friend S passed away in 2016 in an accident, and I was bereft. I will eventually write more about what he meant to me later, but one of the ways my grief manifested was in giving up certain bands and artists' whose music S loved and talked to me about. I too, embraced that, thinking that if I gave up these things, I'd never forget S. And yes, I didn't forget S. But it was torture to live life this way. Every time I heard that band, or that one song, I'd be filled with sadness and longing. It took me some time to realize that this was almost a sort of trauma bond that I was constructing with myself, and that I had to stop.

The funny thing is that it took a few more losses for me to realize that it just wasn't possible. I couldn't live every day with a mental account of all the things _not_ to do, especially when a part of me was asking, begging for me to instead embrace those very things. All of us are imprinted upon by the people around us, and it is futile to fight that. So each time I told myself I wouldn't listen to S's favourite band, I became less myself, I think. I don't think the answer is to totally embrace every single influence in my surroundings, but at the least to avoid defining life by what to avoid.

Right now I'm in a healthier place. There are things I remember each of these people for, and I make a conscious effort, almost, to incorporate those things into my life. I don't know how better to say it, but it almost feels like I am secure in my loss. I know what it is that I miss, I don't need external validation to bolster my feelings, and I am sure that I keep these people alive in me in my own way. I am also comfortable keeping these things private. There are certain rituals or activities I do for each of these people, and I don't think I'll ever tell anyone what they are. I just don't need to, and I genuinely enjoy the feeling of having my own personal tribute. The fact that these things aren't known or visible seems to make them more real and valid. This is so freeing, so much better than remembrance by omission. I am happy to be here.

I am happy to be here because I am not trudging through life with an albatross around my neck. I am not afraid of losing my grief, which means I don't hold on to it so tightly. I still honour it, but in a more positive way. My relationship to these people is not defined by them being away, but rather by a feeling of gratitude that they were in my life in the first place. I still feel angry, of course, and wish they were around. But it is not the only thing I feel.

What feels especially pleasing to me is how this whole journey has integrated me as a person. I feel aligned in how I deal with loss and grief, because it goes parallel to the way I wish to participate in the world. I feel empowered having figured this out mostly by myself, and know that I have the emotional wherewithal to deal with any other loss that I'll inevitably have to deal with in years to come. I've come a long way in my general long-term personal project of wanting to feel more things and feel them instinctively. And finally, I'm doing far more justice to people who are no longer with us but who I am still able to hold in high regard without feeling guilt.
