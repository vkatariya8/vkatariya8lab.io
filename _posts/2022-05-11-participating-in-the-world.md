---
layout: post
title: "[incomplete] Participating in the world"
description: "set of notes about wonder, beauty and optimism"
date: "2022-05-11"
tags: [blog]
categories: [blog]
comments: false
share: true
---

I want to make some notes about, broadly speaking, "participating in the world". This is very incomplete, and I would like to both flesh it out and expand on the points below because these things matter a lot to me.

- Reading about indigenous ways of living, in particular _Braiding Sweetgrass_ by Robin Wall Kimmerer, made me realize that it is possible for humans to live in such a way that they can enhance natural ecosystems.
- I think it's important to combat the notion that the only thing humans can do is harm the natural environment, even if that is what we do for the most part.
- Relatedly, this ties into my views on the messaging that humans should minimize their footprint, the amount of waste they generate, and so on. That messaging is fine, but when things are exclusively like that, then we start to live tentatively, and also our existence is defined by things to _reduce_ and things to _not do_ instead of things to _do_.
- Even though humans harm the environment for the most part, it is important to remember, and even center, the fact that that's not the _only_ way we act.
- Further, by centering the things we shouldn't do, or the things we should reduce, makes us think of being apart from the world, instead of as a part of it.
- And species loneliness is scary too.
- To me, a natural way to combat that cynicism is to look at the world with wonder.
- The world is a big, beautiful, sometimes funny, often profound place, and there are all sorts of things to witness, if we look.
- Observing the world closely is also a way of respecting it.
- Also, for me personally, this is part of a concerted effort of dealing with depression over the years. Whenever I have the energy, I will actively resist the anhedonia and cyninism that comes from being depressed.

also [How can wonder transform us?](https://helensreflectionsblog.wordpress.com/2021/12/03/how-can-wonder-transform-us/), an essay about Rachel Carson, Earthsea, and wonder in general with regards to the natural world.

also 

> And of the moth that first captured my heart in a single moment of pure trembling enchantment? How could I _not_ have known such a joyous thing existed, or that it would inspire such a sense of wonder? A sumptuous beauty in clothed in emerald velveteen that came to mind each time I closed my eyes for weeks after my first sighting.

from [The Extravagant Partisanship of the Leek-Green Faction](https://naturallatin.com/2016/05/30/the-extravagant-partisanship-of-the-leek-green-faction/)

> More than any other season, winter requires a kind of metronome that ticks away its darkest beats, giving us a melody to follow into spring. The year will move on no matter what, but by paying attention to it, feeling its beat, and noticing the moments of transition—perhaps even taking time to think about what we want from the next phase in the year—we can get the measure of it.

from _Wintering_, by Katherine May.

also [Noticing small things](https://www.sundrymourning.com/2022/05/31/noticing/)

[Attention and Will](https://rohandrape.net/ut/rttcc-text/Weil1952d.pdf), an essay by Simone Weil

[The Pathologies of the Attention Economy](https://open.substack.com/pub/theconvivialsociety/p/the-pathologies-of-the-attention) and [Your Attention Is Not a Resource](https://theconvivialsociety.substack.com/p/your-attention-is-not-a-resource) and [On Two Ways of Relating to the World](https://theconvivialsociety.substack.com/p/on-two-ways-of-relating-to-the-world) and [The Virtue of Noticing](https://comment.org/the-virtue-of-noticing). All by LM Sacasas.

> When you spend enough time with someone who pays close attention to something (if you were hanging out with me, it would be birds), you inevitably start to pay attention to some of the same things. I’ve also learned that patterns of attention — what we choose to notice and what we do not — are how we render reality for ourselves, and thus have a direct bearing on what we feel is possible at any given time. These aspects, taken together, suggest to me the revolutionary potential of taking back our attention.

from Jenny Odell's book

also see Mary Oliver's [The Summer Day](https://www.loc.gov/programs/poetry-and-literature/poet-laureate/poet-laureate-projects/poetry-180/all-poems/item/poetry-180-133/the-summer-day/):

> I do know how to pay attention, how to fall down
> into the grass, how to kneel down in the grass,
> how to be idle and blessed, how to stroll through the fields,
> which is what I have been doing all day.
> Tell me, what else should I have done?
> Doesn't everything die at last, and too soon?
> Tell me, what is it you plan to do
> with your one wild and precious life?