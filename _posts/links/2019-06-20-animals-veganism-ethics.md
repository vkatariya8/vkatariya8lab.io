---
layout: links-post
title: Animals/Veganism/Ethics
categories: links
share: true
---

## Links - Animals and Ethics

- [Are Animal Experiments Justified?](http://nautil.us/issue/72/quandary/are-animal-experiments-justified) {%sidenote 'source' 'Nautilus - May 2019' %}
- [Listening to ketamine](https://www.knowablemagazine.org/article/mind/2019/listening-ketamine) {%sidenote 'source' 'Knowable Magazine - 2019' %}
- [Do Elephants Have Souls?](https://www.thenewatlantis.com/publications/do-elephants-have-souls)
- [Slaughterhouse](https://granta.com/slaughterhouse/)
- [Happy hens, happy world](http://www.knowablemagazine.org/article/sustainability/2018/happy-hens-happy-world)


