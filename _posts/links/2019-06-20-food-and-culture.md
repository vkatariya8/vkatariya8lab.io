---
layout: links-post
title: Food-and-Culture
categories: links
share: true
---

## Links - Food and Food + Culture

- ['This Land is Meant only for Saffron. Without it, it Means Nothing.'](https://www.eater.com/2019/2/13/18212411/saffron-kashmir-india-most-expensive-spice-climate-change) - Eater 02/2019
- [This Engineer Is Preparing to Feed a World Without Sunlight](https://www.atlasobscura.com/articles/farming-after-nuclear-disaster) - AtlasObscura 02/2019
- [Innovation that’s good enough to eat](https://www.1843magazine.com/food/look-closer/innovation-thats-good-enough-to-eat)
- [An Interview With A Man Who Eats Leftover Food From Strangers' Plates In Restaurants](https://theconcourse.deadspin.com/an-interview-with-a-man-who-eats-leftover-food-from-str-1834424806)
- 

