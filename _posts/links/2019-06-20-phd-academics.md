---
layout: links-post
title: PhD/Academics
categories: links
share: true
---

## Links - PhD/Academics

- [To PhD or not to PhD](https://www.symmetrymagazine.org/article/to-phd-or-not-to-phd)
- [Intellectual humility: the importance of knowing you might be wrong](https://www.vox.com/science-and-health/2019/1/4/17989224/intellectual-humility-explained-psychology-replication)
- 

