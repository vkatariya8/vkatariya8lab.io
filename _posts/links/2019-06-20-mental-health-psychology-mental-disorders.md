---
layout: links-post
title: Mental-Health/Psychology/Mental-Disorders
categories: links
share: true
---

## Links - Mental Health/Disorders/Psychology

- [Should Mental Disorders Have Names?](https://blogs.scientificamerican.com/observations/should-mental-disorders-have-names/) - SciAm 2019
- [How to Help Someone With Depression](https://www.thecut.com/article/how-to-help-someone-with-depression.html)
- [I am not always very attached to being alive](https://theoutline.com/post/7267/living-with-passive-suicidal-ideation)
- [Why Breaking a Streak Feels So Awful](https://www.thecut.com/2019/04/why-breaking-a-streak-feels-so-awful.html)
- [Designing for Mental Health](https://magenta.as/designing-for-mental-health-e9534ac8a7b4)
- [On Depression](https://www.theschooloflife.com/thebookoflife/on-depression/)
- [Staying awake: the surprisingly effective way to treat depression](https://mosaicscience.com/story/staying-awake-surprisingly-effective-way-treat-depression)
- [On Sylvia Plath and the Many Shades of Depression](https://lithub.com/on-sylvia-plath-and-the-many-shades-of-depression/)
- [What It’s Like to Have an ADHD Brain](https://medium.com/s/greatescape/what-its-like-to-have-an-adhd-brain-52a7c5171d8f)
- [When athletes share their battles with mental illness](https://www.usatoday.com/story/sports/2017/08/30/michael-phelps-brandon-marshall-mental-health-battles-royce-white-jerry-west/596857001/)
- [The Challenge of Going Off Psychiatric Drugs](https://www.newyorker.com/magazine/2019/04/08/the-challenge-of-going-off-psychiatric-drugs)
- 

