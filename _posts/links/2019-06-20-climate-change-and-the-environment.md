---
layout: links-post
title: Climate-Change-and-the-Environment
categories: links
share: true
---

## Links - Climate Change and Environment

- [Why We Stink at Tackling Climate Change](http://nautil.us/issue/69/patterns/why-we-stink-at-tackling-climate-change) - Nautilus 02/2019
- [How sci-fi could help solve climate change](https://grist.org/article/how-sci-fi-could-help-solve-climate-change/) - Grist 02/2019
- [Want to Help Fight Climate Change? Have More Children](https://www.bloomberg.com/opinion/articles/2019-03-14/want-to-help-fight-climate-change-have-more-children) Bloomberg 03/2019
- [Is Environmentalism Just for Rich People?](https://www.nytimes.com/2018/12/14/opinion/sunday/yellow-vest-protests-climate.html)
- [This scientist thinks she has the key to curb climate change: super plants](https://www.theguardian.com/environment/2019/apr/16/super-plants-climate-change-joanne-chory-carbon-dioxide)
- [How much can forests fight climate change?](https://www.nature.com/articles/d41586-019-00122-z)
- [Starbucks, Dunkin Race Against Bans, Taxes on Disposable Cups](https://www.bloomberg.com/news/articles/2019-04-28/starbucks-sbux-dunkin-dnkn-brace-for-coffee-cup-bans-fees)
- [The Third Phase of Clean Energy Will Be the Most Disruptive Yet](http://rameznaam.com/2019/04/02/the-third-phase-of-clean-energy-will-be-the-most-disruptive-yet/)
- [Can we truly think about climate change at all?](https://theoutline.com/post/7268/how-to-think-about-climate-change)
- [The Methane Detectives: On the Trail of a Global Warming Mystery](https://undark.org/article/methane-global-warming-climate-change-mystery/)
- [Why Recycling Doesn't Work](https://thewalrus.ca/why-recycling-doesnt-work/)
- [Carefully, Japan Reconsiders the Trash Can](https://www.citylab.com/life/2019/05/trash-cans-japan-garbage-bin-recycling-waste-tidying-up/589825/)
- [Why Carbon Credits For Forest Preservation May Be Worse Than Nothing](https://features.propublica.org/brazil-carbon-offsets/inconvenient-truth-carbon-credits-dont-work-deforestation-redd-acre-cambodia/)
- 

