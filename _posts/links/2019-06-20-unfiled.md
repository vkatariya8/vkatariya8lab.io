---
layout: links-post
title: Unfiled
categories: links
share: true
---

## Links - Unfiled

- [For the pangolin](https://samkriss.com/2019/02/16/for-the-pangolin/)
- [A Journey Into the Animal Mind](https://www.theatlantic.com/magazine/archive/2019/03/what-the-crow-knows/580726/) Atlantic 03/2019
- [Oh God, It's Raining Newsletters](https://craigmod.com/essays/newsletters/)
- [What it means, and doesn’t mean, to get a job in physics](https://gravityandlevity.wordpress.com/2019/03/25/what-it-means-and-doesnt-mean-to-get-a-job-in-physics/)
- [How Gandalf acts with his eyes](https://www.youtube.com/watch?time_continue=50&v=TzLXHViyW7I)
- [Surgeons should not look like surgeons](https://medium.com/incerto/surgeons-should-notlook-like-surgeons-23b0e2cf6d52)
- [How Not Having an Opinion Has Improved My Quality of Life](https://www.manrepeller.com/2019/04/art-of-not-having-opinions.html)
- [Fat, Not Meat, May Have Led to Bigger Hominin Brains](https://www.scientificamerican.com/article/fat-not-meat-may-have-led-to-bigger-hominin-brains/)
- [The Information Diet](https://futurecrun.ch/articles/the-information-diet)
- [Why Books Don't Work](https://andymatuschak.org/books)
- [Against Advice](https://thepointmag.com/2019/examined-life/against-advice-agnes-callard)
- [It's Okay to Be Good and Not Great](https://www.outsideonline.com/2348226/case-being-good-enough)
- [Her Left Hand, the Darkness](https://granta.com/her-left-hand-the-darkness/)
- [Ebooks for All](https://craigmod.com/essays/worldreader/)
- [Living with perfect pitch and Synaesthesia – what it’s really like](https://ljrich.wordpress.com/2014/04/29/living-with-perfect-pitch-what-its-really-like/)
- [I Now Suspect the Vagus Nerve Is the Key to Well-being](https://www.thecut.com/2019/05/i-now-suspect-the-vagus-nerve-is-the-key-to-well-being.html)
- [The Family That Feels Almost No Pain](https://www.smithsonianmag.com/science-nature/family-feels-almost-no-pain-180971915/)
- [How to be successful](http://blog.samaltman.com/how-to-be-successful)
- [Why Your Brain Hates Slowpokes](http://nautil.us/issue/71/flow/why-your-brain-hates-slowpokes-rp)
- [The Tao of Sir Terry](https://www.tor.com/2019/03/15/the-tao-of-sir-terry-pratchett-and-philosophy/)

