---
layout: post
title: "Fermentation and timescales"
description: "A WIP short essay for Dense Discovery"
date: "2022-11-12"
tags: [blog]
categories: [blog]
comments: false
share: true
---

_This is a coarsely edited short essay that my friend Kai asked me to write for his newsletter [Dense Discovery](https://www.densediscovery.com)._

I like to ferment food and drink, and given the kind of person I am, I also like to think about the process quite a bit. Of late, I've started to think of the different timescales involved in fermentation, and how I seem to be measuring life, in a way, based on them. Tepache takes a day, sauerkraut takes two weeks, sweet miso takes three months, and a salty miso can be aged for a whole year before it's considered ready.

I moved to a new apartment (in a new city!) last month, and it didn't feel like home until I started a few fermented foods and drinks in various glass jars and bottles. These things generally take time to come ready, and rather than be frustrated by the lag between making and eating, I like to embrace it. I know I'll be marking the new calendar year with some miso soup and miso ginger cookies made with miso I started last month. Similarly, a friend told me that they might be moving to the same city as me in six months, and my first response was, "let me start a batch of soy sauce now so we can taste it together when it's ready."

It feels like the timescales in my life, and perhaps all of our lives, have been mostly set externally. For a few years, I was in grad school getting my PhD and I measured life in semesters and the time between getting papers published, neither of which were really in my control. Apart from days and years, the other units of time we use: minutes, hours, weeks, months, they're all just made up. Measuring life in three-month increments of miso batches is far more relaxing and freeing--at the same time, it also feels more... wholesome and constructive. It lets me pace life with a different clock.

Part of embracing the lengthy incubation period of fermented foods is falling in love with the process. I think it's simply wonderful that putting the right ingredients in the right conditions enables microbes and enzymes to transform them in scarcely believable ways. Wonder is an emotion I try to cultivate more and more of these days, and watching my ferments do their thing is a big part of it. Encouraging and watching over these biochemical processes lets me "commune", so to speak, with life forms we generally don't interact with much. Not intentionally, at least.

Working, and being, with my ferments lets me be more intentional. When it feels like life is sweeping me up, it's nice to remember that the ferments in my apartment can't be hurried. There's a witchcraft to them, and to paraphrase Gandalf, "a ferment is never late. Nor is he early. He arrives precisely when he means to." And it's good to be there with them.
