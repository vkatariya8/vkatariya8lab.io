---
layout: post
title: "PhD thoughts (towards the end)"
description: "Rudimentary thoughts on the PhD now that it's almost over"
date: "2021-07-31"
tags: [blog]
categories: [blog]
comments: false
share: true
---

- Now that there's a definite end line for this, it's become a finite game. I don't feel like doing the things I used to do, like read fresh new papers written, coz why? I don't feel like keeping up with things or making connections, not that I ever liked that anyway.
- Constant questions of, "why even finish this?". I know that I will finish it, and that it's not super hard or anything, and if not anything else, it's employment right now. But still, I think it's a useful thing to have in perspective. It's a constructive question because the answer to it is not at all obvious. It's clearer to me now, but it wasn't clearer when I started asking it of myself.
- Less pressure? Of course, less pressure in terms of writing more papers, applying for jobs, "networking", etc.
- However, there is a sense of grief--wasting (I know it's not the right word) 4+ years on something intense that I am not going to end up using. There is also grief because math and physics and science has always been something closely tied in with my identity, and will likely not be so for much longer. It's a strange type of grief, almost like grieving a mix of things that have happened and also grieving things that will not come to pass due to me quitting academia.
- However, that grief reg. physics/math being tied to identity has a flipside. I don't know if _I_ associated myself with science, or if it was a function of my family and surroundings. But the decision to step away from academia is _mine_. It feels good. It feels good because it's the right thing to do (for my wellbeing, mental health, etc.) and also because it just feels like the _right_ decision. There are some regrets, but I don't want to keep going.
- Am I "quitting" or am I leaving? Are they all that different? I'm not quitting the PhD though, so that's nice.
- Also, how much of a role does depression play? I've come to realize that academia is very very self-motivated. I mean, yes, I knew that, but the thing is that it gets worse the more senior you get. You're more responsible for things, you're responsible for other people at times also. I don't think that, with my depression, I would be able to maintain an energetic enough self to manage all of the things I would need to. I don't feel bad about it (maybe I should), it is what it is.
- I am very intimidated by the fact that I could be a PhD advisor to someone. It scares me. I think that when I'm in the zone and well engaged, I would do a good job. However, there are times when I'm not, when I am really not in the space for work, and I don't think I would be a good advisor then.
