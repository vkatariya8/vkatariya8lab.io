---
layout: post
title: "How to Take Smart Notes, by Sonke Ahrens"
description: "Use your brain as a processor, not as a hard disk"
date: 2018-11-11
tags: [books, blog, productivity]
categories: book-reviews
comments: false
share: true

---

I'd heard some good things about this book and read it a few weeks ago after an age of procrastination and delay. It was a good read. The author talks about the _Zettelkasten_ method of taking notes. This is a process where you create a personal "slip-box" of notes for yourself. Every idea you have, every significant note about something you've read; the aim is to capture these all in a comprehensive way. The most important thing, however, is to make connections between new notes and old ones. When done well, this system leads to an interconnected web of ideas that constantly grows, and from which you can draw inspiration, ideas or inferences.

The book is written for an academic; reading the literature, you are bound to come up with constructive ideas and comments. Instead of mentally taking note of these ideas, the method is to note them down actively in a complete form as you come across them. At the end of each day, these ideas or notes get added to your mesh of existing notes, and you cross-reference new cards (as they originally were, one card for each note) to old ones and old ones to your new ones. 

_A good structure is something you can trust. It relieves you from the burden of remembering and keeping track of everything. If you can trust the system, you can let go of the attempt to hold everything together in your head and you can start focusing on what is important: The content, the argument and the ideas. By breaking down the amorphous task of “writing a paper” into small and clearly separated tasks, you can focus on one thing at a time, complete each in one go and move on to the next one._

I was inspired by this book in a specific way. I don't know if the specific _Zettelkasten_ method will work for everyone, but the spirit of the program is this: by freeing up your brain from _remembering_ all the ideas you need, why not outsource that task to an external system? If done well, a personal note archive serves as a second brain, so to speak. If you can trust it to contain and incubate all your ideas, then you free up your brain to work on the ideas you are currently thinking about.

I've had this thought for a long time where I think of the brain as a mixture of computer and memory. The brain does a good job at both of these, but sometimes memories accumulate and take up so much space (both literally ane metaphorically) that you need to do some cleaning. What if you did this cleaning up systematically and at specific times? Then it doesn't become organizing; it becomes outsourcing. Your brain is completely freed up to only act on the present thing at any point of time. I'll encapsulate this in a single sentence: treat your brain like a processor, not as a memory unit. It's easier to outsource your memory to a physically embodied system, so why not do that? I've been trying to do so, and really, if I have quick access to my well-organized and labeled notes, why store it all in my head?

update: 17 Nov. 2018

Passages highlighted from the book.

_With that in mind, it is not surprising that the single most important indicator of academic success is not to be found in people’s heads, but in the way they do their everyday work. In fact, there is no measurable correlation between a high IQ and academic success – at least not north of 120._

_Every task that is interesting, meaningful and well-defined will be done, because there is no conflict between long- and short-term interests. Having a meaningful and well-defined task beats willpower every time. Not having willpower, but not having to use willpower indicates that you set yourself up for success. This is where the organisation of writing and note-taking comes into play._

_The best way to maintain the feeling of being in control is to stay in control._

_To get a good paper written, you only have to rewrite a good draft; to get a good draft written, you only have to turn a series of notes into a continuous text. And as a series of notes is just the rearrangement of notes you already have in your slip-box, all you really have to do is have a pen in your hand when you read._



