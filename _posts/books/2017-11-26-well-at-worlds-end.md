---
layout: post
title: "The Well at the World's End"
description: "One of the first modern fantasy novels"
date: 2017-11-26
tags: [books]
categories: book-reviews
comments: false
share: true
---

The Well at the World's End

I'm very interested in fantasy literature, and enjoy reading it very much. A few months ago I'd read _The King of Elfland's Daughter_, and now I've just read an even older book by William Morris. It was first published in 1896 and is considered to be one of the first modern fantasy works in English.

I found that the book was quite misogynistic - is this something that I should have expected given the timeline of its publication? The important characters all seem to be men, and the women are just there, doing what women are "supposed to do"? I felt a little irritated with this, but I don't know enough about the times in the nineteenth century, so I'll let this pass. But I'll go ahead and say that this was the most major thing I felt about the book in the first one hundred and fifty pages.

The writing is good! Plot development is nice, although a lot of details are encapsulated and even hidden from the reader. When reading a fantasy book, I like to be in the thick of things, knowing of the various nuances happening, but this book focuses on only person's quest for the most part, and doesn't have vivid descriptions of nice scenes. Again, I am assuming that this is how books were back then.

So many of the usual devices that you find in fantasy literature are present in most of the books you will read. The central object in question in this book, the well at the world's end, is a well whose waters provide a sort of immortality. The theme of immortality has been dealt with so many times in fantasy literature; and this is quite interesting. It is one of humankind's primary goals to live forever. (If you're interested in a more realistic treatment of this idea, read _The Denial of Death_ - it's fantastic and I should re-read it soon) So yes, coming back to this book, Ralph of Upmeads wishes to go a-questing and after hearing of it from many people, decides that his quest will be to drink from the well at the world's end. What I liked about this was that he chose it just out of spirit of adventure (partially, at least - if I told you why exactly he does it, it would be a spoiler).

The geographical area covered in this book is _tiny_. Really tiny. Major towns are only thirty miles apart and can be traveled between on horseback in a day or so. This is something I've noticed quite often in fantasy books - the distances are automatically constrained by the fact that horseback is the primary mode of transport.

There are other things that are similar in other fantasy books - the presence of a sage who is wise and acts as a guide. Haven't we seen so many of these? There's another similarity with _The Lord of the Rings_, but I think I will not write it here because it'll be a spoiler, again?

I am not criticising the book because of these things, nor am I criticising the genre of fantasy. Some things need to stay the same so that readers are comfortable looking at other things changing. I, for one, enjoy horses and swords and knights. Attempting to make sense of immortality is a good thing; and I enjoy looking at how various authors think about it.

This book does not have much in terms of worldbuilding. The setting seems to be in Britain near the coast, and the "world's end" where the well is at is just the sea. That's fine. I enjoyed reading the adventures, and the interplay of the characters was nice. There was a deliberate placing of people in places and Morris had good control over the plot, although it did get a little draggy towards the end. 

I was coming into this book slightly expecting the characters to fall flat in terms of depth and complexity. And after reading the first hundred or so pages, I felt like this was going to be the case. However, it wasn't to be - the characters were complex, there is romance, kingship, lordship, thralldom and more in this book. They are all treated well, and looked at from various angles. Except the romance, perhaps. Haha.

Characters are quite decisive, and we don't get to see too much of any soul-searching happening. Truly, it is a happening world and it seems like someone told everyone : "get down and do your job". There isn't too much scheming or soliloquiy. I would have liked that. 

But overall, I enjoyed the book and even finished it as it ran past five hundred pages. It was instructive to read and see how William Morris inspired an entire generation, or cohort, of writers to adopt certain aspects and mannerisms in their own fantasy writing. For a trailblazer (arguably), this is a very good book and holds its own.