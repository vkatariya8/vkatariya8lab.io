---
layout: post
title: "Autumn, by Karl Ove Knausgaard"
description: "A sometimes silly exercise in mindfulness"
date: 2018-02-25
tags: [books]
categories: book-reviews
comments: false
share: true
---

When I picked this book up at my local library, I felt a little measure of awe, because all I had heard of Knausgaard till then pertained to his _My Struggle_ series. This book, however, is not heavy or forbidding in any way. It opens very nicely and poignantly, with a letter to his unborn daughter. The book is a compilation of short essays (sometimes so short that you'd be embarrassed to call them essays) about commonplace things in Knausgaard's life. It reads series a number of writing exercises. Sometimes whimsical, sometimes obvious, the essays are not all very impressive. However, I found that reading the book was an exercsie in mindfulness. I spent two pages and just as many minutes thinking about one particular thing - the sun, badgers, fingers, and so on. Knausgaard has a gift for metaphors and similes, and that shines through. He has ways of putting things across that I may not have ever thought of. 

I'll share a couple of very nice passages from the very first essay, which is the first letter to his unborn daughter, who will go on to be called Anne.

> What makes life worth living?
> 
> No child asks itself that question. To children life is self-evident. Life goes without saying: whether it is good or bad makes no difference. This is because children don’t see the world, don’t observe the world, don’t contemplate the world, but are so deeply immersed in the world that they don’t distinguish between it and their own selves. Not until that happens, until a distance appears between what they are and what the world is, does the question arise: what makes life worth living?

This one that follows is one single sentence that rambles and meanders, but evokes lovely images. 

> That’s the experience I’ve gained from working in the garden: there’s no reason to be cautious or anxious about anything, life is so robust, it seems to come cascading, blind and green, and at times it is frightening, because we too are alive, but we live in what amounts to a controlled environment, which makes us fear whatever is blind, wild, chaotic, stretching towards the sun, but most often also beautiful, in a deeper way than the purely visual, for the soil smells of rot and darkness, teems with scuttling beetles and convulsing worms, the flower stalks are juicy, their petals brim with scents, and the air, cold and sharp, warm and humid, filled with sunrays or rain, lies against skin, accustomed to the indoors, like a soothing compress of hereness.

After reading this letter to Anne, I was excited. If the first essay is so good, this book must be a treat. Unfortunately, that didn't happen. Of the maybe fifty or sixty essays that comprise this book, I loved only ten or so. The book was still fun to read, because it was slow, aimless, with no plot and no suspense. It's the sort of book that I would suggest to somebody waiting for ten minutes in a queue. 

I found that Knausgaard was very personally open about his life and all its aspects - his childhood, parents, his current family, how and where he lives, etc. When I looked up on the internet, I found that that is indeed true - people enjoy his work because he lets the reader so far into his own life. Is that a good thing? Perhaps. He had to do it in this compilation, otherwise the whole thing would fall flat. But it's not something I particularly liked - if a book is not an autobiography, it doesn't necessarily need to refer to the author so much.

Some of the essays were written with naiveté - I could not imagine how Penguin Press agreed to put this stuff out! Perhaps that is the point? In an age where "adult" colouring books sell in massive droves, this will sell too. There is nothing inherently wrong in this, however - I enjoyed reading this book along with its virtues and faults. 

Some of my favourite essays were the ones about badgers, the sun, and about wars. I'll share some excerpts here. I don't know much about living in Scandinavia, and this book offered a very transparent and wide lens through which I could look. That was a nice feel. I'm a little in awe of living in a place where you can _drive_ to a fjord - would that not be magnificent? 

> I drive fast but not very fast, and I never take risks. What I like best of all talking to the children while I drive, for a space is created between us as we move through the open landscape out here, it's as if the distance between what they think and what they say is abolished in the car, as if they can talk to me about anything.

> There are few sights I find more beautiful than that of lightning, and the sound of thunder always heightens the sense of being alive. Water and air, rain and clouds, they too have been here for ever, but they are such an integral part of life that their ancientness is never apparent in our thoughts or emotions, contrary to lightning and thunder, which only occur now and then, during brief intervals which we are at once familiar with and foreign to, just as we are at once familiar with and foreign to ourselves and the world we are a part of.

The poignancy or deepness is not faked - the genuineness of Knausgaard shines through, both in a good and a bad way. The essays are perhaps even him taking breaks between spurts of more serious writing, and I find that nice - he has his guard a little down and wants to expend less effort - thus revealing more and being more true to what he believes in. 

The essays get less and less interesting as the book goes on, and I had to skim through the last part. It seemed like an agreement had been signed between reader and writer - both parties lose interest in the work as it goes on, and so it is. I didn't really have a problem with that, since I could just skim through the parts that I found boring. 

The copy I read had illustrations done by Vanessa Baird - they were magnificent. The book was very well printed, and I'm very glad to now be a member of the library so I can get back to reading good ol' hardbacks. It's a great feeling, and while I like my Kindle for being lightweight, the heft of a hardback is quite the thing.