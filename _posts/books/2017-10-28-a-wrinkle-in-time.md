---
layout: post
title: "A Wrinkle in Time"
description: "One of the great books for children."
date: 2017-10-28
tags: [books]
categories: book-reviews
comments: false
share: true
---


I must confess that I hadn't read _A Wrinkle in Time_ until last week. Well, now that's one piece of guilt off my conscience, and what a book. I personally love books for children, even though I'm now 20-something. And the best books for children are the mature ones that treat children as complete human beings, with the necessary faculties for achievements, decisions, and mistakes. 

_A Wrinkle of Time_ is truly a masterpiece. I am sure I can read this book once every year for the next twenty years and still be able to enjoy and relate to it. I've started taking notes between sittings of readings, and when I was halfway through, I noted to myself that the sheer innocence of the book was magical. The book is about outlier children, and is a sort of tribute to them. Madeleine L'Engle understands that just like adults, children are multifarious must be treated accordingly. Meg may not be good at history or writing sentences, but she is very good at math. 

I sometimes try to accord a genre to the books that I read. _A Wrinkle in Time_ got science-fiction, fantasy, and children. However, it doesn't really fit into science fiction. The book uses the platform and eccentricities of science fiction to talk about real and physical things. Bullying in school. Being picked upon. Forming close groups with the people who accept you. Living in your own world when alone or with the people who understand you. 

I really love Meg, because she is so real and genuine. She's all of fifteen years old, and has all the complexities in character you would expect from a young growing-up child. Her virtues, faults, everything, shine forth. She's real and genuine, and it's surprising how well you get to know her from a book that's not even two hundred pages long. I got the feeling that L'Engle modeled Meg after someone important in her life, a stubborn maternal figure, I think. She's so ..maternal. 

Music plays an important role in the book. Aunt Beast, a temporary caretaker of Meg on a strange planet, sings a lovely melody to Meg that she can't understand but which just _heals_ her. On the planet Uriel, the three protagonist children Meg, Charles Wallace and Calvin are treated to the best and most ethereal music that they have heard. The music can't be translated to human tongues without losing some of its original charm - the paragraph that explains this both belongs and doesn't belong in a book targeted at children. 

My favourite line of the book is when Mrs. Whatsit tells Meg, "Meg, I give you your faults" when she is giving the three children advice to do well on their quest. This line stuck with me throughout and in the end, it was the real advice that Meg needed - I won't say what happened because I'll be spoiling the book, but this, to me, is the heart of the book. 

The book has humour too. As a reader, you will grow fond of the three maternal figures guiding the three children - Mrs. Which, Mrs. Whatsit and Mrs. Who. They are fun, funny, and yet wise. The fact that there are three of them may be an allegory to some past mythical or religious tale, but why take anything away from the author. The three of them just _had_ to be women, they could not have been men, and they should not have been. 

> "If you have some liniment I'll put it on my dignity," Mrs. Whatsit said, still supine. "I think it's sprained."[...]

Meg is the real hero of it all; she is not special in a unilateral way, she is referred to multiple times as a mix between a human and the special being that Charles Wallace is, but she does her best and she stays herself throughout the book. 

> "Do you have the courage to go alone?" Mrs. Whatsit asked her. Meg's voice was flat. "No. But it doesn't matter."

Do yourself and your inner child a favour and read this book _now_. The version I read was only 160 pages long, and I finished it in a couple of hours. You might want to re-read it as soon as you're finished - I wanted to do that too. What a great combination of characters, plot, and knowledge. Surely on my list of favourite books. 