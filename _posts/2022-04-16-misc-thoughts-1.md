---
layout: post
title: "Random thoughts 1"
description: ""
date: "2022-04-16"
tags: [blog]
categories: [blog, short-lists]
comments: false
share: true
---

I haven't blogged for a while, and it's frustrating because I have at least ten partially written posts on my laptop which I haven't gotten around to completing. So! Today I will blog randomly just to get a post out and then use that momentum to finish my incomplete ones.

Here are some miscellaneous things:

- I'm traveling to a friend's farm in two days and also visiting some other friends along the way. I'm excited and since I'm traveling by train, I plan to take a big bag of ferments (food and fermented sodas) with me. It's going to be so great and I'm going to have to remember to burp all the ferments every eight hours when traveling.
- Last night I was thinking of a friend who I used to be close to but am not so much anymore. I wanted to text them, but with something specific rather than "hey how's it going?". I was lying in bed trying to conjure up something that happened recently that reminded me of them.. and I did! Well, to be particular, I remembered an answer in a quiz show that I was watching the other day. I knew the answer, somehow, and it turns out it was because my friend told me about it! Coz they went there on vacation. I found this whole thing quite surreal--I think what happened is that I started missing my friend _because_ they led me to the answer in the quiz show and then that missing percolated upwards to my conscious mind.
- I really really love doing things with my hands. The tactile experience is wonderful. My senses are better engaged (as compared to, say, reading). I feel like I am exploring new terrain in terms of defining my personality. This is interesting especially, because in the past my identity was comprehensively tied to being good academically. 
- I've been juggling for the past week, teaching myself. It's been a nice meta-experiment as I'm watching how I teach myself to learn it. I've not looked at anything on YouTube or elsewhere. I think this has made the whole thing more rewarding. Juggling is another thing that has me feeling good about doing things with my hands.
- I miss making lists. I am catching myself these days doing things, albeit incorrectly or incompletely. So I think it's important that I make lists or checklists and then do the things I'm wanting to do.

OK I'm out. This was fun to write actually.

