---
layout: post
title: "[incomplete] Flywheel theory of hobbies (and other things)"
description: "set of notes about the momentum of hobbies and how it can cut both ways"
date: "2022-05-19"
tags: [blog]
categories: [blog]
comments: false
share: true
---

Again, a very rough set of notes that I'd like to expand upon at some point. I think about this a lot.

- I've always maintained a number of concurrent hobbies.
- This is usually good, because it makes life meaningful in a variety of ways.
- Further, there is a sort of flywheel effect. Each independent "thing" gives me momentum, joy and motivation to do the next thing. When all the things are "working", they all move the flywheel, and it makes me get along well! Also it keeps me robust, in the sense that I have ways to pick up the slack if I slow down or pause some facets of life.
- This is why I call it a flywheel: each hobby (or each activity) adds to the overall momentum of the flywheel and lets me smooth out the kinks in life while also maintaing a certain level of enjoyment.
- I think I think this way because of being depressed. I want to actively cultivate this flywheel so I can have the energy to stave off major depressive episodes when they threaten.
- Anyway, to continue the analogy.. a flywheel is heavy. It needs momentum to keep going. This is why I can have some periods of catastrophic slowing-down. This is when more and more aspects of life stop working, and then it all comes to a halt.
- Because of the way I live my life (doing lots of different things), these crashes feel terrible, and they make it really hard to build up to the previous momentum of the flywheel.
- It's not lost on me that this also seems similar, in a way, to a hamster wheel. I seem to be in a loop of my own making. In the sense that it has arisen because my coping mechanism has been "do lots of things".