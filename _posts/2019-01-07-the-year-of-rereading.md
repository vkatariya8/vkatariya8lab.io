---
layout: post
title: "The year of rereading"
description: "What happens if I attempt to reread books that have been important to me?"
date: "2019-01-07"
tags: [blog, books]
categories: blog
comments: false
share: true
---

Last year, as I wrote in a previous post, I read over a hundred books. I didn't expect to feel major euphoria or joy, but I still felt less than I thought I would. Turns out that the people who call the number of books read in a year a vanity metric are partially correct. I read a lot, I remember significant amounts of what I've read, but the sheer quantity of books didn't merit much in my head. Therefore, I'm pivoting this year to move to a different goal: read fewer books, but read deeper.

Over the past few years, I've consciously/subconsciously gravitated towards books that have been easy to read so that I could further my book count. In this process, I'd set by the side classics that were long and would take time to plow through. Yes, I read short classic works, as long as they were short. But I was skipping the novels of Charles Dickens, and I was avoiding the thought of _Infinite Jest_, and _Ulysses_. Why spend three weeks on _one_ book when in that time I could read twelve sci-fi novellas? As it turns out, a year of chasing and achieving numbers told me all I needed to know: the number of books doesn't matter as I, or some other people, make it. 

My resolution for 2019? As far as possible, I want to read long books that I've been putting off, and I also want to reread the books that I've mentally shelved as "important", "inspiring", and "classic". The definition of "classic" that I am most drawn to is that a classic is a work of literature that stays relevant through generations, and there is new value and insight to be gained with each rereading at a different stage in life. I'd read formative books by Vonnegut, Steinbeck, etc. in my undergrad and high school days, and it seems that four years later, I could use an injection of the philosophies they offered. I also idolize a number of fictional characters for their talents, empathy, and powers of concentration. One of the best examples of this is Muad'Dib, from _Dune_. When I read _Dune_ in a single day in 2017, I took in the adventure, excitement and philosophy all in one gulp. If I go back and read it now with foreknowledge of the plot and outcome, I can focus more on the things I can take out of the book. One of my favorite aspects of the character of Muad'Dib/Paul Atreides is this:

> ... we can say that Muad’Dib learned rapidly because his first training was in how to learn.

Last night I was rereading _The Adventures of Sherlock Holmes_ after borrowing it on a whim from the library. It has been delightful. I was beginning to forget a number of qualities of Holmes that I'd admired, and this was a good reminder. As an researcher-in-training, the powers of detachment and concentration that Holmes brings are the best possible to emulate. I am also attempting to build a personal knowledge system for myself, and Holmes' index of information about people and other entities is somewhat what I want. I could go on about other specific aspects, but you get the point. 

I expect that as I reread _East of Eden_, _Cat's Cradle_, _For Whom the Bell Tolls_, I will recall other things that I admire. They exist only vaguely in my head now, but soon they will be revived. And I'll be the better for it. 
