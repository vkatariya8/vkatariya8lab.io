---
layout: post
title: "Small and Big Cities"
description: "My thoughts on living for a year in a small city"
date: "2018-11-26"
tags: [blog,personal]
categories: blog
comments: false
share: true
---

In 2017, I moved to live in Baton Rouge, which despite being the state capital of Louisiana, is a small city. When I decided to move to the United States to pursue my graduate studies and research, my initial and default dream involved a large city like New York or San Fransisco, where every research hiccup would be associated with a visit to a brand new museum, or cafe. In a large city, I thought, it would be easy to go four years without exhausting all the new things I could do and try out. So when I did select Baton Rouge (for a number of reasons), I knew I would have to forgo those dreams. My city only has a handful of museums. It has substandard public transport, and a limited number of options for anything I would wish to do. It sounded disappointing, but useful in a productive way--I'd always be holed up at my desk, working, because nothing around me would appeal enough to distract me.

A significant amount of time later, I reflect and find that yes, that is true, but there's another side to the smallness of the city that I didn't appreciate. My weekend jaunts are limited to four or five of my favorite spots in the city, and I've come to appreciate those spots intimately. I know the farmers at the farmers market, I know the layout of my favorite grocery stores, I know how exactly how to locate a book I want at the public library, and I know exactly which roads are good to bike on (aided by the fact that there are only a few of these). I don't go to a new restaurant every weekend; rather, I go to the same pizza place next door to my house and order the same pizza each time. The routine and familiarity is comforting.

I've always thought of routine and familiarity as a sort of structure to life--the boring things you do every day form the framework within which you are allowed to experiment with your more creative endeavors. With living in Baton Rouge, it's awfully similar. My excursions to the city are limited to a few spots, but these spots give me both physical and temporal markers around which to plan my weekends, domestic life, reading habit and physical exercise routine. I still have a number of options to explore in these aspects, but they are more subtle and nuanced ones. In some sense, I've been forced to know my city in more detail, and I'm glad for it. 
