---
layout: post
title: "Bread Baking as Mental Model"
description: "Bread baking as a mental model for process and timescales"
date: "2018-12-21"
tags: [bread,blog,thinking]
categories: blog
share: true
---

It's been a while since I've blogged here at all. I've been on a bit of a break from everything, which has been refreshing. Not being at home for a while, I've been unable to carry on with my routine of baking two loaves of bread every weekend. Of course, I miss the comfort and regularity of it, but I'm also realizing that I think of the process of baking bread as a fundamental paradigm of activities. I don't know if it's accurate to say so, but it seems like I'm thinking of bread as a "mental model" of process.

Let me explain. To bake bread, there are multiple stages involved. It's a far cry from other baking processes, which are some variation or complication of: prepare ingredients, mix, shape or pour, and bake. With baking bread, once you fix your recipe, there are two (or more) stages of fermentation you have to manage; you have to deal with the temperature and your available times, and more. With the bread I bake currently, if I want a loaf of bread for dinner on Saturday, I have to start the process on Thursday evening so that I can extract maximum flavor from the extended fermentations I give the dough.

I've never had the need to "incubate" something for so long before preparing it. I've been able to imagine long, long periods of work and wait, such as when you work towards writing an exam, or writing a paper. Before baking bread, I didn't appreciate work that takes time on the order of multiple days. This is part of what I mean by a mental model of process that bread gives me. 

Additionally, when I attempt or analyze something, I try to look for steps of waiting/ideating that are analogous to the long fermentations that I give my bread dough. I can't think of quick examples now, but one not so relevant one is going to sleep with an idea or issue in your head. You usually have clarity once you wake up. Somehow, the idea has tumbled inside your head subconsciously for a while to give you some opinions. When you let bread ferment at a low temperature for an extended period, you do a similar thing. 

Perhaps this may not seem like much, or it might be obvious to the point of me insulting you, the reader, but the conclusion I want to draw from this is that there are some processes and timescales that it's useful to get comfortable with. Baking bread for many months has somehow taught me to be more appreciative of processes that take one or two days to come to fruition. It has also taught me to _think this way_: to try and see where anything in my life can be likened to a particular step of baking bread. If yes, does that give me some additional insight into it? To me, that's how a mental model should work, so yes, bread baking has given me a new mental model that I've learnt over time.
