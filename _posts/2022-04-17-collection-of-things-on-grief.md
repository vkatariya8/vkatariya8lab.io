---
layout: post
title: "A collection of things on grief"
description: ""
date: "2022-04-17"
tags: [blog]
categories: [blog, collections]
comments: false
share: true
---

Last updated: 21st November 2022

Recently a friend and I were talking about grief and we exchanged notes. I sent them a bunch of things I've collected from various places that talk about grief and loss in a poignant and useful way. I'll keep this list updated.

<br/>

* * *

<br/>

- [The Thing Is](https://www.poetryfoundation.org/poems/151844/the-thing-is), a poem by Ellen Bass

to love life, to love it even  
when you have no stomach for it  
and everything you’ve held dear  
crumbles like burnt paper in your hands,  
your throat filled with the silt of it.  
When grief sits with you, its tropical heat  
thickening the air, heavy as water  
more fit for gills than lungs;  
when grief weights you down like your own flesh  
only more of it, an obesity of grief,  
you think, _How can a body withstand this?_  
Then you hold life like a face  
between your palms, a plain face,  
no charming smile, no violet eyes,  
and you say, yes, I will take you  
I will love you, again.  
  
<br/>

---

<br/>

- "Each person’s grief is as unique as their fingerprint. But what everyone has in common is that no matter how they grieve, they share a need for their grief to be witnessed. That doesn’t mean needing someone to try to lessen it or reframe it for them. The need is for someone to be fully present to the magnitude of their loss without trying to point out the silver lining."

from [Our Experience of Grief is Unique as a Fingerprint](https://lithub.com/our-experience-of-grief-is-unique-as-a-fingerprint), an online article on Lithub

<br/>  

---

<br/>

- "Among the many lessons Hannah taught me that day, and in the days to come, was the significance and seriousness of grief in human lives—which is another way of saying the significance of love in our lives, because it is the fever of our attachment to one another that charges grief with its intolerable brilliancy. And so, my meeting with Death that day was as much a meeting with Love."

from [On Death and Love](https://emergencemagazine.org/essay/on-death-and-love/), a personal essay by Melanie Challenger in Emergence Magazine
  
 <br/>

---

<br/>

- "This funk had dragged on, however, and I couldn't help but imagine what his week of mourning in California must have been like. How the coming of dusk dissolves the laces of the splint that holds you together during the day, and the desperate sadness that follows can be anesthesized only with sleep. The heaviness of opening your eyes the next morning when you realize you've begun another day of grief, so pervasive that it removes even the taste from the food that you eat. I knew that when someone you love has died, you feel that you have also. And I knew that there was nothing that I, or anybody else, could do to fix it."

from LAB GIRL, a book by Hope Jahren. Here she's describing how her best friend deals with stuff.
  
<br/>

---

<br/>

- "When your family explodes (implodes?) it’s the big stuff that hurts, for a while, like the impact of a slap, but that fades, quite quickly, because you have to get used to it, and the way to get used to it is, basically, not to think about it. It’s the little things like this that get you, forever as far as I can tell. I turned the pages, carefully – they weren’t brittle, but soft, almost bruisable, and they felt as though they could have come off in my fingers, like petals tugged from a daisy. I suppose it’s the fact that these small memories come from the kind of tiny reminders that you simply can’t predict, and so can’t protect yourself from, and they catch you, paper cuts across the heart." 

from The Lost for Words Bookshop, by Stephanie Butland

<br/>

---

<br/>

- Alright, here goes. I'm old. What that means is that I've survived (so far) and a lot of people I've known and loved did not. I've lost friends, best friends, acquaintances, co-workers, grandparents, mom, relatives, teachers, mentors, students, neighbors, and a host of other folks. I have no children, and I can't imagine the pain it must be to lose a child. But here's my two cents. I wish I could say you get used to people dying. I never did. I don't want to. It tears a hole through me whenever somebody I love dies, no matter the circumstances. But I don't want it to "not matter". I don't want it to be something that just passes. My scars are a testament to the love and the relationship that I had for and with that person. And if the scar is deep, so was the love. So be it. Scars are a testament to life. Scars are a testament that I can love deeply and live deeply and be cut, or even gouged, and that I can heal and continue to live and continue to love. And the scar tissue is stronger than the original flesh ever was. Scars are a testament to life. Scars are only ugly to people who can't see. As for grief, you'll find it comes in waves. When the ship is first wrecked, you're drowning, with wreckage all around you. Everything floating around you reminds you of the beauty and the magnificence of the ship that was, and is no more. And all you can do is float. You find some piece of the wreckage and you hang on for a while. Maybe it's some physical thing. Maybe it's a happy memory or a photograph. Maybe it's a person who is also floating. For a while, all you can do is float. Stay alive. In the beginning, the waves are 100 feet tall and crash over you without mercy. They come 10 seconds apart and don't even give you time to catch your breath. All you can do is hang on and float. After a while, maybe weeks, maybe months, you'll find the waves are still 100 feet tall, but they come further apart. When they come, they still crash all over you and wipe you out. But in between, you can breathe, you can function. You never know what's going to trigger the grief. It might be a song, a picture, a street intersection, the smell of a cup of coffee. It can be just about anything...and the wave comes crashing. But in between waves, there is life. Somewhere down the line, and it's different for everybody, you find that the waves are only 80 feet tall. Or 50 feet tall. And while they still come, they come further apart. You can see them coming. An anniversary, a birthday, or Christmas, or landing at O'Hare. You can see it coming, for the most part, and prepare yourself. And when it washes over you, you know that somehow you will, again, come out the other side. Soaking wet, sputtering, still hanging on to some tiny piece of the wreckage, but you'll come out. Take it from an old guy. The waves never stop coming, and somehow you don't really want them to. But you learn that you'll survive them. And other waves will come. And you'll survive them too. If you're lucky, you'll have lots of scars from lots of loves. And lots of shipwrecks.

This is one of my favourite Reddit comments. [Source](https://www.reddit.com/r/garysully1986/comments/6g3brt/gsnow_on_grief/)

<br/>

---

<br/>

- "Grief is forcing new skins on me, scraping scales from my eyes. I regret my past certainties: Surely you should mourn, talk through it, face it, go through it. The smug certainties of a person yet unacquainted with grief. I have mourned in the past, but only now have I touched grief’s core. Only now do I learn, while feeling for its porous edges, that there is no way through. I am in the center of this churning, and I have become a maker of boxes, and inside their unbending walls I cage my thoughts. I torque my mind firmly to its shallow surface alone. I cannot think too much; I dare not think too deeply, or else I will be defeated, not merely by pain but by a drowning nihilism, a cycle of thinking there’s no point, what’s the point, there’s no point to anything."

from [Notes on Grief](https://www.newyorker.com/culture/personal-history/notes-on-grief) by Chimamanda Adichie on the passing of her dad.

<br/>

---

<br/>

- "Grief from loss is like the butter in a puff pastry. It's huge and seemingly impenetrable. It sits heavy, smothering everything. But eventually it gets folded in. It's never gone, but instead infiltrates every part. It's there, but softer in texture and flavor, integrated into your life from here on out, informing each moment. Until one day, it is simply part of you that feels okay."

from this lovely [Instagram post](https://www.instagram.com/p/CgmkpyZuKiT/?hl=en).

<br/>

---

<br/>

- "If, as a culture, we don’t bear witness to grief, the burden of loss is placed entirely upon the bereaved, while the rest of us avert our eyes and wait for those in mourning to stop being sad, to let go, to move on, to cheer up. And if they don’t — if they have loved too deeply, if they do wake each morning thinking, I cannot continue to live — well, then we pathologize their pain; we call their suffering a disease."

from Cheryl Strayed's 2002 essay [The Love Of My Life](https://thesunmagazine.org/issues/321/the-love-of-my-life).

<br/>

---

<br/>

- "It seems to me that our existence itself is kept aloft on an infinity of absences. All our lives are lived on the boundless tide of sorrow of those who have passed before. We lurch around the world in all our desperate and splendid humanity and, whether we realise it or not, our lived condition is forever saying “Whatever happens now is for them.” This is how we honour humanity itself, as the living testimony of those no longer with us. We who remain, continue. 

from [The Red Hand Files 211](https://www.theredhandfiles.com/how-is-earl/).

<br/>

---