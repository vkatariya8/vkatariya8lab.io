---
layout: post
title: "Book Data"
description: "Organisationalists rejoice"
date: 2017-07-26
tags: [data, books]
comments: false
share: true
category: books
---

I'm a productivity and organisation obsessed person, and this obsession falls out into spheres of my life where I make myself log a lot of things about myself in neat spreadsheets. I've been tracking the books I've read for quite some time now on my own spreadsheet. I'm going to put up a couple of cool graphs from what I've found so far. I started this logging in December 2014. 

![](https://dl.dropboxusercontent.com/s/7v2bu4746wrky6b/book-genre.jpg)

***

![](https://dl.dropboxusercontent.com/s/84stll9x703usrv/page-genre.jpg)

***

![](https://dl.dropboxusercontent.com/s/x1z30tzurjfr4fc/book-length.jpg)

***

![](https://dl.dropboxusercontent.com/s/twy19s0a8vxnlzq/book-pub.jpg)

