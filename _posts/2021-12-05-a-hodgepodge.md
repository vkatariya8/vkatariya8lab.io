---
layout: post
title: "A little hodgepodge of stuff"
description: "List of a few things from this week, unfinished thoughts"
date: "2021-12-05"
tags: [blog, short-lists]
categories: [blog]
comments: false
share: true
---

A hodgepodge of things from this week which might later become full posts in themselves.

*The ergodicity of friends*

I've realized that a lot of the times, the reason I get disappointed in people is because I impose some expectations on them. One way, which I've been using a lot these days, is to do away with the idea that I should expect any specific thing from any specific person. Instead, I am going to revel in the more expansive and comfortable feeling that all my friends put together are going to take care of me and my needs and fulfilment. Even writing that feels good. It's a "broader" and nicer feeling than the "narrow" one of expecting certain things from certain people. In the latter case, I set myself up for disappointment. In the former, I set myself up to be surprised and pleased.

*Wholesome feelings and reciprocity*

I had a theory which I tried to explain to a friend in a voice note that the truly wholesome feelings are the ones that you can reciprocate. Like, I can be proud of my friend and they can be proud of me. But we both can't be envious of each other. But that argument broke down when I realized that we both can be angry at each other. So I stopped thinking about it.

*Quantity is perhaps more important than quality in my friendships*

I realized that I sometimes get upset when I tell someone I'm having a bad time, and they offer to listen if I want to talk. Feeling upset for that made no sense, until I realized that I didn't actually trust them. They weren't around _enough_ for me to trust their company and their presence after I'd made myself vulnerable. The valuable currency, then, is quantity of time spent with someone. This is a good proxy, I think, for their presence in my life. This works primarily because I think I have high standards for friends. And of course, there is a chicken-and-egg at play here. This is important for me to note because it lets me avoid feeling disappointed in people for no reason except for the way I conduct my friendships.

*Understanding things in the flow*

A bunch of friends attended my PhD defense talk, which was great. It was instructive to talk to them later about what they thought. Valerie, in particular, gave me interesting feedback. In my talk, there was a lot of technical jargon as well as a bunch of facts expressed mathematically with a lot of abstract terminology. When I showed those expressions or inequalities, I could sense some blankness from the audience. I'd anticipated that so I prepared qualitative explanations of them too. As soon as I said that, I could sense the audience loosen up a bit briefly, and this was very clear in Valerie's face. What she said on the following day was that she "got" things in the flow; i.e., she understood the immediate reason to introduce a new quantity, or prove a certain inequality. However, she couldn't repeat it on the following day. This made me pleased, because I realized later that it was exactly what I was going for. I wanted to make a narrative for the work we did, and wanted to make the narrative make sense to a layperson even if I couldn't convince them of the exact import of our results.
