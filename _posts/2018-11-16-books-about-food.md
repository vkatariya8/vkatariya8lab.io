---
layout: post
title: "Books about Food"
description: "Intimate books that become friends"
date: "2018-11-16"
tags: [books,food,blog]
categories: [blog, food]
comments: false
share: true
---

In the past few months, I've had the good fortune to read a number of excellent books about food. I'm not talking about recipe books, but the book analogues of _A Brief History of Time_. A popular-science, completely accessible history and walkthrough of _why_ and _certain_ foods are the way they are. As someone who enjoys cooking and especially experimenting when cooking, it is always rewarding to know the history and reasons behind our culinary habits.

It started off in May 2018, when I read _Taste, Memory_ by David Buchanan. It grabbed me at the library because of its title. An audacious move, I thought, to reference Nabokov's masterful memoir _Speak, Memory_ (which I have mixed feelings about). _Taste, Memory_ is about local foods in the USA, from the perspective of the slow food movement. Buchanan is based in Portland, Maine, and talks about heirloom varieties of various foods. He seemed to have a special passion for apples, and after reading his book, I have one too. He also talks about growing crops in his own house, and then leasing a large space at the edge of town to do organic farming. Selling things at the farmers market is difficult, and I learnt that only from him. All in all, this was such a nice book because at the end, when I finished it and had to put the book down, my thought was, "This could go on forever and I could read it."

Before the next book, a small aside about why some things in the world are inherently nice and serendipitous. On my [newsletter](vishalkatariya.com/katskable), I ran a food special and asked for some book recommendations from my readers. And voila, was I swamped or what. The first of the suggestions I tried was Michael Pollan's _The Botany of Desire_. The book's subtitle is, "A plant's eye view of the world". I loved it so much. It describes the journey of humanity through history from the point of view of our partnership with/domestication of four plants: apples, tulips, marijuana, and potatoes. Extremely well researched, eloquently yet straightforwardly written; this is what a nonfiction book should be. I learnt a lot too, about the tulipomania in the Netherlands, about the cannabinoid network of chemicals in our brain, about the realities, both positive and negatives, about monoculture crops and genetic modification. It gave me so much to appreciate and understand about how we've evolved, not only as a species but in terms of our relationships with other species.

Something again popped up at me in the library. Dawn Drzal wrote _The Bread and the Knife_, which is an A-Z of food writing. Consisting of 26 short essays on different aspects of her love for food, Drzal writes a memoir in the most delightful ways. The cornerstones of her life aren't the big conventional events that we would traditionally consider, but instead important culinary milestones. It's enjoyable to read something that's written with such passion. It made me also think: if I were to write such a themed autobiography; what would I center chapters around? It also reminds me a bit of Primo Levi's _Periodic Table_.

And then! Bee Wilson's _Consider the Fork_. Again, recommended by two of my friends; this is not about foods as much as it is about the tools, techniques and utensils we use. The title itself poses this thought: consider the fork as a technology. Why did we ever end up needing a fork? Did you know that a few hundred years ago, Europeans carried their own cutlery to all meals? The knife at the table was much sharper to hack off pieces of meat, but this led to tension at the table because those same knives could be used to ...kill someone. Thus the current iteration of the patently harmless kitchen knife. (personally, I find it funny that we even call it a knife.) Another interesting thing is that there is a significant difference between the bites of Europeans and the Chinese, and this can be understood as arising from the different knives used by the cultures, and also because the Chinese serve every dish in bite-sized morsels that don't need to be further broken down in size. 

I'm about to read in full Samin Nosrat's _Salt, Fat, Acid, Heat_ which is just magnificent. If you are familiar with science, you might know about the legendary status of the Feynman Lectures in Physics. Richard Feynman gave a series of lectures as part of his teaching the basics of physics, and physicists and students all over the world refer to them. The interesting thing is that these lectures are not referred to as _textbooks_. They don't contain techniques to solve problems that you will find in exams. They don't contain problems at all. But they give you a fresh, unique and distilled view of the _physics_. _Salt, Fat, Acid, Heat_ is the Feynman lectures for food. No matter how advanced (or a beginner) of a cook you are, you will stand to gain from Nosrat's masterful reduction of sophisticated techniques to their essential basics. I am excited to read it. It is also beautifully illustrated. 

Some other books that I want to read in the future:

	- The Omnivore's Dilemma, by Michael Pollan
	- Modernist Cuisine, by Nathan Myhrvold (my library has this!)
	- books/work by MFK Fisher.
