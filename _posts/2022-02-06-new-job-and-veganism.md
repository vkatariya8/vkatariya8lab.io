---
layout: post
title: "A satisfying realization about my current career limbo"
description: "I realize that there's something in common with my upcoming career decision and the way I think about the vegan food system"
date: "2022-02-06"
tags: [blog]
categories: [blog]
comments: false
share: true
---

I'm currently taking a break from doing anything for "work". It's the first time I'm doing it, I think, as an adult. I've always been in "work mode", as far as I can remember. I guess it's me trying to be useful and productive always as a way of justifying my existence and resource hogging. Anyway.. my point is that this break is causing a lot of reflection, some good and some counterproductively energy-sapping.

One of my goals is to get into a better relationship with myself and what I want from my career. I did a PhD because it would appear cool, and because it seemed at the time that physics was an unproblematic, apolitical field and thus free of any moral quandaries. I don't think I did physics because I _wanted_ to.

I was talking to a friend last week and told them how this break is an opportunity for me to take all the moving parts in my brain and heart and consciously reassemble them into something that makes me better-integrated than I was last year. Which is great! I was thinking about this today and realized something about my break and thoughts on a future career.

Sometimes I feel like all I want is a "regular" job where I do what's told to me and don't worry about my work even though I may work long hours. All of this seems like a direct opposite to my PhD. But it seems like this is just a band-aid, and doesn't really do anything constructive with all of the moving parts right now.

Other times, I feel like I want to pursue something I'm passionate about and go after it in a big and energetic way. That sounds really exciting too, to be honest. But I am not sure if it's worth the effort to do so. But if I could summon it and take the plunge/risk, it would be great! Possibly great. But I'd at least give myself a chance to be much happier with my choice. 

I was just brushing my teeth now when I realized that that something else I think about often is an accurate analogy here. It's quite unrelated. It's about how we're seeing a big shift in food in supermarkets these days with more and more vegan food. As a vegan myself, it's quite heartening. At the same time, it also feels like the food is markedly different and yet the same. As in, the end product being sold is different, but it comes from markets and industries which operate in much the same way as legacy non-vegan food did. 

Let me elaborate a bit. In a more "ideal" world (for me), we'd see more fresh produce, more whole foods, and less processing. What we have instead are vegan food products which seem quite divorced from their origins (cheese made from potatoes, for example) and which are highly processed. It seemed, initially at least, that there was a big opportunity to celebrate plants and put them forward in a glorious way with all the new vegan opportunities. Instead, all we got was a closed-minded, "let's use plants in direct and convoluted ways to create exact replicas of what we have now." This is a good thing in some ways, as it addresses _some_ issues, but it also perpetuates many problematic, harmful and unproductive themes of the legacy food system.

Why is this relevant to what I'm talking about regarding my next career choice? 

I think it's relevant because I have two broad options open to me now, which I spoke about just above. I can either do a "regular" job and not figure more things about myself, which seems similar to the way we have the current boring yet innovative vegan food products. Alternatively, I can take some time, be deliberate, and choose something while being truer to myself, and this seems analogous to my "ideal world" situation of centering plants, celebrating them, and using that big change as an opportunity to overhaul the system and fix a bunch of things.

This is a cool realization! I think so. It doesn't add any new information to me, but the connection between my current situation and something that I think about often is really satisfying to make and it seems to integrate two previously disparate things that affect me in a big way. 