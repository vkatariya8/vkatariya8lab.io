---
layout: post
title: "#86"
description: "A weekly compilation of great longform journalism from around the internet"
date: 2017-10-15
categories: [katskable]
comments: false
share: true
---

Welcome to Kat's Kable 86! It's been a good reading week for me, hope you enjoy this list. 

If you got this from a friend and want to subscribe, [here's](http://vishalkatariya.com/katskable) the link.

* * *

### **1** [What Happens if you Turn off the Traffic Lights?](https://www.theguardian.com/environment/bike-blog/2017/sep/22/what-happens-if-you-turn-off-the-traffic-lights)

Very interesting experiment in Amsterdam - all the traffic lights were removed from a junction. This made journeys faster and interactions better. It took a while to get used to it, but cyclists (who abound aplenty in Amsterdam) have benefited the most. 

_All respondents complained less about infrastructure and spoke more about human interaction. “People pay more attention,” said one man. “It’s amazing that it regulates itself,” said a young woman. “It’s a bit scary, but you never have to stop and nobody is grumpy,” said a teenager. But no one could really explain more about why or how._

* * *

### **2** [The Coming Software Apocalypse](https://www.theatlantic.com/technology/archive/2017/09/saving-the-world-from-code/540393/)

Quite a scary article to be honest, but something that's quite relevant and important. Creating software right now isn't at all WYSIWYG (what you see is what you get), which means that it becomes incredibly difficult to pinpoint where (and how) something can go wrong. I recommend this piece highly. 

_The principle was this: “Creators need an immediate connection to what they’re creating.” The problem with programming was that it violated the principle. That’s why software systems were so hard to think about, and so rife with bugs: The programmer, staring at a page of text, was abstracted from whatever it was they were actually making._

* * *

### **3** [The Mind of John McPhee](https://www.nytimes.com/2017/09/28/magazine/the-mind-of-john-mcphee.html?nytmobile=0&_r=0)

I loved reading this piece about John McPhee, a nonfiction writer I hadn't heard of earlier. _Also_ highly recommended (I did say this was a good reading week!).

_His mind is pure curiosity: It aspires to flow into every last corner of the world, especially the places most of us overlook. Literature has always sought transcendence in purportedly trivial subjects — “a world in a grain of sand,” as Blake put it — but few have ever pushed the impulse further than McPhee. He once wrote an entire book about oranges, called, simply, “Oranges” — the literary cousin of Duchamp’s urinal mounted in an art museum._

_McPhee is now 86 years old, and each of those years seems to be filed away inside of him, loaded with information, ready to access._

* * *

### **4** [This is your Brain on Art](https://www.washingtonpost.com/graphics/2017/lifestyle/your-brain-on-art/?utm_term=.ba3279a072aa)

A very interesting infographic deconstructing the impact a ballet performance of 'Swan Lake' has on a human brain. Here's an interesting finding:

_Soft, round and open body shapes elicited positive feelings, as in Odette’s whirling images of flight._
_Edgy body shapes triggered negative emotions, such as the Black Swan’s spiky, asymmetrical moves. They’re impressive but also a little alarming._

* * *

### **5** [From Einstein's Theory to Gravity's Chirp](https://www.quantamagazine.org/daniel-kennefick-on-einstein-and-gravitys-chirp-20160218/)

This year's Nobel Prize in Physics went to the LIGO project, which is now detecting gravitational waves from the far reaches of space. This is an interview with Daniel Kannefick, who did his PhD with Kip Thorne, one of the Nobel awardees. It's a nice introduction/explanation of the phenomenon of gravitational waves.

![](https://d2r55xnwy6nx47.cloudfront.net/uploads/2016/02/LIGO_GIF_Infinite.gif)

* * *

### **6** [The New Economy of Excrement](http://www.nature.com/news/the-new-economy-of-excrement-1.22591)

Companies are cropping up now which are looking to use human excrement in order to mitigate its negative environmental impacts, and are now learning how to make a profit also. BioCycle's co-founder David Wilco Drew dreams of a future where 'every citizen of the world can contribute to our supply chain'. It's refreshing to see a startup solving a problem that actually needs to be solved. 

* * *

### **7** [The Father of Mobile Computing is not Impressed](https://www.fastcompany.com/40435064/what-alan-kay-thinks-about-the-iphone-and-technology-now)

Alan Kay is a person I look up to very much, and he is indeed, as this piece calls him, a living legend. He isn't too happy with the way mobile technology has progressed, especially with the fact that phones are far more powerful than they need to be, but aren't being used very constructively. This interview also has him talk about his views on education, which I found instructive. (Get it?)

* * *

### **8** [The Streak of Streaks](http://www.nybooks.com/articles/1988/08/18/the-streak-of-streaks/)

Stephen Jay Gould writes about the probabilistic miracle of Joe DiMaggio's 56-game hitting streak (in the 1941 US Baseball season). What a well written piece that merges statistics and sports. 

_Thus Joe DiMaggio’s fifty-six–game hitting streak is both the greatest factual achievement in the history of baseball and a principal icon of American mythology. What shall we do with such a central item of our cultural history?_

(By the way, I thought of doing a sports special and have a few pieces saved up, should I? What do you think? Would you read it?)

* * *

### **9** [Why India's Farmers want to Conserve Indigenous Heirloom Rice](https://www.theguardian.com/environment/2017/sep/24/why-indias-farmers-want-to-conserve-indigenous-heirloom-rice)

This is an interesting thing. India was once home to 100,000 different varieties of rice (let that sink in), but now only a few high yielding modified varieties dominate. These are less hardy, less robust and so farmers want to conserve the wonderful gene pool they have in the form of indigenous rice. I have tasted a few of the different varities in Tamil Nadu, and I have to say, there were very good and different. 

* * *

### **10** [First Evidence That Online Dating Is Changing the Nature of Society](https://www.technologyreview.com/s/609091/first-evidence-that-online-dating-is-changing-the-nature-of-society/)

A person's romantic activity would usually be limited to his or her friend's friend's friends, but the advent of online dating has started to change that - new and seemingly random connections are being found. Apparently, this is good for the state of society.

![](https://cdn.technologyreview.com/i/images/internet-dating.png?sw=1700&cx=16&cy=0&cw=812&ch=457)

* * *

That's it for this week, let me know if you have something to say, or any ideas about how Kat's Kable could or should be. See you next week. - Kat