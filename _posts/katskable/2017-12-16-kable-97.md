---
layout: post
title: "#97"
description: "A weekly compilation of great longform journalism from around the internet"
date: 2017-12-16
categories: [katskable]
comments: false
share: true
---

Hope you're having a good end-of-year. I'm back with the week's usual list of ten good things to read. Enjoy.

If you got this from a friend and want to subscribe, [here's](http://vishalkatariya.com/katskable) the link.

* * *

### **1** [How Coral Researchers Are Coping With the Death of Reefs](https://www.theatlantic.com/science/archive/2017/11/coral-scientists-coping-reefs-mental-health/546440/)

This is similar to something I'd shared earlier as well, about the mental health of environmentalists and how they keep themselves together despite there being overwhelmingly negative news about their subject most of the time. 

_“I’m a coral reef. I’m also failing to cope.”_

* * *

### **2** [Down the River: The Allure and Perils of Hydropower](https://undark.org/article/hydropower-dams-albania-vjosa/)

"Damming rivers may seem like a clean and easy solution for Albania and other energy-hungry countries. But the devil is in the details."

Hydroelectricity is cheap, but it also has side effects that we sometimes don't take into consideration. Communities (of humans and other species) are displaced, still water grows hostile for life, and more. The more people are studying hydroelectricity now, the more they are concluding that the overall demerits outweigh the positives. 

![](https://undark.org/wp-content/uploads/sites/2/2017/11/itaipudam.jpg)

* * *

### **3** [The Beautiful Intelligence of Bacteria and Other Microbes](https://www.quantamagazine.org/the-beautiful-intelligence-of-bacteria-and-other-microbes-20171113/)

Beautiful visuals.

![](https://d2r55xnwy6nx47.cloudfront.net/uploads/2017/11/02_Scott-Chimileski_-Physarum2_smaller.gif)

* * *

### **4** [Taking Care With Broken Things: How I Came to Practice Ethical Taxidermy](https://catapult.co/stories/artifact-how-i-came-to-practice-taxidermy)

_"I imagined that spending so much time with a dead thing might make death more understandable."_

I enjoyed reading this piece about a woman describing why she practices taxidermy, and also describes the process involved. 

![](https://d7n0myfi538ky.cloudfront.net/production/story_images/2737/large/IMG_20160814_173121_1508883565.jpg?1508883565)

* * *

### **5** [Remove the Legend to Become One](http://www.eugenewei.com/blog/2017/11/13/remove-the-legend)

Lovely and whimsical blog post about what an analytics job in the 1990s looked like. The author worked at Amazon.com, and says that the work primarily was just to produce graphs. Oh, how times have changed. 

_Even with that process, much could go wrong. While I tried to create guardrails to preserve formulas linking all the workbooks, everything from locked cells to bold and colorful formatting to indicate editable cells, no spreadsheet survives engagement with a casual user. Someone might insert a column here or a row there, or delete a formula by mistake. One month, a user might rename a sheet, or decide to add a summary column by quarter where none had existed before. Suddenly a slew of #ERROR's show up in cells all over the place, or if you're unlucky, the figures remain, but they're wrong and you don't realize it._

* * *

### **6** [Poison Pages](http://annarborchronicle.com/2012/05/03/in-the-archives-poison-pages/index.html)

One of the most poisonous books you can imagine exists in the University of Michigan's Buhr book storage facility. Why? Because it has very liberal doses of arsenic and must be handled carefully. Arsenic was used widely in wallpapers as well back in the day, when its poisonous nature was not so clear. The piece then goes on to talk about arsenic poisoning a little more generally. I learnt a bunch of new things. Here's a rather innocent looking (but deadly in real life) picture.

![](http://annarborchronicle.com/wp-content/uploads/2012/05/shad-gold-small.jpg)

* * *

### **7** [Paradise Looted: How Sicily Became Ungovernable](http://www.spiegel.de/international/europe/italian-crisis-region-sicily-elects-new-government-a-1177068.html)

"Italy's poorest region, Sicily, is the country's problem child. Now it has elected a new government. To fix the island, it will have to overcome corruption and widespread Mafia control - and figure out how to convince its population not to leave."

The Mafia has been destabilising the Sicily region heavily for a long time - these days it is infiltrating the government itself. Another problem is that many Sicilians just choose to leave, because the region is just too dangerous to live in.  

* * *

### **8** [Genetic Struggles Within Cells May Create New Species](https://www.quantamagazine.org/genetic-struggles-within-cells-may-create-new-species-20170927/)

Conflicts and mismatches between an organisms own DNA and the DNA of its mitochondria may be responsible for species' splitting into two. This is pretty interesting, and shows yet another way that evolution can occur. 

![](https://d2r55xnwy6nx47.cloudfront.net/uploads/2017/09/Copedpod560.jpg) 

* * *

### **9** [The Story Behind John Cage's 4'33"](http://mentalfloss.com/article/59902/101-masterpieces-john-cages-433)

4'33" is a music track by composer John Cage, which is basically four and a half minutes of silence. It was very controversial when played (to full theatres of mostly exasperataed listeners) but there's a story behind it. This was in protest against the newly popular Muzak music, which played everywhere in the background and was believed to increase positivity and productivity. Cage hated it and bemoaned the increasing loss of silence. John Cage was an interesting man with interesting views. I would give you a link to the piece, but hey, it's only silence.

* * *

### **10** [The Ways in Which a Novel Can Fail Like a Marriage](http://lithub.com/the-ways-in-which-a-novel-can-fail-like-a-marriage/)

Kill your darlings, _the old saw goes. Terrible and true. But I’m not talking about severing a limb to save the whole, tossing a scene or character for the good of the greater story. I’m talking about doing away with the whole damn thing._

How do you know if something’s still worth working on? _That is the hardest question I get asked by other writers, students or peers._ How do you know when it’s time to quit? _Thank God they never ask the only one that’s worse:_ How the hell, then, do you do it?

* * *

Sometimes I save the pieces I find about specific topics for the special editions alone, and I feel that that makes the normal weekly issues a little less diverse. Do you have anything to say about that? Have a good week ahead - Kat.