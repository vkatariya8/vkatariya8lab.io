---
layout: post
title: "Literature Special #82"
description: "A weekly compilation of great longform journalism from around the internet"
date: 2017-09-27
categories: [katskable]
comments: false
share: true
---

Time for literature special. I am excited, hope you are too. Here goes.

By the way, it's been eighty years since _The Hobbit_ was published, and here are two nice things for you to read about it. [There and Back Again - What J.R.R. Tolkien’s classic The Hobbit still has to offer, 80 years after its publication](https://www.theatlantic.com/entertainment/archive/2017/09/the-hobbit-80-years-later/540684/) and [C. S. Lewis’ 1937 Review of The Hobbit](http://lithub.com/c-s-lewis-1937-review-of-the-hobbit/).

![](http://16411-presscdn-0-65.pagely.netdna-cdn.com/wp-content/uploads/2017/09/Hobbit_featured.png)

* * *

### **1** [Why Doesn't Ancient Fiction talk about Feelings?](http://nautil.us/issue/47/consciousness/why-doesnt-ancient-fiction-talk-about-feelings)

_Perhaps people living in medieval societies were less preoccupied with the intricacies of other minds, simply because they didn’t have to be. When people’s choices were constrained and their actions could be predicted based on their social roles, there was less reason to be attuned to the mental states of others (or one’s own, for that matter)._

_Stories that vault readers outside of their own lives and into characters’ inner experiences may sharpen readers’ general abilities to imagine the minds of others. If that’s the case, the historical shift in literature from just-the-facts narration to the tracing of mental peregrinations may have had an unintended side effect: helping to train precisely the skills that people needed to function in societies that were becoming more socially complex and ambiguous._

* * *

### **2** [Beautiful Miniature Books that are Worth Sacrificing your Eyesight for](http://www.atlasobscura.com/articles/miniature-books-iowa-antique)

Isn't this gorgeous. 

![](http://assets.atlasobscura.com/article_images/lg/42421/image.jpg)

* * *

### **3** [Amazon’s Brick-and-Mortar Bookstores Are Not Built for People Who Actually Read](https://www.newyorker.com/culture/cultural-comment/amazons-brick-and-mortar-bookstores-are-not-built-for-people-who-actually-read)

Amazon has physical book stores now and they have superficial, shallow collections. Bestsellers, the most popular books, and the like. These stores cannot ever in their current form satisfy someone who enters a bookstore to explore and to enjoy, and therefore yes, the article's title is right - these bookstores are not for people who actually read. 

* * *

### **4** [Names Aren’t Neutral: David J. Peterson on Creating a Fantasy Language](http://www.unboundworlds.com/2017/05/names-arent-neutral-david-j-peterson-on-creating-a-fantasy-language/)

This is a very instructive tutorial on how to create a fantasy language, if you ever do create one. Even if you won't, you'll enjoy reading what is otherwise a nice chat on practical linguistics. 

* * *

### **5** [Shakespeare's Genius is Nonsense](http://nautil.us/issue/48/chaos/shakespeares-genius-is-nonsense-rp)

This is a triggering title - the article is not at all about how nonsensical Shakespeare is, but about the various devices Shakespeare included in his work without them seeing immediately obvious. He made subtle changes between the dialogues and entries/exits of good and bad guys, for example. One has to be a microscopic reader (not a close reader, a microscopic reader) to discern these, and this just proves further how amazing Shakespeare really was. 

* * *

### **6** [The Time Murakami Met Carver (And other Literary Meet-Cutes)](http://lithub.com/the-time-murakami-met-carver-and-other-literary-meet-cutes/)

_They got along, and afterwards, Carver wrote a poem that he dedicated to Murakami. It begins: “We sipped tea. Politely musing / on possible reasons for the success / of my books in your country. Slipped / into talk of pain and humiliation / you find occurring, and recurring, / in my stories. And that element / of sheer chance. How all this translates / in terms of sales.” Carver also promised to visit Murakami in Japan, and reportedly, Murakami and his wife had a bed specially made to accommodate Carver’s large size—but he never came. He died in 1988._

_Somehow, at no point during his visit did Murakami tell Carver that he was a writer too. “I guess I should have done that,” he told the Harvard Crimson in 2005. “But I didn’t know he would die so young.”_

* * *

### **7** [This Bot Generates a Fantasy World Every Hour](http://www.atlasobscura.com/articles/uncharted-atlas-twitter-bot)

I'm a huge fan of fantasy fiction and have read many many series. One of the most exciting parts of reading fantasy is the presence of new worlds, geographical features. It's also interesting to see how much 'worldbuilding' is required to make a good fantasy novel. From what I can see, there's a sweet spot of effort spent to make a good world. The Uncharted Atlas is a Twitter bot that makes one new map every hour, complete with rivers, fjords, islands, deserts, regions. Everything. It's fascinating. 

* * *

### **8** [Before Dylan, Tagore: on the erasure of Indian literature](https://overland.org.au/2017/08/before-dylan-tagore-on-the-erasure-of-indian-literature/)

This is a nice opinion piece from last month about why nobody spoke about Tagore when Dylan got his Nobel, calling the latter "the first songwriter to win the prize". Tagore is a hugely important figure, important enough to even be knighted despite being from colonial India. Here the author talks about Tagore's achievements, his significance, and his legacy.  

_Tagore’s achievements have inspired me, as an Indian-Australian, to consider the potential to establish myself as an individual with my own agency free from the tokenistic admiration and peculiarity attached to my ethnicity._

* * *

### **9** [On the Ubiquitous, Economy-Shaping, Library-Defining Billy Bookcase](http://www.signature-reads.com/2017/09/fifty-inventions-shaped-modern-economy-billy-bookcase/)

IKEA's Billy Bookcase is rather ubiquitous in many parts of the world. Sixty million of them have been sold, and one is created every three seconds. Bloomberg actually uses the price of the Billy to compare purchasing power in countries across the world. 

* * *

### **10** [Can Poetry Change Your Life?](https://www.newyorker.com/magazine/2017/07/31/can-poetry-change-your-life)

A roundabout answer to a seemingly deceptive and simple question. I recommend that if you find the piece boring and irrelevant, skip the first part and move to the second half. 

_Robbins, Lerner, and Zapruder all tell pretty much the identical story about themselves. One day, almost inadvertently, they read a poem, and suddenly they knew that they had to become writers. They did, and it changed their lives. Later, they all wrote books about poetry. I read those books, and it changed my life. You read this piece about those books. Maybe it will change your life. If it does, the change will be very, very tiny, but most change comes in increments. Don’t expect too much out of any one thing. For although the world is hard, words matter. Rock beats scissors. It may take a while, but paper beats rock. At least we hope so._

* * *

Hope you enjoyed reading this. As always, feedback is more than welcome; just reply to the email. -Kat