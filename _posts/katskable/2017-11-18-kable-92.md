---
layout: post
title: "#92"
description: "Finding the best longform journalism on the internet, ten pieces at a time"
date: 2017-11-18
categories: [katskable]
comments: false
share: true
---

Namaste, and welcome to #92 of Kat's Kable. I wrote something a while ago about my life at my undergraduate college (IIT Madras) and it's [here](http://www.t5eiitm.org/2017/11/goggles-graduate-vishal-katariya/) if you want to read. 

If you got this from a friend and want to subscribe, [here's](http://vishalkatariya.com/katskable) the link.

* * *

### **1** [How to Remember what you Read](https://www.farnamstreetblog.com/2017/10/how-to-remember-what-you-read/)

This is a nice piece from _Farnam Street_ that talks about how you can/should remember what you read. I try to follow a few of these steps, and am constantly trying to wring out as much as I can from things I read. I think the biggest takeaway from this article is that reading actively is infinitely more useful than reading passively. I'm curious to know if you have any strategies that you use to remember things better.

* * *

### **2** [What Happens If China Makes First Contact?](https://www.theatlantic.com/magazine/archive/2017/12/what-happens-if-china-makes-first-contact/544131/)

![](https://cdn.theatlantic.com/assets/media/img/posts/2017/11/WEL_Andersen_ChinaSETI_Web_Dish/edf0a0b96.jpg)

Interesting report on China's preparation for extraterrestrial contact. The whole SETI program has not been receiving as much attention lately as it was a few decades ago, but China's programs are vast and secretive. IF you haven't read Cixin Liu's book _The Three Body Problem_ yet, it's a good take on this exact issue. So much so that now Liu has a large role to play in the Chinese search for extraterrestrial life and contact. 

* * *

### **3** [Great Writers on the Letters of the Alphabet, Illustrated by David Hockney](https://www.brainpickings.org/2017/10/26/hockneys-alphabet-book/)

From _Brain Pickings_. I really loved this piece about a book where Sir Stephen Spender asked world-famous authors like Susan Sontag, Iris Murdoch etc to write about one letter of the alphabet each. I'm going to be a little narcissistic and quote Iris Murdoch's paragraph on the letter C.

_I find the letter C a warm comforting friendly sort of letter, perhaps because I first came across it in action in the word_ cat. _However, there is much to be said against it. It lacks authority.It is not interesting or imposing, certainly not self-assertive. When scrawled by hand it can be easily overwhelmed by its more prominent neighbors. [...] Nevertheless, for the sake of that old friendship, I feel affection for the poor little letter. After all, who wants a kat?_  

* * *

### **4** [Winston Churchill’s Secret Productivity Weapon](https://michaelhyatt.com/naps/)

All hail afternoon naps. 

* * *

### **5** [As U.S. Confronts Internet’s Disruptions, China Feels Vindicated](https://www.nytimes.com/2017/10/16/world/asia/china-internet-cyber-control.html?_r=0)

A nice take on the whole business of foreign interference and fake news. China now feels that it did well by heavily censoring the internet and even employing people to take down posts that don't meet (arguably) draconian rules. The Chinese claim, however, that despite their censorship, the internet is vibrant, useful and widespread. 

* * *

### **6** [Could Young Organic Farmers Stop Climate Change?](https://www.rodalesorganiclife.com/future-young-farmers)

This is something I think of a lot. A quote that I like to recollect is from Jonathan Safran Foer's _Eating Animals_ where Foer says that every food decision we make is farming by proxy. Organic farming is definitely healthier for the soil in the long run, and we need more people doing it. This piece is a nice profile of Harrison Topp, who is a thirty year old organic farmer. 

![](https://www.rodalesorganiclife.com/sites/rodalesorganiclife.com/files/embedovcc-5132.jpg)

* * *

### **7** [A Chess Novice Challenged Magnus Carlsen. He Had One Month to Train.](https://www.wsj.com/articles/chess-novice-challenged-magnus-carlsen-1510866214)

A mad mad piece. A 24-year old entrepreneur Max Deutsch is one of the best learners in the world, and as part of his challenge to learn one new thing a month, he got himself a match with Magnus Carlsen, undoubtedly now the world's best chess player. How do you prepare for, in a month, such an occassion? I only heard about Max Deutsch yesterday and he is amazing and very _very_ inspirational. 

_The unlikely series of events that brought him to this stage began last year, when Max challenged himself to a series of monthly tasks that were ambitious bordering on absurd. He memorized the order of a shuffled deck of cards. He sketched an eerily accurate self-portrait. He solved a Rubik’s Cube in 17 seconds. He developed perfect musical pitch and landed a standing back-flip. He studied enough Hebrew to discuss the future of technology for a half-hour._

* * *

### **8** [Can Google Build a City? We're About to Find Out](http://www.slate.com/articles/technology/metropolis/2017/10/sidewalk_labs_quayside_development_in_toronto_is_google_s_first_shot_at.html)

Sidewalk Labs is a subsidiary of Alphabet, and it has acquired 12 acres of land in Toronto, Canada to build a small city-ette of sorts. It's interesting to see what they have planned, and I'm looking forward to seeing how they go about it. Last week I shared an essay about the rise of city-states, and now this is instructive to see how tomorrow's internet-first, built-with-AI cities will look like. 

_Sidewalk describes its vision for Quayside in terms worthy of Blade Runner, as a city “built from the internet up … merging the physical and digital realms.” In reality, the company’s ambition lies first in the synthesis of established techniques like modular construction, timber-frame building, underground garbage disposal, and deep-water cooling. Not low-tech, but not rocket science either. Sidewalk’s success will depend on deploying those concepts at scale, beginning with a preliminary tract at Quayside but expanding—if all goes well—to Toronto’s Port Lands, a vast, underused peninsula of reclaimed land the size of downtown Toronto._

* * *

### **9** [Inside Botox's Margin for Error](https://www.bloomberg.com/news/features/2017-10-26/inside-fort-botox-where-a-deadly-toxin-yields-2-8-billion-drug)

You have probably heard of Botox, the solution injected to make skin smoother and faces more beautiful. However, Botox comes from a very potent biotoxin Clostridium botulinum that can possibly kill a million people with a well-distributed one gram dose. This piece is the tale of how the poison is contained and yet used so widely around the world. 

![](https://assets.bwbx.io/images/users/iqjWHBFdfxIU/i194YOipCgVk/v4/1000x-1.jpg)

* * *

### **10** [How Close are we to Creating Artifical Intelligence?](https://aeon.co/essays/how-close-are-we-to-creating-artificial-intelligence)

This is an old piece but written by a physicist and thinker who I very much admire, David Deutsch. He argues using a concept called the universality of computation, which basically means that if one type of computer can do a task, any type can. So if the brain can be intelligent, it means that we should be able to make a 'different' sort of computer that can be similarly intelligent too. The essay is exhaustive, and very fun to read.

* * *