---
layout: post
title: "Sports & Games Special #89"
description: "A weekly compilation of great longform journalism from around the internet"
date: 2017-10-31
categories: [katskable]
comments: false
share: true
---

Hello! This is something I've been thinking about for a while, and I'm finally getting around to it. Sports and video games! Here goes. 

If you got this from a friend and want to subscribe, [here's](http://vishalkatariya.com/katskable) the link.

* * *

### **1** [Pipe Dreams: Life on the Road in pursuit of Surfing Glory](https://www.theguardian.com/sport/2017/mar/13/pipe-dreams-life-on-the-road-in-pursuit-of-surfing-glory)

Competitive surfing is a competitive sport, and not one many people enjoy. You have to travel from beach to beach, from tournament to tournament. What keeps people going? Can they fund themselves? This is a great report of the world of surfing. Freya Prumm, after losing a quarterfinal, said, “It just wasn’t my day. My alarm didn’t even go off this morning and then I lost my scourers so I couldn’t wash my dishes.” This really is a very graceful sport.

![](https://i.guim.co.uk/img/media/6bde0d0ad7f19122a626a36c5953220c83be5876/0_0_2953_1969/master/2953.jpg?w=1140&q=20&auto=format&usm=12&fit=max&dpr=2&s=8a0742831f5a9bee83b80d65049ce618)

* * *

### **2** [The Blind Gamer Playing 'Street Fighter 5' at a Pro Level](https://motherboard.vice.com/en_us/article/nev47x/the-blind-gamer-playing-street-fighter-5-at-a-pro-level)

"I think gaming is the thing that kept me going when I went blind.”

Sven van Wege is blind, but wants to make a career out of playing video games. His girlfriend is blind too, and the way they play is just amazing. 

_"Look, I just count to see how long it takes, and this way I can measure how far away from me you are; one jump or two." He jumps once, and gives me an uppercut with fire (a so-called X-special), waits until I try to get up again, and kicks me down once more._

_"It's a matter or timing," he explains, while he keeps kicking me into the ground._

_"There are small problems that come up," he says. "If you touch the arrow for short time only, the forward movement isn't audible." In his world, a quiet enemy is an invisible enemy, and every move that doesn't involve a sound, leaves him guessing about what is going on. "I would like to say to Capcom: 'You're almost there, but change this too. Then I can fully participate.'"_

* * *

### **3** [Remote Control - about Tonya Harding and Nancy Kerrigan](https://www.believermag.com/issues/201401/?read=article_marshall)

If you don't know about the incident between Tonya Harding and Nancy Kerrigan, you could look it up ([wiki](https://en.wikipedia.org/wiki/Tonya_Harding#Attack_on_Nancy_Kerrigan_and_aftermath) - it's one of the iconic sports incidents. This piece is a great in-depth report on what happened, who was really involved, and why people did what they did.

* * *

### **4** [Truly Intelligent Enemies could change the Face of Gaming](https://www.engadget.com/2017/05/26/the-future-of-video-game-violence/)

_Khandaker can imagine creating games where the enemies aren't just tokens or pawns but more fully formed virtual characters. "Instead of just committing violence upon some kind of enemy, maybe [players will be] trying to understand their motives, she said. "Now, in this cultural context, more than ever, a human understanding of the reasons why people make decisions they do is super-important. Even if, on some level, we think decisions people make might be evil, we still need to have the level of understanding because that's how we learn and grow and how we combat evil."_

* * *

### **5** [The Beautiful Chaos Of The Warriors’ Offense](https://fivethirtyeight.com/features/the-beautiful-chaos-of-the-warriors-offense/)

The Oakland, California-based basketball team Golden State Warriors have won the NBA championship in 2015 and 2017, and are currently dominating the world of basketball in an entirely new way. I won't get into technicalities because I am sure this piece explains it all better than I can. FiveThirtyEight does great sports and statistics reporting. 

_“Steve [Kerr] has really empowered these guys with the way he teaches them. It’s not, ‘You go to this space; you here, you here and you here.’ Not at all. He teaches guys the way you would imagine guys learning back in the day on the playground. … So where I’m going with this: It’s not scripted. And when you have something that’s not scripted, your opponents break down, because it’s different every single time.”_

* * *

### **6** [Key Crazy: Inside the Wonderful World of Keyboard Fanatics](https://gizmodo.com/key-crazy-inside-the-wonderful-world-of-keyboard-fanat-1794380835)

![](https://i.kinja-img.com/gawker-media/image/upload/s--PNEqtvUv--/c_scale,fl_progressive,q_80,w_1600/y3ifraezi5bcmirtxvya.jpg)

As someone who loves looking at different keyboards and prefers noisy ones over the new silent ones, this article was a positive delight. 

* * *

### **7** [We Are Nowhere Close to the Limits of Athletic Performance](http://nautil.us/issue/51/limits/we-are-nowhere-close-to-the-limits-of-athletic-performance-rp)

Interesting - this article from _Nautilus_ is filed under Genetics, and that's the aspect of athletic development that it focuses on. I don't know if I agree with genetically modified 'outliers' - I don't think that is how sport should work.

_We’re just scratching the surface of what genetic outliers can do. The normal distribution we see in athletic capabilities is a telltale signature of many small additive effects that are all independent from each other. Ultimately, these additive effects come from gene variants, or alleles, with small positive and negative consequences on traits such as height, muscularity, and coordination. It is now understood, for example, that great height is due to the combination of an unusually large number of positive variants, and possibly some very rare mutations that have a large effect on their own._

* * *

### **8** [The Notorious Board Game That Takes 1,500 Hours To Complete](https://kotaku.com/the-notorious-board-game-that-takes-1500-hours-to-compl-1818510912)

Wow. Just wow. This game is called "The Campaign for North Africa" and it tries to be as realistic and complicated as an actual campaign to wrest control of North Africa. 

_The thick, black-and-white rulebook packaged with every copy of the 1979 war-game The Campaign For North Africa is full of obtuse decrees, but the tabletop community always had a special appreciation for entry 52.6 - affectionately known as the “macaroni rule.” The Italian troops in World War II were outfitted with noodle rations, and in the name of historical dogma, the player responsible for the Italians is required to distribute an extra water ration to their forces, so that their pasta may be boiled. Soldiers that do not receive their “pasta point” may immediately become “disorganized,” rendering them useless in the field. It’s a fact of life really: if the Italians can’t boil their pasta, the Italians may desert._

![](https://i.kinja-img.com/gawker-media/image/upload/s--_FY-Qzhd--/c_scale,fl_progressive,q_80,w_800/iyh5s7dgcbtwtgfdo1cm.jpg)

* * *

### **9** [Forget Velocity, the Curveball's Resurgence is changing Modern Pitching](https://www.si.com/mlb/2017/05/23/curveball-clayton-kershaw-lance-mccullers)

The curveball is the more enigmatic and artistic pitch in the game of baseball. I don't know too much about modern baseball, or even baseball, but this article was very interesting nonetheless.

_No pitch causes major league hitters to freeze more often. No pitch has inspired more legends, myths, fear, grips, nicknames, and ooohs and aaahs. It is a wonder of physics, geometry and art, a beautiful, looping arc through space made possible by the interplay between gravity and the Magnus force—a result of the flow of air around the spinning sphere—that often leaves us, and the hitter, paralyzed in wonderment._

![](https://cdn-s3.si.com/images/curveball-inline-1.jpg)

* * *

### **10** [How Cheat Codes Vanished from Video Games](https://waypoint.vice.com/en_us/article/8qg7gk/how-cheat-codes-vanished-from-video-games)

Cheat codes were created by developers to help them develop games, and developers would then not take the easter eggs out of the game because they weren't sure if the game would run as smoothly as it did with the cheat codes. Now this doesn't happen.  

_"The process was very different back then," added David Brevik, co-founder of Blizzard North. Before paving the road to hell with 1996's  Diablo, Blizzard North paid the bills by taking contracts to write games for Sega Genesis and Game Boy. "You couldn't patch games. You sent the code off to get approved and it was rejected or approved by the console manufacturer, and that was it. During those last builds, sometimes people just plain forgot to take codes out."_

* * *

Here are a few more pieces that I think I'd share with you. The number 10 is just a construct created by me.

* [The U.S. Gymnastics System Wanted More Medals, And Created A Culture Of Abuse To Get Them](https://deadspin.com/the-u-s-gymnastics-system-wanted-more-medals-and-crea-1794525855)
* [When athletes gotta go ... where do they go?](http://www.espn.com/nfl/story/_/id/19769575)
* [Lacey Baker: The Rebel Queen of Skateboarding](http://www.rollingstone.com/sports/features/lacey-baker-the-rebel-queen-of-skateboarding-w480892)
* [Veteran game developers reveal their childhood creations](https://www.polygon.com/2017/5/22/15655926/veteran-game-developers-reveal-their-childhood-creations)
* [Fifa: the video game that changed football](https://www.theguardian.com/technology/2016/dec/21/fifa-video-game-changed-football)