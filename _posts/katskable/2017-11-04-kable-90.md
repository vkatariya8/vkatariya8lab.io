---
layout: post
title: "#90"
description: "Finding the best longform journalism on the internet, ten pieces at a time"
date: 2017-11-04
categories: [katskable]
comments: false
share: true
---

Hope you've had a good week. I've read a lot, and it's quite exciting. Here goes, and I'm very excited that in roughly two months, I'll hit a hundred issues! 

If you got this from a friend and want to subscribe, [here's](http://vishalkatariya.com/katskable) the link.

* * *

### **1** [Science on Trial](https://theoutline.com/post/1803/science-on-trial)

An interesting piece about how bad science and underqualified experts can drastically change court decisions. It is (as much as many things I also share) centred around America, but nonetheless it is a good report. 

_There’s one obvious problem with this approach. Judges are presented with evidence ranging from forensic accounting to psychology to burn patterns from fires; it’s impossible to have in-depth understandings of them all. As a result, judges must often make an assessment of an expert’s reliability without a preexisting understanding of the underlying discipline. “No judge is familiar with all areas,” said federal district judge Jed Rakoff. “I think what judges have in common is they’re quick studies. The judge may not be an expert in science, but they are an expert in reasoning and logic.”_

* * *

### **2** [The Unknowable Joni Mitchell](https://www.theatlantic.com/magazine/archive/2017/11/the-unknowable-joni-mitchell/540618/)

This (as well as [Joni Mitchell: Fear of a Female Genius](https://www.theringer.com/music/2017/10/16/16476254/joni-mitchell-pop-music-canon)) is a good biography of a musician I admire. 

_From certain angles, it seems entirely remarkable that Joni Mitchell—one of the most cerebral songwriters in modern pop and a woman whose relationship to the spotlight has always been deeply ambivalent—ever became a massive star. From other angles, her ascension seems inevitable._

* * *

### **3** [Google X and the Science of Radical Creativity](https://www.theatlantic.com/magazine/archive/2017/11/x-google-moonshot-factory/540648/)

The point of Google X is not to augment Google in any which way, but to do something to solve a problem outside its normal purview. Of course, this piece is obviously motivated to be good about it, but it's still nice to read.

_X is perhaps the only company on the planet where regular investigation into the absurd is encouraged, and even required._

* * *

### **4** [The World’s Greenest Sports Team Is a Century-Old Football Club in a Tiny English Town](https://www.newyorker.com/news/sporting-scene/the-worlds-greenest-sports-team-is-a-century-old-football-club-in-a-tiny-english-town)

As someone who is a vegan and cares about it, I think it's fair that once in a while I share something that's motivated by what I want people to know. This is an interesting piece about a football club that's as green as can be. 

_Forest Green is the first completely vegan professional sports team in the world. But its ethos extends way beyond food. The team plays on an organic and vegan field, called the New Lawn, which is fed with a solution of Scottish seaweed that’s hand-cut and cold-pressed. No pesticides are used to kill weeds; the groundskeeper, Adam Witchell, pulls them himself. “I’m the only groundskeeper here, except for the robot,” he told me recently. He was referring to Forest Green’s lawnmower, an autonomous, solar-powered, G.P.S.-guided device they call the Mow Bot. The New Lawn’s stadium boasts solar panels, a rainwater-collection system, drainage under the pitch that captures excess water for reuse, and charging stations for electric cars. In May, fifa dubbed the team the “world’s greenest football club.”_

* * *

### **5** [What Britain needs to understand about the profound and ancient divisions in Germany](https://www.newstatesman.com/world/europe/2017/09/what-britain-needs-understand-about-profound-and-ancient-divisions-germany)

Pretty interesting review of the history of Germany. I did not know that the West and East were this radically different. 

* * *

Here's a bunch of articles (coincidentally) on artifical intelligence.

* * *

### **6** [Big Data meets Big Brother as China moves to Rate its Citizens](http://www.wired.co.uk/article/chinese-government-social-credit-score-privacy-invasion)

"The Chinese government plans to launch its Social Credit System in 2020. The aim? To judge the trustworthiness – or otherwise – of its 1.3 billion residents".

If you have not read this yet, you should. This is a nearly-crazy idea of assigning reliability scores to every single citizen. It seems too futuristic to be true, but it's now nearly a real thing. 

* * *

### **7** [India's Grasp on IT jobs is loosening up. Is Artificial Intelligence to blame?](https://www.technologyreview.com/s/609118/india-warily-eyes-ai/)

My view on this issue goes like : we must have had similar fears during the Industrial Revolution; will we not figure it out this time too like we did then? Good reporting.

On a slightly positive note,
_Companies are eager to explain why automation won’t hollow out, and might even expand, their swarms of employees. For one thing, it isn’t as if machines can make people instantly redundant. “Jobs are not structured in such a clean way,” says Giacomelli, at Genpact. The architectures of modern work that have developed over decades all have human beings at their center; they rely upon people’s agility and their capacity to hold different things in their minds. “People do many things, so it’s not that easy to extricate one task or the other and make that happen through AI,” he says._

* * *

### **8** [The Last Invention of Man](http://nautil.us/issue/53/monsters/the-last-invention-of-man)

A fictional story (excerpted from a book I want to read) about how an ultra-intelligence Prometheus will radically change nearly everything in the world - business, education, government, social structure, etc. Very fascinating read. 

_The rest of the book is about another tale—one that’s not fictional and not yet written: the tale of our own future with AI. How would you like it to play out? Could something remotely like the Omega story actually occur and, if so, would you want it to? Leaving aside speculations about superhuman AI, how would you like our tale to begin? How do you want AI to impact jobs, laws and weapons in the coming decade? Looking further ahead, how would you write the ending? This tale is one of truly cosmic proportions, for it involves nothing short of the ultimate future of life in our universe. And it’s a tale for us to write._

* * *

### **9** [Shiv on the Shore](http://www.thecricketmonthly.com/story/793249/shiv-on-the-shore)

![](http://p.imgci.com/db/PICTURES/CMS/196400/196431.jpg)

I realised that I forgot to share this on the sports special this week (did you like it?) and I aboslutely must, so here. Shivanrine Chanderpaul is someone to admire. 

_The Chanderphiles revel in Shivnarine's focus, his faux-warrior glare-patches (introduced to him by Faoud Bacchus in Florida, where the "light real bright"), the evolving splendours of his stance, the routines (the bail hammered into the pitch for guard, the pitch kissed after a century, the fidgeting, the flickering tongue and darting eyes before facing up), in the improbably quick innings and especially the interminable ones, and an inexplicable interest in the numbers which bear out, triumphantly, the entire spectrum of Shiv._

* * *

### **10** [The Beautiful Language of the People who Talk like Birds](http://www.bbc.com/future/story/20170525-the-people-who-speak-in-whistles)

What a delightful study of whistling languages used by human tribes around the world. Scientists first thought that whistles couldn't form a complete language and that they could be used only for some specific tasks - but they were surprised when they found that you can encode enough variation in a language of just whistles that can be an entire language, on par with any written one.  

* * *

As always, if you have anything to say, just reply to this email. 