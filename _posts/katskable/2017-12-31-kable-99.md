---
layout: post
title: "#99"
description: "A weekly compilation of great longform journalism from around the internet"
date: 2017-12-31
categories: [katskable]
comments: false
share: true
---


Hi, here's the ninetynineth issue of Kat's Kable. With no further ado, here we go. 

If you got this from a friend and want to subscribe, [here's](http://vishalkatariya.com/katskable) the link.

* * *

### **1** [The True Story of the fake US Embassy in Ghana](https://www.theguardian.com/world/2017/nov/28/the-true-story-of-the-fake-us-embassy-in-ghana)

This was a pretty mad read. A building was purported to be a fake US embassy, but it wasn't. However, there is still a thriving underground visa industry there. You may not be able to get yourself a fake USA visa, but you can get fake documents, or fake visas (and passport stamps) from other countries to prove your credibility. 

_The fake embassy became a sensation largely because the story was so predictably familiar. The Africans were scammers. The victims were desperate and credulous. The local police officers were bumbling idiots. Countless officials were paid off. And at the end, the Americans swooped in and saved the day. There was only one problem with the story: it wasn’t true._

![](https://i.guim.co.uk/img/media/8e21fa4c49c0bf35d609c05edfd0497ee1909b6a/0_192_4212_2527/master/4212.jpg?w=1300&q=20&auto=format&usm=12&fit=max&dpr=2&s=de4f928155b84e310cc94208a3e2b62d)

The purported fake embassy.

* * *

### **2** [How Russian Tycoon Yuri Milner Bought His Way Into Silicon Valley](https://www.wired.com/2011/10/mf_milner/)

Yuri Milner, the Russian billionaire, has literally bought his way to a prime position in the social media industry, possibly the most happening and valuable of all modern ones. 

_To many, Milner’s success is not just too much and too fast in a land of too much and too fast but … but … and here people start to petulantly phumpher … somehow_ unfair: _Here’s an outsider who has handed out money at outrageously founder-friendly terms—paying huge amounts for relatively small stakes, essentially buying exclusive access to the most desirable companies on the web! It is his outsiderness that seems most irritating and even alarming. How is it that an outsider has spotted opportunities that the Valley’s best investors missed? Does Milner’s success suggest that the rest of the world is starting to horn in on what has been, to date, as American as apple pie—the Internet future and Internet riches?_

* * *

### **3** [Running in Circles](https://m.signalvnoise.com/running-in-circles-aae73d79ce19)

This is a blog post from the company Basecamp where the author Ryan Singer talks about his team and company's approach to building tools and doing work. The best part of this piece was the identification that building a piece of software is not a linear labour; it is difficult in the beginning to estimate the amount of effort/labour needed to ship a final product. In reality, the trajectory is more akin to crossing a hill, and once you have crossed the highest point, your future path becomes clear to you. 

![](https://cdn-images-1.medium.com/max/1600/1*xV-g3zRDo6Zuu0QfTBGtNQ.png)

* * *

### **4** [The Secret Life of Herbs in the Trans-Himalayas](http://www.goyajournal.in/blog/the-secret-life-of-herbs-in-the-trans-himalayan-flowers)

This is not exactly a longform piece, but a very aesthetically pleasing account of the various herbs found in the Trans-Himalayas of Himachal Pradesh.

![](https://static1.squarespace.com/static/578753d7d482e9c3a909de40/t/5a16cfe5e2c483f5ad737d9c/1511444660708/Nepeta+floccosa%3A+we+were+treated+to+entire+mountain+sides+swathed+in+blooming+flowers.JPG?format=2500w)

* * *

### **5** [Maybe China Can't Take over the World](https://www.bloomberg.com/view/articles/2017-12-03/maybe-china-can-t-take-over-the-world)

This is interesting perspective from _Bloomberg_. There are definite reasons why some technologies have thrived in China, such as WeChat, mobile payments, AliExpress, and even bicycle rentals, and these reasons are exlusive to the Chinese mainland. It is not trivial to see how the same companies will thrive in more libertarian economies without unilateral government support and data.

* * *

### **6** [The Impossibility of Intelligence Explosion](https://medium.com/@francois.chollet/the-impossibility-of-intelligence-explosion-5be4a9eda6ec)

This was an interesting piece I read a short while ago; the author argues that we will not see an explosion of superhuman artifical intelligence purely based on the fact that humans haven't been able to, and will not, create systems more intelligent than themselves. I don't know if I agree, but I sort of hope that I do.

* * *

### **7** [Learning About Cities by Mapping Their Smells](https://www.atlasobscura.com/articles/art-mapping-smell-smellscapes-kate-mclean)

This is very fun to look at. For example, take this map of Edinburgh's smells in 2011.

![](https://assets.atlasobscura.com/article_images/49503/image.jpg)

* * *

### **8** [Lake Chad: The World's Most Complex Humanitarian Disaster](https://www.newyorker.com/magazine/2017/12/04/lake-chad-the-worlds-most-complex-humanitarian-disaster)

"Boko Haram, climate change, predatory armies, and extreme hunger are converging on a marginalized population in Central Africa."

This is a very bleak picture, but I have a side note. How true is/can this piece be? It is available for free on the internet, and if I don't read up on the issue by myself, I can only accept the facts stated in this article. Do I have to be careful when reading a piece from any news outlet, regardless of how reputed it is? I'm not saying that I doubt the veracity of this piece, but commenting in general. 

* * *

### **9** [A Generation in Japan Faces a Lonely Death](https://www.nytimes.com/2017/11/30/world/asia/japan-lonely-deaths-the-end.html?_r=0)

Post WW2, Japan sought to emulate western living styles and thus many large apartment complexes were constructed. Now, decades later, the occupants of those complexes are old and living alone. With hardly any connections and friends, they are very alone. This is a haunting article from the _New York Times_.

_Months ago, after he had not been seen for a week, a volunteer went knocking on his door. There was no answer, but she could hear the television from inside. Thinking he was dead, the volunteer called the police. When Mr. Kinoshita finally woke up from a deep sleep, he was a little embarrassed, yet also relieved and maybe even a little happy that his existence had figured into someone’s thoughts._

* * *

### **10** [Hunt Elephants to Save Them? Some Countries See No Other Choice](https://www.nytimes.com/2017/12/04/science/elephants-lions-africa-hunting.html)

By selling some hunting permits for large sums of money, African countries raise funds to conserve wildlife on a large scale. Is this a sustainable solution? Hunters pay upwards of $50,000 in Zimbabwe to hunt elephants. That is a large sum of money that can be put to good use. 

* * *

Have a good week ahead. - Kat