---
layout: post
title: "Lazy Adjectives"
description: "Use the right knife for each task"
date: "2018-11-17"
tags: [blog, writing, miscellany]
categories: blog
comments: false
share: true
---

For the last few months, I've tried to incorporate one particular rule all my writing: avoid "lazy" adjectives and adverbs like "very", "just", "really" etc.

Story time. Why do I think this way? My inspiration, funnily, comes from cricket commentary from nearly a decade ago. I think it was Virat Kohli who hit an imperious, flowing cover drive and David Lloyd (my favorite cricket commentator) called it a languid shot. Nasser Hussain, the other commentator in the box, remarked on Lloyd's choice of adjective and said that it was refreshing to hear a particular adjective used, instead of a generic superfluous adjective that you hear all the time in sports commentary. Ever since, two of my favorite adjectives have been laconic and languid. When I came across them in my GRE preparation material, I positively whooped in delight.

I think about using the right adjective as using the right knife for each particular task. There is a reason there are so many adjectives adverbs; it's because each one means something different, and also arose in a different context. Isn't sanguine a far nicer word to see, write and say than plain "cheerful"? When I read or remember Sherlock Holmes, the first word that comes to me is "laconic". It is accurate. Arthur Conan Doyle clearly chose the right word. Other thesaurus entries are "concise", "terse" and "succinct". None of those would work.

This rule that I've begun to adopt has had benefits beyond effective GRE preparation. When I'm writing, I can't say something is "really good" -- I have to choose another word. And I end up going to the thesaurus, even, sometimes. And in the search for the right description, I am forced to re-evaluate the thing I'm describing in the first place. It's like writing things down to understand them better; or paraphrasing things you've read so that you can get to their root. And as Feynman says, the only way you can be sure you've understood something is by explaining it in simple terms to someone else. What other way to do that than to search for the perfect adjective?

Interesting thought to end with: what if the thing you want to describe has no word in the language you're thinking in? Does that mean your repertoire has to expand to another _language_? When peeling a carrot, you can't use a knife as effectively as you can use a peeler. 
