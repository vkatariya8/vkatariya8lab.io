linkscsvpath = "../_data/link_links.csv"
pagespath = "../_posts/links/"
from time import strftime, gmtime 

def remove_metadata(s):
    s = s.split('---')
    s = s[0]
    return s

import os, shutil
for the_file in os.listdir(pagespath):
    file_path = os.path.join(pagespath, the_file)
    try:
        if os.path.isfile(file_path):
            os.unlink(file_path)
    except Exception as e:
        print(e)

todays_date = strftime('%Y-%m-%d', gmtime())
todays_date = "2019-06-20"

linksfile = open(linkscsvpath, 'r')
linksfile.readline()
#print(linksfile.readlines()[0].split(','))
lines = linksfile.readlines()
for line in lines:
    split = line.split(',')
    category = split[0]
    category = category.replace(' ', '-')
    categoryfilename = pagespath + todays_date + '-'+ str.lower(category.replace('/', '-')) + '.md'
    link = '/Users/kat/Documents/joplin/' + split[1][:-1] + '.md'
    yaml_layout = '---\nlayout: links-post\ntitle: ' + category + '\ncategories: links\nshare: true\n---\n\n## Links - '
    linkcontent = remove_metadata(open(link, 'r').read())
    content = yaml_layout + linkcontent
    #print(content)
    outfile = open(categoryfilename, 'w')
    outfile.write(content)